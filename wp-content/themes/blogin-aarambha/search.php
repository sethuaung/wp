<?php
/**
 * The template for displaying search results pages
 *
 * @link https://developer.wordpress.org/themes/basics/template-hierarchy/#search-result
 *
 * @package Blogin_Aarambha
 */

get_header();
?>

<div id="primary" <?php Blogin_Aarambha_Helper::primary_class();?>>
    <main id="main" class="site-main">

        <?php
        /**
         * Functions hooked into blogin_aarambha_content_before action
         *
         */
        do_action( 'blogin_aarambha_content_top' );
        ?>

        <?php if ( have_posts() ) : ?>

            <?php
            /**
             * Functions hooked into blogin_aarambha_content_loop_before action
             *
             */
            do_action('blogin_aarambha_content_loop_before');

            /* Start the Loop */
            while ( have_posts() ) : the_post();

                /**
                 * Run the loop for the search to output the results.
                 * If you want to overload this in a child theme then include a file
                 * called content-search.php and that will be used instead.
                 */

				get_template_part( 'template-parts/content', '' );

            endwhile;

            /**
             * Functions hooked into blogin_aarambha_content_loop_after action
             *
             * @hooked blogin_aarambha_posts_navigation - 10
             */
            do_action('blogin_aarambha_content_loop_after');

        else :

            get_template_part( 'template-parts/content', 'none' );

        endif;
        ?>

        <?php
        /**
         * Functions hooked into blogin_aarambha_content_bottom action
         *
         */
        do_action( 'blogin_aarambha_content_bottom' );
        ?>

    </main><!-- #main -->
</div><!-- #primary -->

<?php get_footer(); ?>
