<?php
/**
 * The sidebar containing the main widget area
 *
 * @link https://developer.wordpress.org/themes/basics/template-files/#template-partials
 *
 * @package Blogin_Aarambha
 */

if ( ! is_active_sidebar( 'sidebar-1' ) || Blogin_Aarambha_Helper::get_sidebar_layout() == 'none' ) {
    return;
}

/**
 * Functions hooked into blogin_aarambha_sidebar_before action
 *
 */
do_action( 'blogin_aarambha_sidebar_before' );
?>

    <aside id="secondary" <?php Blogin_Aarambha_Helper::sidebar_class(); ?>>

        <?php
        /**
         * Functions hooked into blogin_aarambha_sidebar_top action
         *
         */
        do_action( 'blogin_aarambha_sidebar_top' );
        ?>

        <?php dynamic_sidebar( 'sidebar-1' ); ?>

        <?php
        /**
         * Functions hooked into blogin_aarambha_sidebar_bottom action
         *
         */
        do_action( 'blogin_aarambha_sidebar_bottom' );
        ?>

    </aside><!-- #secondary -->

<?php
/**
 * Functions hooked into blogin_aarambha_sidebar_before action
 *
 */
do_action( 'blogin_aarambha_sidebar_after' );
