<?php
/**
 * Template part for displaying post content in single.php
 *
 * @link https://developer.wordpress.org/themes/basics/template-hierarchy/
 *
 * @package Blogin_Aarambha
 */

?>
<article id="post-<?php the_ID(); ?>" <?php post_class(); ?>>

    <div class="post-detail-wrap d-flex flex-column">
        <?php
        /**
         * Functions hooked into blogin_aarambha_post_content action
         *
         * @see blogin_aarambha_post_header_wrapper()
         * @see blogin_aarambha_post_entry_content()
         * @see blogin_aarambha_edit_post_link()
         */
        do_action( 'blogin_aarambha_post_content' );
        ?>
    </div>
    
</article><!-- #post-<?php the_ID(); ?> -->

