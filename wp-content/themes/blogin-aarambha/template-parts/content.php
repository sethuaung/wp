<?php
/**
 * Template part for displaying posts
 *
 * @link https://developer.wordpress.org/themes/basics/template-hierarchy/
 *
 * @package Blogin_Aarambha
 */
?>

<article id="post-<?php the_ID(); ?>" <?php post_class(); ?>>

    <?php
    /**
     * Functions hooked into blogin_aarambha_posts_content action
     *
     * @hooked blogin_aarambha_get_post_thumbnail - 10
     * @hooked blogin_aarambha_blog_post_content   - 15
     */
    do_action( 'blogin_aarambha_posts_content' );

    ?>

</article><!-- #post-<?php the_ID(); ?> -->
