<?php
/**
 * Template part for displaying header color mode
 *
 * @link https://developer.wordpress.org/themes/basics/template-hierarchy/
 *
 * @package Blogin_Aarambha
 */
?>

<div class="header-color-mode-wrap">
    <button id="theme-toggle" class="theme-color-mode" type="button">
        <span class="color-light"><i class="fas fa-moon"></i></span>
        <span class="color-dark"><i class="fas fa-sun"></i></span>
    </button>
</div><!-- .header-html-wrap -->