<?php
/**
 * Template part for displaying page content in page.php
 *
 * @link https://developer.wordpress.org/themes/basics/template-hierarchy/
 *
 * @package Blogin_Aarambha
 */

?>

<article id="post-<?php the_ID(); ?>" <?php post_class(); ?>>

    <?php
    /**
     * Functions hooked into blogin_aarambha_page_content action
     *
     * @see blogin_aarambha_page_header_wrapper()
     * @see blogin_aarambha_page_entry_content()
     * @see blogin_aarambha_edit_post_link()
     */
    do_action( 'blogin_aarambha_page_content' );
    ?>

</article><!-- #post-<?php the_ID(); ?> -->
