<?php
/**
 * Blogin Aarambha Helper functions
 *
 * @package Blogin_Aarambha
 */

class Blogin_Aarambha_Helper {

    /**
     * Store the post ids.
     *
     * Since blog page takes the first post as its id,
     * here we are storing the id of the post and for the blog page,
     * storing its value via getting the specific page id through:
     * `get_option( 'page_for_posts' )`
     *
     * @return false|int|mixed|string|void
     */
    public static function get_post_id() {

        $post_id        = '';
        $page_for_posts = get_option( 'page_for_posts' );

        // For single post and pages.
        if ( is_singular() ) {
            $post_id = get_the_ID();
        } elseif ( ! is_front_page() && is_home() && $page_for_posts ) { // For the static blog page.
            $post_id = $page_for_posts;
        }
        elseif ( Blogin_Aarambha_Helper::is_woocommerce() && is_shop() ) { // Shop Page
            $post_id = wc_get_page_id('shop');
        }

        // Return the post ID.
        return $post_id;
    }

    /**
     * Function to return the boolean value if `WooCommerce` plugin is activated or not.
     *
     * @return boolean
     */
    public static function is_woocommerce() {

        if ( class_exists( 'WooCommerce' ) ) {
            return true;
        } else {
            return false;
        }
    }
    
    /**
     * Returns sidebar layout value
     *
     * @param string $sidebar default sidebar value is none
     * @return string $sidebar
     */
    public static function get_sidebar_layout( $sidebar = 'none' ) {

        // Check meta first to override and return (prevents filters from overriding meta)
        $sidebar = get_post_meta( self::get_post_id(), 'blogin_aarambha_sidebar_layout', true );
        if ( $sidebar && $sidebar != 'default' ) {
            return $sidebar;
        }
        if ( is_single() ) {
            $sidebar = get_theme_mod( 'blogin_aarambha_post_sidebar_layout', 'right' );
        } elseif ( is_page() ) {
            $sidebar = get_theme_mod( 'blogin_aarambha_page_sidebar_layout', 'right' );
        }
        else {
            $sidebar = get_theme_mod( 'blogin_aarambha_blog_sidebar_layout', 'none' );
        }
        return $sidebar;
    }

    /**
     * Get an array of terms from a taxonomy.
     *
     * @static
     * @access public
     * @param string|array $taxonomies See https://developer.wordpress.org/reference/functions/get_terms/ for details.
     * @return array
     */
    public static function get_terms( $taxonomies ) {
        $items = [];

        // Get the post types.
        $terms = get_terms(
            array(
                'taxonomy'   => $taxonomies,
                'hide_empty' => true,
            )
        );

        // Build the array.
        if ( $terms ) {
            foreach ( $terms as $term ) {
                $items[ $term->term_id ] = esc_html($term->name);
            }
        }
        return $items;
    }

	/**
	 * Get an array of posts.
	 *
	 * @static
	 * @access public
	 * @param array $args Define arguments for the get_posts function.
	 * @return array
	 */
	public static function get_posts( $args ) {
		if ( is_string( $args ) ) {
			$args = add_query_arg(
				array(
					'suppress_filters' => false,
				)
			);
		} elseif ( is_array( $args ) && ! isset( $args['suppress_filters'] ) ) {
			$args['suppress_filters'] = false;
		}

		// Get the posts.
		// TODO: WordPress.VIP.RestrictedFunctions.get_posts_get_posts.
		$posts = get_posts( $args );

		// Properly format the array.
		$items = array();
		foreach ( $posts as $post ) {
			$items[ $post->ID ] = $post->post_title;
		}
		wp_reset_postdata();

		return $items;
	}

    /**
     * Get data columns with values.
     *
     * @access public
     * @param array $values
     * @return void
     */
    public static function get_data_columns( $values = [] ) {

        ob_start();

        if ( ! empty( $values ) ) {

            // Base or Mobile
            echo isset( $values['mobile'] )
                ? ' data-columns="' . esc_attr( $values['mobile'] ) .'"'
                : ( isset( $values['tablet'] )
                    ? ' data-columns="' . esc_attr( $values['tablet'] ) .'"'
                    : ( isset( $values['desktop'] )
                        ? ' data-columns="' . esc_attr( $values['desktop'] ) .'"'
                        : ''
                    )
                );
            // Tablet
            echo isset( $values['tablet'] ) && isset( $values['mobile'] )
                ? ' data-columns-md="' . esc_attr( $values['tablet'] ) .'"'
                : ( isset( $values['desktop'] ) && isset( $values['tablet'] )
                    ? ' data-columns-md="' . esc_attr( $values['desktop'] ) .'"'
                    : ''
                );
            // Desktop
            echo isset( $values['desktop'] ) && isset( $values['tablet'] ) && isset( $values['mobile'] )
                ? ' data-columns-lg="' . esc_attr( $values['desktop'] ) .'"'
                : '';
        }

        $output = ob_get_clean();

        echo apply_filters( 'blogin_aarambha_get_data_columns', $output ); // phpcs:ignore WordPress.Security.EscapeOutput.OutputNotEscaped
    }

    /**
     * Archive Title
     *
     * @param $before string
     * @param $after string
     * @access public
     * @return void
     */
    public static function archive_title( $before = '', $after = '' ) {
        if ( is_category() ) {
            $title = sprintf( __( '<label>Category</label> %s', 'blogin-aarambha' ), '<span>' . single_cat_title( '', false ) . '</span>' );
        } elseif ( is_tag() ) {
            $title = sprintf( __( '<label>Tag</label> %s', 'blogin-aarambha' ), '<span>' . single_tag_title( '', false ) . '</span>' );
        } elseif ( is_author() ) {
            $title = sprintf( __( '<label>Author</label> %s', 'blogin-aarambha' ), '<span class="vcard">' . get_the_author() . '</span>' );
        } elseif ( is_year() ) {
            $title = sprintf( __( '<label>Year</label> %s', 'blogin-aarambha' ), '<span>' . get_the_date( esc_html_x( 'Y', 'yearly archives date format', 'blogin-aarambha' ) ) . '</span>' );
        } elseif ( is_month() ) {
            $title = sprintf( __( '<label>Month</label> %s', 'blogin-aarambha' ), '<span>' . get_the_date( esc_html_x( 'F Y', 'monthly archives date format', 'blogin-aarambha' ) ) . '</span>' );
        } elseif ( is_day() ) {
            $title = sprintf( __( '<label>Date</label> %s', 'blogin-aarambha' ), '<span>' . get_the_date( esc_html_x( 'F j, Y', 'daily archives date format', 'blogin-aarambha' ) ) . '</span>' );
        } elseif ( is_tax( 'post_format' ) ) {
            if ( is_tax( 'post_format', 'post-format-aside' ) ) {
                $title = esc_html_x( 'Asides', 'post format archive title', 'blogin-aarambha' );
            } elseif ( is_tax( 'post_format', 'post-format-gallery' ) ) {
                $title = esc_html_x( 'Galleries', 'post format archive title', 'blogin-aarambha' );
            } elseif ( is_tax( 'post_format', 'post-format-image' ) ) {
                $title = esc_html_x( 'Images', 'post format archive title', 'blogin-aarambha' );
            } elseif ( is_tax( 'post_format', 'post-format-video' ) ) {
                $title = esc_html_x( 'Videos', 'post format archive title', 'blogin-aarambha' );
            } elseif ( is_tax( 'post_format', 'post-format-quote' ) ) {
                $title = esc_html_x( 'Quotes', 'post format archive title', 'blogin-aarambha' );
            } elseif ( is_tax( 'post_format', 'post-format-link' ) ) {
                $title = esc_html_x( 'Links', 'post format archive title', 'blogin-aarambha' );
            } elseif ( is_tax( 'post_format', 'post-format-status' ) ) {
                $title = esc_html_x( 'Statuses', 'post format archive title', 'blogin-aarambha' );
            } elseif ( is_tax( 'post_format', 'post-format-audio' ) ) {
                $title = esc_html_x( 'Audio', 'post format archive title', 'blogin-aarambha' );
            } elseif ( is_tax( 'post_format', 'post-format-chat' ) ) {
                $title = esc_html_x( 'Chats', 'post format archive title', 'blogin-aarambha' );
            }
        } elseif ( is_post_type_archive() ) {
            $title = post_type_archive_title( '', false );
        } elseif ( is_tax() ) {
            $tax = get_taxonomy( get_queried_object()->taxonomy );
            /* translators: 1: Taxonomy singular name, 2: Current taxonomy term */
            $title = sprintf( esc_html__( '%1$s: %2$s', 'blogin-aarambha' ), $tax->labels->singular_name, '<span>' . single_term_title( '', false ) . '</span>' );
        } elseif ( is_singular() ) {
            $title = get_the_title();
        } elseif ( is_404() ) {
            $title = esc_html__( 'Error Page', 'blogin-aarambha' );
        } elseif ( is_search() ) {
            $title = get_search_query();
        } elseif ( Blogin_Aarambha_Helper::is_woocommerce() && is_shop() ) {
            $title = esc_html__( 'Shop', 'blogin-aarambha' );
        } else {
            $title = esc_html__( 'Blog', 'blogin-aarambha' );
        }
    
        /**
         * Filter the archive title.
         *
         * @param string $title Archive title to be displayed.
         */
        $title = apply_filters( 'get_the_archive_title', $title );
    
        if ( ! empty( $title ) ) {
            echo $before . $title . $after;  // WPCS: XSS OK.
        }
    }

    /**
     * Social Network Lists
     *
     * @access public
     * @return array
     */
    public static function social_network_list() {
        return [
            'facebook'		=> esc_html__( 'Facebook', 'blogin-aarambha' ),
            'twitter'		=> esc_html__( 'Twitter', 'blogin-aarambha' )
        ];
    }
    /**
     * Retrieves the post meta.
     *
     * @param int    $post_id The ID of the post.
     * @param null|array $meta_list custom post meta list
     * @return void
     */
    public static function post_meta( $post_id = null, $meta_list = null )  {

        // Require post ID.
        if ( ! $post_id ) {
            return;
        }

        /**
         * Filters post types array.
         *
         * @param array Array of post types
         */
        $disallowed_post_types = apply_filters( 'blogin_aarambha_disallowed_post_meta', array( 'page' ) );

        // Check whether the post type is allowed to output post meta.
        if ( in_array( get_post_type( $post_id ), $disallowed_post_types, true ) ) {
            return;
        }

        $post_meta = $meta_list ? $meta_list : get_theme_mod(
            'blogin_aarambha_meta_elements',
            ['categories','date']
        );

        // If the post meta setting has the value 'empty', it's explicitly empty and the default post meta shouldn't be output.
        if ( $post_meta && ! in_array( 'empty', $post_meta, true ) ) {

            // Make sure we don't output an empty container.
            $has_meta = false;

            global $post;
            $the_post = get_post( $post_id );
            setup_postdata( $the_post );
            ob_start();
            ?>
            <div class="d-flex flex-wrap entry-meta">
                <?php
                /**
                 * Fires before post meta HTML display.
                 *
                 * Allow output of additional post meta info to be added by child themes and plugins.
                 *
                 * @param int    $post_id   Post ID.
                 * @param array  $post_meta An array of post meta information.
                 */
                do_action( 'blogin_aarambha_before_post_meta_list', $post_id, $post_meta );
                ?>

                <?php foreach ( $post_meta as $meta ) : ?>

                    <?php if ( post_type_supports( get_post_type( $post_id ), 'author' ) && in_array( 'author', $post_meta, true ) && $meta == 'author' ) : $has_meta = true; ?>

                        <?php blogin_aarambha_posted_by(); ?>

                    <?php elseif ( in_array( 'date', $post_meta, true ) && $meta == 'date' ) : $has_meta = true; ?>

                        <?php blogin_aarambha_posted_on(); ?>

                    <?php elseif ( in_array( 'categories', $post_meta, true ) && $meta == 'categories' && has_category() ) : $has_meta = true; ?>

                        <?php blogin_aarambha_posted_cats(); ?>

                    <?php elseif ( in_array( 'tags', $post_meta, true ) && $meta == 'tags' && has_tag() ) : $has_meta = true; ?>

                        <?php blogin_aarambha_posted_tags(); ?>

                    <?php elseif ( in_array( 'comment', $post_meta, true ) && ! post_password_required() && ( comments_open() || get_comments_number() ) && $meta == 'comment' ) : $has_meta = true; ?>

                        <?php blogin_aarambha_comment_count(); ?>

                    <?php endif; ?>

                <?php endforeach; ?>
            </div>
            <?php
        }

    }

    /**
     * Post Comment template
     *
     * @return void
     */
    public static function post_comment() {
        // If comments are open or we have at least one comment, load up the comment template.
        if ( ! post_password_required() && ( comments_open() || get_comments_number() ) ) :
            comments_template();
        endif;
    }

    /**
     * Post Navigation
     *
     * @return void
     */
    public static function post_navigation() {

        // Only display for single post navigation
        if ( ! is_single() ) {
            return;
        }

        $next_post = get_next_post();
        $prev_post = get_previous_post();

        if ( $next_post || $prev_post ) {

            $pagination_classes = '';

            if ( ! $next_post ) {
                $pagination_classes = ' only-one only-prev';
            } elseif ( ! $prev_post ) {
                $pagination_classes = ' only-one only-next';
            }

            ?>

            <nav class="navigation post-navigation section-inner<?php echo esc_attr( $pagination_classes ); ?>" aria-label="<?php esc_attr_e( 'Post', 'blogin-aarambha' ); ?>" role="navigation">

                <h2 class="screen-reader-text"><?php esc_html_e('Post navigation','blogin-aarambha'); ?></h2>

                <div class="nav-links">

                    <?php
                    if ( $prev_post ) {
                        ?>
                        <div class="nav-previous text-left">
                            <span class="screen-reader-text"><?php esc_html_e( 'Previous Post', 'blogin-aarambha' ); ?></span>
                            <a class="previous-post" href="<?php echo esc_url( get_permalink( $prev_post->ID ) ); ?>" rel="prev">
                                <div class="nav-content-wrap">
                                    <span class="nav-title"><?php echo esc_html( get_the_title( $prev_post->ID ) ); ?></span>
                                </div><!-- .nav-content-wrap -->
                            </a>
                        </div><!-- .nav-previous -->
                        <?php
                    }

                    if ( $next_post ) {
                        ?>
                        <div class="nav-next text-right">
                            <span class="screen-reader-text"><?php esc_html_e( 'Next Post', 'blogin-aarambha' ); ?></span>
                            <a class="next-post" href="<?php echo esc_url( get_permalink( $next_post->ID ) ); ?>" rel="next">
                                <div class="nav-content-wrap">
                                    <span class="nav-title"><?php echo esc_html( get_the_title( $next_post->ID ) ); ?></span>
                                </div><!-- .nav-content-wrap -->
                            </a>
                        </div><!-- .nav-next -->
                        <?php
                    }
                    ?>

                </div><!-- .pagination-single-inner -->
            </nav><!-- .pagination-single -->

            <?php
        }
    }

    /**
     * Post Pagination
     *
     * @return void
     */
    public static function post_pagination() {

        global $wp_query;

        // Don't print empty markup if there is only one page.
        if ($wp_query->max_num_pages < 2) {
            return;
        }

        $pagination_type = get_theme_mod(
            'blogin_aarambha_blog_pagination_type',
            'nxt-prv'
        );

        if ( $pagination_type ) :

            ob_start();

            echo '<div class="pagination-wrap pagination-' . esc_attr( $pagination_type ) . '">';

            switch ( $pagination_type ) :

                case 'nxt-prv' :

                    the_posts_navigation();

                    break;

                case 'numeric' :

                    the_posts_pagination();

                    break;

            endswitch;

            echo '</div><!-- .pagination-wrap -->';

            $output = ob_get_clean();

            echo apply_filters('blogin_aarambha_pagination_markup', $output); // phpcs:ignore WordPress.Security.EscapeOutput.OutputNotEscaped

        endif;
    }

    /**
     * Author Box
     *
     * @return void
     */
    public static function author_box() {

        // Only display for standard posts
        if ( 'post' != get_post_type() ) {
            return;
        }

        // Get author data
        $author             = get_the_author();
        $author_description = get_the_author_meta( 'description' );
        $author_url         = esc_url( get_author_posts_url( get_the_author_meta( 'ID' ) ) );
        $author_avatar      = get_avatar( get_the_author_meta( 'user_email' ), apply_filters( 'blogin_aarambha_avatar_size', 150 ) );
        $author_contents 	= ['name','info'];
        ?>

            <div class="d-flex item-align-center author-info-section">
                <figure class="author-profile vcard" data-ratio="auto">
                    <a href="<?php echo esc_url( $author_url ); ?>" rel="<?php esc_attr_e( 'Author', 'blogin-aarambha'); ?>"><?php echo wp_kses_post( $author_avatar ); ?></a>
                </figure><!-- .author-avatar -->

                <?php if ( $author_contents ) : ?>
                    <div class="author-details">
                        <?php foreach ( $author_contents as $content ) :
                            switch ( $content ) :
                                case 'name' :
                                    ?>
                                    <h3 class="author-name"><a href="<?php echo esc_url( $author_url ); ?>" class="author-name" rel="<?php esc_attr_e( 'Author', 'blogin-aarambha'); ?>"><?php echo esc_html( $author ); ?></a></h3>
                                    <?php
                                    break;

                                case 'info' :
                                    ?>
                                    <div class="author-desc">
                                        <?php echo wp_kses_post( wpautop( $author_description ) ); ?>
                                    </div>
                                    <?php
                                    break;
                            endswitch;
                        endforeach; ?>
                    </div><!-- .author-details -->
                <?php endif; ?>
            </div><!-- .author-info-section -->
        <?php
    }

    /**
     * Related Posts
     *
     * @return void
     */
    public static function related_posts() {
        // Only display for standard posts
        if ( 'post' != get_post_type() ) {
            return;
        }

        global $post;
        $current_post       = $post;
        $args               = [];

        // Categories arguments
        $cats   = wp_get_post_categories( $post->ID, [ 'fields' => 'ids' ] );
        if ( ! empty( $cats ) ) {
            $args['posts_per_page']         = 4;
            $args['post__not_in']           = [ $current_post->ID ];
            $args['category__in']           = $cats;
            $args['no_found_rows']          = true;
            $args['ignore_sticky_posts']    = true;
        }

        $the_query = new WP_Query( $args );

        if ( $the_query->have_posts() ) :

            // Columns per row
            $col_per_row = [
                'desktop'           => '2',
                'tablet'            => '2',
                'mobile'            => '1'
            ];
            ?>

            <div class="related-post-section">
                <header class="entry-header heading">
                    <h2 class="entry-title"><?php esc_html_e( 'Related Posts', 'blogin-aarambha' ); ?></h2>
                </header>
                <div class="row columns"<?php Blogin_Aarambha_Helper::get_data_columns( $col_per_row );?>>

                    <?php while ( $the_query->have_posts() ) : $the_query->the_post();

                        /*
                        * Include the Post-Type-specific template for the content.
                        * If you want to override this in a child theme, then include a file
                        * called content-___.php (where ___ is the Post Type name) and that will be used instead.
                        */
                        ?>
                        <div class="column">
                            <div class="post">

                                <?php blogin_aarambha_post_thumbnail( 'large', '4x3' ); ?>

                                <div class="post-detail-wrap">

                                    <?php Blogin_Aarambha_Helper::post_meta( get_the_ID(), ['date'] ); ?>

                                    <?php the_title( sprintf( '<h2 class="entry-title"><a href="%s" rel="bookmark">', esc_url( get_permalink() ) ), '</a></h2>' ); ?>

                                    <?php Blogin_Aarambha_Helper::post_excerpt(); ?>
                                    
                                </div><!-- .post-detail-wrap -->
                            </div>
                        </div>

                    <?php endwhile; ?>

                    <?php wp_reset_postdata(); ?>

                </div>
            </div><!-- .related-post-wrapper -->
        <?php
        endif;

    }

    /**
     * Post Title
     *
     * @return void
     */
    public static function post_title() {

        if ( is_singular() ) :

            $html_tag = is_single() ? get_theme_mod('blogin_aarambha_post_title_tag',['desktop' => 'h1'] ) : get_theme_mod( 'blogin_aarambha_page_title_tag', ['desktop' => 'h1'] );

            the_title( '<' . esc_attr( $html_tag['desktop'] ) . ' class="entry-title">', '</' . esc_attr( $html_tag['desktop'] ) . '>' );

        else :
            $html_tag = get_theme_mod(
                'blogin_aarambha_blog_title_tag',
                ['desktop' => 'h2']
            );

            the_title( '<' . esc_attr( $html_tag['desktop'] ) . ' class="entry-title"><a href="' . esc_url( get_permalink() ) . '" rel="bookmark">', '</a></' . esc_attr( $html_tag['desktop'] ) . '>' );

        endif;
    }

    /**
     * Post Content
     *
     * @return void
     */
    public static function post_content() { 

        echo '<div class="entry-content">';
    
        if ( is_singular() ) :

            the_content(
                sprintf(
                    wp_kses(
                    /* translators: %s: Name of current post. Only visible to screen readers */
                        __( 'Continue reading<span class="screen-reader-text"> "%s"</span>', 'blogin-aarambha' ),
                        array(
                            'span' => array(
                                'class' => array(),
                            ),
                        )
                    ),
                    wp_kses_post( get_the_title() )
                )
            );

            wp_link_pages(
                array(
                    'before' => '<div class="page-links">' . esc_html__( 'Pages:', 'blogin-aarambha' ),
                    'after'  => '</div>',
                )
            );
            
        else :
            
            self::post_excerpt();

        endif;

        echo '</div><!-- .entry-content -->';

    }

    /**
     * Post Read More Button
     *
     * @return void
     */
    public static function read_more() {

        $btn_type = get_theme_mod(
            'blogin_aarambha_blog_read_more_type',
            ['desktop'=>'text']
        );
        $enable_arrow = get_theme_mod(
            'blogin_aarambha_blog_read_more_arrow',
            ''
        );

        $read_more_class = ['read-more'];

        if ( $btn_type && $btn_type['desktop'] == 'button' ) {
            $read_more_class[] = 'read-more-button';
        }

        if ( $enable_arrow && array_key_exists( 'desktop' , $enable_arrow ) ) {
            $read_more_class[] = 'd-flex align-items-center';
        }
        ob_start(); ?>

        <div class="d-flex justify-content-left read-more-wrap">
            <a href="<?php the_permalink( get_the_ID() ); ?>" class="<?php echo esc_attr( implode( ' ', $read_more_class ) ); ?>">
                <?php esc_html_e( 'Read More', 'blogin-aarambha' ); ?>
                <?php if ( $enable_arrow && array_key_exists( 'desktop' , $enable_arrow ) ) : ?>
                    <?php Blogin_Aarambha_Font_Awesome_Icons::get_icon( 'ui', 'fas fa-arrow-right'); ?>
                <?php endif; ?>
            </a>
        </div>

        <?php
        $output = ob_get_clean();
        echo apply_filters( 'blogin_aarambha_read_more', $output ); // phpcs:ignore WordPress.Security.EscapeOutput.OutputNotEscaped
    }

    /**
     * Banner Post Read More Button
     *
     * @return void
     */
    public static function banner_read_more() {

        $btn_type = get_theme_mod(
            'blogin_aarambha_banner_read_more_type',
            ['desktop'=>'button']
        );
        $enable_arrow = get_theme_mod(
            'blogin_aarambha_banner_read_more_arrow',
            ''
        );

        $read_more_class = ['read-more'];

        if ( $btn_type && $btn_type['desktop'] == 'button' ) {
            $read_more_class[] = 'read-more-button';
        }

        if ( $enable_arrow && array_key_exists( 'desktop' , $enable_arrow ) ) {
            $read_more_class[] = 'd-flex align-items-center';
        }
        ob_start(); ?>

        <div class="d-flex justify-content-left read-more-wrap">
            <a href="<?php the_permalink( get_the_ID() ); ?>" class="<?php echo esc_attr( implode( ' ', $read_more_class ) ); ?>">
                <?php esc_html_e( 'Discover More', 'blogin-aarambha' ); ?>
                <?php if ( $enable_arrow && array_key_exists( 'desktop' , $enable_arrow ) ) : ?>
                    <?php Blogin_Aarambha_Font_Awesome_Icons::get_icon( 'ui', 'fa-arrow-right'); ?>
                <?php endif; ?>

            </a>
        </div>

        <?php
        $output = ob_get_clean();
        echo apply_filters( 'blogin_aarambha_banner_read_more', $output ); // phpcs:ignore WordPress.Security.EscapeOutput.OutputNotEscaped
    }

    /**
     * Post Excerpt
     *
     * @return void
     */
    public static function post_excerpt() {

        $excerpt = wp_trim_words( get_the_excerpt( get_the_ID() ), '20', '...' );
        echo wp_kses_post(wpautop($excerpt));
    }

    /**
     * Add the classes into site content.
     *
     * @param string|array $class One or more classes to add to the class list.
     * @return void
     */
    public static function site_content_class(  $class = '' ) {

        $classes    = ['site-content'];

        if ( is_active_sidebar( 'sidebar-1' ) && Blogin_Aarambha_Helper::get_sidebar_layout() != 'none' ) {
            $classes[] = 'have-sidebar';
        }

        if ( ! empty( $class ) ) {
            if ( ! is_array( $class ) ) {
                $class = preg_split( '#\s+#', $class );
            }
            $classes = array_merge( $classes, $class );
        } else {
            // Ensure that we always coerce class to being an array.
            $class = array();
        }

        $classes = array_map( 'sanitize_html_class', $classes );

        /**
         * Filter site content class names
         */
        $classes = apply_filters( 'blogin_aarambha_site_content_class', $classes, $class );

        $classes = array_unique( $classes );

        echo 'class="' . esc_attr( join( ' ', $classes ) ) . '"'; // WPCS: XSS ok.
    }

    /**
     * Add the classes into sidebar widget area
     *
     * @param string|array $class One or more classes to add to the class list.
     * @return void
     */
    public static function sidebar_class(  $class = '' ) {

        $classes    = ['widget-area'];

        if ( is_active_sidebar( 'sidebar-1' ) && Blogin_Aarambha_Helper::get_sidebar_layout() ) {

            $classes[] = '' . self::get_sidebar_layout() . '-sidebar';
        }

        if ( ! empty( $class ) ) {
            if ( ! is_array( $class ) ) {
                $class = preg_split( '#\s+#', $class );
            }
            $classes = array_merge( $classes, $class );
        } else {
            // Ensure that we always coerce class to being an array.
            $class = array();
        }

        $classes = array_map( 'sanitize_html_class', $classes );

        /**
         * Filter sidebar class names
         */
        $classes = apply_filters( 'blogin_aarambha_sidebar_class', $classes, $class );

        $classes = array_unique( $classes );

        echo 'class="' . esc_attr( join( ' ', $classes ) ) . '"'; // WPCS: XSS ok.
    }


    /**
     * Add the classes into page site
     *
     * @param string|array $class One or more classes to add to the class list.
     * @return void
     */
    public static function site_class(  $class = '' ) {

        $classes = ['site'];

        if ( ! empty( $class ) ) {
            if ( ! is_array( $class ) ) {
                $class = preg_split( '#\s+#', $class );
            }
            $classes = array_merge( $classes, $class );
        } else {
            // Ensure that we always coerce class to being an array.
            $class = array();
        }

        $classes = array_map( 'sanitize_html_class', $classes );

        /**
         * Filter site class names
         */
        $classes = apply_filters( 'blogin_aarambha_site_class', $classes, $class );

        $classes = array_unique( $classes );

        echo 'class="' . esc_attr( join( ' ', $classes ) ) . '"'; // WPCS: XSS ok.
    }

    /**
     * Add the classes into primary div
     *
     * @param string|array $class One or more classes to add to the class list.
     * @return void
     */
    public static function primary_class(  $class = '' ) {

        $classes = ['content-area'];

        if ( ! empty( $class ) ) {
            if ( ! is_array( $class ) ) {
                $class = preg_split( '#\s+#', $class );
            }
            $classes = array_merge( $classes, $class );
        } else {
            // Ensure that we always coerce class to being an array.
            $class = array();
        }

        $classes = array_map( 'sanitize_html_class', $classes );

        /**
         * Filter primary class names
         */
        $classes = apply_filters( 'blogin_aarambha_primary_class', $classes, $class );

        $classes = array_unique( $classes );

        echo 'class="' . esc_attr( join( ' ', $classes ) ) . '"'; // WPCS: XSS ok.
    }
}


