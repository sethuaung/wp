<?php
/**
 * Blogin Aarambha back compat functionality
 *
 * Prevents Blogin Aarambha from running on WordPress versions prior to 4.7,
 * since this theme is not meant to be backward compatible beyond that and
 * relies on many newer functions and markup changes introduced in 5.6.
 *
 * @package Blogin_Aarambha
 */

/**
 * Prevent switching to Blogin Aarambha on old versions of WordPress.
 *
 * Switches to the default theme.
 */
function blogin_aarambha_switch_theme() {
	switch_theme( WP_DEFAULT_THEME );
	unset( $_GET['activated'] );
	add_action( 'admin_notices', 'blogin_aarambha_upgrade_notice' );
}
add_action( 'after_switch_theme', 'blogin_aarambha_switch_theme' );

/**
 * Adds a message for unsuccessful theme switch.
 *
 * Prints an update nag after an unsuccessful attempt to switch to
 * Blogin Aarambha on WordPress versions prior to 5.6.
 *
 * @global string $wp_version WordPress version.
 */
function blogin_aarambha_upgrade_notice() {
	$message = sprintf( esc_html__( 'Blogin Aarambha requires at least WordPress version 5.6. You are running version %s. Please upgrade and try again.', 'blogin-aarambha' ), $GLOBALS['wp_version'] );
	printf( '<div class="error"><p>%s</p></div>', $message );
}

/**
 * Prevents the Customizer from being loaded on WordPress versions prior to 5.6.
 *
 * @global string $wp_version WordPress version.
 */
function blogin_aarambha_customize() {
	wp_die( sprintf( esc_html__( 'Blogin Aarambha requires at least WordPress version 5.6. You are running version %s. Please upgrade and try again.', 'blogin-aarambha' ), $GLOBALS['wp_version'] ), '', array(
		'back_link' => true,
	) );
}
add_action( 'load-customize.php', 'blogin_aarambha_customize' );

/**
 * Prevents the Theme Preview from being loaded on WordPress versions prior to 5.6.
 *
 * @global string $wp_version WordPress version.
 */
function blogin_aarambha_preview() {
	if ( isset( $_GET['preview'] ) ) {
		wp_die( sprintf( esc_html__( 'Blogin Aarambha requires at least WordPress version 5.6. You are running version %s. Please upgrade and try again.', 'blogin-aarambha' ), $GLOBALS['wp_version'] ) );
	}
}
add_action( 'template_redirect', 'blogin_aarambha_preview' );
