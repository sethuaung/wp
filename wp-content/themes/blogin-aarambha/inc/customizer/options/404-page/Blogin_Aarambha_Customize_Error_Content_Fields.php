<?php
/**
 * Blogin Aarambha Theme Customizer 404 Page Content settings
 *
 * @package Blogin_Aarambha
 */

class Blogin_Aarambha_Customize_Error_Content_Fields extends Blogin_Aarambha_Customize_Base_Field {

    /**
     * Arguments for fields.
     *
     * @return void
     */
    public function init() {
        $this->args = [
			// Grouping Settings
			'blogin_aarambha_error_content_grouping_settings' => [
				'type'              => 'group',
				'section'           => 'blogin_aarambha_error_content_section',
				'priority'          => 10,
				'choices'           => [
					'normal'            => array(
						'tab-title'     => esc_html__( 'General', 'blogin-aarambha' ),
						'controls'      => array(
							'blogin_aarambha_error_content_elements',
							'blogin_aarambha_error_content_image'
						)
					),
					'hover'         => array(
						'tab-title'     => esc_html__( 'Style', 'blogin-aarambha' ),
						'controls'      => array(
							'blogin_aarambha_error_container_background',
						)
					)
				]
			],
            // Error Page Content
            'blogin_aarambha_error_content_elements' => [
                'type'              => 'sortable',
                'default'           => ['title','subtitle','button'],
                'sanitize_callback' => ['Blogin_Aarambha_Customizer_Sanitize_Callback', 'sanitize_sortable' ],
                'label'             => esc_html__( 'Sort Elements', 'blogin-aarambha' ),
                'description'       => esc_html__( 'Enable page content elements and order their list with drag and drop.', 'blogin-aarambha' ),
                'section'           => 'blogin_aarambha_error_content_section',
                'priority'          => 15,
                'choices'           => [
                    'image'             => esc_html__( 'Image', 'blogin-aarambha' ),
                    'title'             => esc_html__( 'Title', 'blogin-aarambha' ),
                    'subtitle'          => esc_html__( 'Sub Title', 'blogin-aarambha' ),
                    'button'            => esc_html__( 'Button', 'blogin-aarambha' ),
                    'search'            => esc_html__( 'Search', 'blogin-aarambha' )
                ],
            ],
            // Image
            'blogin_aarambha_error_content_image' => [
                'type'              => 'image',
                'default'           => '',
                'sanitize_callback' => 'esc_url_raw',
                'label'             => esc_html__( 'Image', 'blogin-aarambha' ),
                'section'           => 'blogin_aarambha_error_content_section',
                'priority'          => 20,
            ],
			// Background Image
			'blogin_aarambha_error_container_background' => [
				'type'              => 'background',
				'default'           => '',
				'sanitize_callback' => ['Blogin_Aarambha_Customizer_Sanitize_Callback', 'sanitize_background' ],
				'label'             => esc_html__( 'Background Image', 'blogin-aarambha' ),
				'description'       => esc_html__( 'Set background image for 404 page content.', 'blogin-aarambha' ),
				'section'           => 'blogin_aarambha_error_content_section',
				'priority'          => 75,
				'fields'            => ['image' => true, 'position' => true, 'attachment' => true, 'repeat' => true, 'size' => true ],
			],
        ];
    }

}
new Blogin_Aarambha_Customize_Error_Content_Fields();
