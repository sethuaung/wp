<?php
/**
 * Blogin Aarambha Theme Customizer Single Post Title settings
 *
 * @package Blogin_Aarambha
 */

class Blogin_Aarambha_Customize_Post_Title_Fields extends Blogin_Aarambha_Customize_Base_Field {

    /**
     * Arguments for fields.
     *
     * @return void
     */
    public function init() {
        $this->args = [
            // Title Tag
            'blogin_aarambha_post_title_tag' => [
                'type'              => 'buttonset',
                'default'           => ['desktop' => 'h1'],
                'sanitize_callback' => ['Blogin_Aarambha_Customizer_Sanitize_Callback', 'sanitize_buttonset' ],
                'label'             => esc_html__( 'Heading Tag', 'blogin-aarambha' ),
                'section'           => 'blogin_aarambha_post_title_section',
                'priority'          => 15,
                'choices' 			=> array(
                    'h1'                => esc_html__( 'H1', 'blogin-aarambha' ),
                    'h2'                => esc_html__( 'H2', 'blogin-aarambha' ),
                    'h3'                => esc_html__( 'H3', 'blogin-aarambha' ),
                    'h4'                => esc_html__( 'H4', 'blogin-aarambha' ),
                    'h5'                => esc_html__( 'H5', 'blogin-aarambha' ),
                    'h6'                => esc_html__( 'H6', 'blogin-aarambha' )
                )
            ]
        ];
    }

}
new Blogin_Aarambha_Customize_Post_Title_Fields();
