<?php
/**
 * Blogin Aarambha Theme Customizer Single Post Element settings
 *
 * @package Blogin_Aarambha
 */

class Blogin_Aarambha_Customize_Post_Content_Fields extends Blogin_Aarambha_Customize_Base_Field {

    /**
     * Arguments for fields.
     *
     * @return void
     */
    public function init() {
        $this->args = [
            // Content Elements
            'blogin_aarambha_post_content_elements' => [
                'type'              => 'sortable',
                'default'           => ['post-content','cats-tags'],
                'sanitize_callback' => ['Blogin_Aarambha_Customizer_Sanitize_Callback', 'sanitize_sortable' ],
                'label'             => esc_html__( 'Elements', 'blogin-aarambha' ),
                'description'       => esc_html__( 'Enable lists of elements and rearrange the vertical order by drag and drop.', 'blogin-aarambha' ),
                'section'           => 'blogin_aarambha_post_content_section',
                'priority'          => 10,
                'choices'           => [
                    'post-content'      => esc_html__( 'Post Content', 'blogin-aarambha' ),
                    'cats-tags'         => esc_html__( 'Categories & Tags', 'blogin-aarambha' ),
                ]
            ],
            // Content After Elements
            'blogin_aarambha_post_after_content' => [
                'type'              => 'sortable',
                'default'           => ['post-comments','post-navigation'],
                'sanitize_callback' => ['Blogin_Aarambha_Customizer_Sanitize_Callback', 'sanitize_sortable' ],
                'label'             => esc_html__( 'After Content', 'blogin-aarambha' ),
                'description'       => esc_html__( 'Enable lists of elements and rearrange the vertical order by drag and drop.', 'blogin-aarambha' ),
                'section'           => 'blogin_aarambha_post_content_section',
                'priority'          => 25,
                'choices'           => [
                    'post-comments'     => esc_html__( 'Comments', 'blogin-aarambha' ),
                    'post-navigation'   => esc_html__( 'Post Navigation', 'blogin-aarambha' ),
                    'author-box'        => esc_html__( 'Author Box', 'blogin-aarambha' ),
                    'related-posts'     => esc_html__( 'Related Posts', 'blogin-aarambha' )
                ]
            ],
        ];
    }

}
new Blogin_Aarambha_Customize_Post_Content_Fields();
