<?php
/**
 * Blogin Aarambha Theme Customizer Single Post Sidebar settings
 *
 * @package Blogin_Aarambha
 */

class Blogin_Aarambha_Customize_Post_Sidebar_Fields extends Blogin_Aarambha_Customize_Base_Field {

    /**
     * Arguments for fields.
     *
     * @return void
     */
    public function init() {
        $this->args = [
            // Sidebar
            'blogin_aarambha_post_sidebar_layout' => [
                'type'              => 'radio_image',
                'default'           => 'right',
                'sanitize_callback' => ['Blogin_Aarambha_Customizer_Sanitize_Callback', 'sanitize_choices' ],
                'label'             => esc_html__( 'Sidebar', 'blogin-aarambha' ),
                'description'       => esc_html__( 'Choose Sidebar Layout for single post.', 'blogin-aarambha' ),
                'section'           => 'blogin_aarambha_post_sidebar_section',
                'priority'          => 15,
                'choices' 			=> array(
                    'left'              => BLOGIN_AARAMBHA_URI . 'assets/images/left.svg',
                    'right'  		    => BLOGIN_AARAMBHA_URI . 'assets/images/right.svg',
                    'none'  		    => BLOGIN_AARAMBHA_URI . 'assets/images/none.svg',
                ),
                'l10n'              => [
                    'left'              => esc_html__( 'Left', 'blogin-aarambha' ),
                    'right'             => esc_html__( 'Right', 'blogin-aarambha' ),
                    'none'              => esc_html__( 'None', 'blogin-aarambha' )
                ]
            ],
        ];
    }

}
new Blogin_Aarambha_Customize_Post_Sidebar_Fields();