<?php
/**
 * Blogin Aarambha Theme Customizer Single Page Element settings
 *
 * @package Blogin_Aarambha
 */

class Blogin_Aarambha_Customize_Page_Content_Fields extends Blogin_Aarambha_Customize_Base_Field {

    /**
     * Arguments for fields.
     *
     * @return void
     */
    public function init() {
        $this->args = [
            // Content Elements
            'blogin_aarambha_page_content_elements' => [
                'type'              => 'sortable',
                'default'           => ['page-content'],
                'sanitize_callback' => ['Blogin_Aarambha_Customizer_Sanitize_Callback', 'sanitize_sortable' ],
                'label'             => esc_html__( 'Elements', 'blogin-aarambha' ),
                'description'       => esc_html__( 'Enable lists of elements and rearrange the vertical order by drag and drop.', 'blogin-aarambha' ),
                'section'           => 'blogin_aarambha_page_content_section',
                'priority'          => 10,
                'choices'           => [
                    'page-content'      => esc_html__( 'Page Content', 'blogin-aarambha' ),
                ]
            ],
            // After Content
            'blogin_aarambha_page_after_content' => [
                'type'              => 'sortable',
                'default'           => ['post-comments'],
                'sanitize_callback' => ['Blogin_Aarambha_Customizer_Sanitize_Callback', 'sanitize_sortable' ],
                'label'             => esc_html__( 'After Content', 'blogin-aarambha' ),
                'description'       => esc_html__( 'Enable/Disable comment from page content area.', 'blogin-aarambha' ),
                'section'           => 'blogin_aarambha_page_content_section',
                'priority'          => 25,
                'choices'           => [
                    'post-comments'     => esc_html__( 'Comments', 'blogin-aarambha' )
                ]
            ],
        ];
    }

}
new Blogin_Aarambha_Customize_Page_Content_Fields();
