<?php
/**
 * Blogin Aarambha Theme Customizer Single Page Header settings
 *
 * @package Blogin_Aarambha
 */

class Blogin_Aarambha_Customize_Page_Header_Fields extends Blogin_Aarambha_Customize_Base_Field {

    /**
     * Arguments for fields.
     *
     * @return void
     */
    public function init() {
        $this->args = [
            // Page Header
            'blogin_aarambha_page_header_elements' => [
                'type'              => 'sortable',
                'default'           => ['post-image','post-title'],
                'sanitize_callback' => ['Blogin_Aarambha_Customizer_Sanitize_Callback', 'sanitize_sortable' ],
                'label'             => esc_html__( 'Elements', 'blogin-aarambha' ),
                'description'       => esc_html__( 'Enable lists of elements and rearrange the vertical order by drag and drop.', 'blogin-aarambha' ),
                'section'           => 'blogin_aarambha_page_header_section',
                'priority'          => 15,
                'choices'           => [
                    'post-title'        => esc_html__( 'Page Title', 'blogin-aarambha' ),
                    'post-image'        => esc_html__( 'Featured Image', 'blogin-aarambha' )
                ],
            ]
        ];
    }

}
new Blogin_Aarambha_Customize_Page_Header_Fields();
