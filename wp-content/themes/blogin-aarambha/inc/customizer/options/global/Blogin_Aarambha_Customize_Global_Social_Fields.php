<?php
/**
 * Blogin Aarambha Theme Customizer Social settings
 *
 * @package Blogin_Aarambha
 */

class Blogin_Aarambha_Customize_Global_Social_Fields extends Blogin_Aarambha_Customize_Base_Field {

	/**
	 * Arguments for fields.
	 *
	 * @return void
	 */
	public function init() {
		$this->args = [
			// Heading One
			'blogin_aarambha_social_icon_note' => [
				'type'              => 'heading',
				'label'             => esc_html__( 'SOCIAL ICONS', 'blogin-aarambha' ),
				'section'           => 'blogin_aarambha_social_section',
				'priority'          => 5,
			]
		];
	}

}
new Blogin_Aarambha_Customize_Global_Social_Fields();
