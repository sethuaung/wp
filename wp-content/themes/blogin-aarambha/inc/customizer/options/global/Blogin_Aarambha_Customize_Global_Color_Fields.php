<?php
/**
 * Blogin Aarambha Theme Customizer Color settings
 *
 * @package Blogin_Aarambha
 */

class Blogin_Aarambha_Customize_Global_Color_Fields extends Blogin_Aarambha_Customize_Base_Field {

    /**
     * Arguments for fields.
     *
     * @return void
     */
    public function init() {
        $this->args = [
            // Accent Color
            'blogin_aarambha_accent_color' => [
                'type'              => 'color',
                'default'           => '',
                'sanitize_callback' => ['Blogin_Aarambha_Customizer_Sanitize_Callback', 'sanitize_color' ],
                'label'             => esc_html__( 'Accent', 'blogin-aarambha' ),
                'section'           => 'colors',
                'priority'          => 10,
                'colors'            => [
                    'color_1'           => esc_html__( 'Primary', 'blogin-aarambha' ),
                    'color_2'           => esc_html__( 'Secondary', 'blogin-aarambha' )
                ],
                'inherits'            => [
                    'color_1'           => 'var(--color-accent)',
                    'color_2'           => 'var(--color-accent-secondary)'
                ]
            ],
            // H1-H6 Color
            'blogin_aarambha_heading_color' => [
                'type'              => 'color',
                'default'           => '',
                'sanitize_callback' => ['Blogin_Aarambha_Customizer_Sanitize_Callback', 'sanitize_color' ],
                'label'             => esc_html__( 'H1 -H6', 'blogin-aarambha' ),
                'section'           => 'colors',
                'priority'          => 15,
                'inherits'            => [
                    'color_1'           => 'var(--color-heading)'
                ]
            ],
            // Text Color
            'blogin_aarambha_text_color' => [
                'type'              => 'color',
                'default'           => '',
                'sanitize_callback' => ['Blogin_Aarambha_Customizer_Sanitize_Callback', 'sanitize_color' ],
                'label'             => esc_html__( 'Base Text', 'blogin-aarambha' ),
                'section'           => 'colors',
                'priority'          => 20,
                'colors'            => [
                    'color_1'           => esc_html__( 'Color 1', 'blogin-aarambha' )
                ],
                'inherits'            => [
                    'color_1'           => 'var(--color-1)'
                ]
            ],
            // Link Color
            'blogin_aarambha_link_color' => [
                'type'              => 'color',
                'default'           => '',
                'sanitize_callback' => ['Blogin_Aarambha_Customizer_Sanitize_Callback', 'sanitize_color' ],
                'label'             => esc_html__( 'Link', 'blogin-aarambha' ),
                'section'           => 'colors',
                'priority'          => 25,
                'colors'            => [
                    'color_1'           => esc_html__( 'Normal', 'blogin-aarambha' ),
                    'color_2'           => esc_html__( 'Hover', 'blogin-aarambha' ),
                    'color_3'           => esc_html__( 'Visited', 'blogin-aarambha' )
                ],
                'inherits'            => [
                    'color_1'           => 'var(--color-accent-secondary)',
                    'color_2'           => 'var(--color-accent)',
                    'color_3'           => 'var(--color-1)'
                ]
            ]
        ];
    }

}
new Blogin_Aarambha_Customize_Global_Color_Fields();
