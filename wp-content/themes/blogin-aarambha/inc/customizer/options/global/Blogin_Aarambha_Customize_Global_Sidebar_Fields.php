<?php
/**
 * Blogin Aarambha Theme Customizer Sidebar settings
 *
 * @package Blogin_Aarambha
 */

class Blogin_Aarambha_Customize_Global_Sidebar_Fields extends Blogin_Aarambha_Customize_Base_Field {

    /**
     * Arguments for fields.
     *
     * @return void
     */
    public function init() {
        $this->args = [
            // Sticky Sidebar
            'blogin_aarambha_sidebar_sticky' => [
                'type'              => 'toggle',
                'default'           => '',
                'sanitize_callback' => ['Blogin_Aarambha_Customizer_Sanitize_Callback', 'sanitize_toggle' ],
                'label'             => esc_html__( 'Sticky Sidebar', 'blogin-aarambha' ),
                'description'       => esc_html__( 'Toggle to enable sticky sidebar. See the effect on content scrolling.', 'blogin-aarambha' ),
                'section'           => 'blogin_aarambha_sidebar_section',
                'priority'          => 15,
            ]
        ];
    }

}
new Blogin_Aarambha_Customize_Global_Sidebar_Fields();
