<?php
/**
 * Blogin Aarambha Theme Customizer Button settings
 *
 * @package Blogin_Aarambha
 */

class Blogin_Aarambha_Customize_Global_Button_Fields extends Blogin_Aarambha_Customize_Base_Field {

    /**
     * Arguments for fields.
     *
     * @return void
     */
    public function init() {
        $this->args = [
            // Color
            'blogin_aarambha_button_color' => [
                'type'              => 'color',
                'default'           => '',
                'sanitize_callback' => ['Blogin_Aarambha_Customizer_Sanitize_Callback', 'sanitize_color' ],
                'label'             => esc_html__( 'Font Color', 'blogin-aarambha' ),
                'section'           => 'blogin_aarambha_button_section',
                'priority'          => 15,
                'colors'            => [
                    'color_1'           => esc_html__( 'Normal', 'blogin-aarambha' ),
                    'color_2'           => esc_html__( 'Hover', 'blogin-aarambha' ),
                ],
                'inherits'            => [
                    'color_1'           => 'var(--color-link-hover)',
                    'color_2'           => 'var(--color-link)',
                ]
            ],
            // Background Color
            'blogin_aarambha_button_bg_color' => [
                'type'              => 'color',
                'default'           => '',
                'sanitize_callback' => ['Blogin_Aarambha_Customizer_Sanitize_Callback', 'sanitize_color' ],
                'label'             => esc_html__( 'Background Color', 'blogin-aarambha' ),
                'section'           => 'blogin_aarambha_button_section',
                'priority'          => 15,
                'colors'            => [
                    'color_1'           => esc_html__( 'Normal', 'blogin-aarambha' ),
                    'color_2'           => esc_html__( 'Hover', 'blogin-aarambha' ),
                ],
                'inherits'            => [
                    'color_1'           => 'var(--color-bg-3)',
                    'color_2'           => 'var(--color-accent-secondary)',
                ]
            ],
            // Border
            'blogin_aarambha_button_border' => [
                'type'              => 'border',
                'default'           => '',
                'sanitize_callback' => ['Blogin_Aarambha_Customizer_Sanitize_Callback', 'sanitize_border' ],
                'label'             => esc_html__( 'Border', 'blogin-aarambha' ),
                'section'           => 'blogin_aarambha_button_section',
                'priority'          => 15,
				'fields'            => ['radius'=>true],
            ],
        ];
    }

}
new Blogin_Aarambha_Customize_Global_Button_Fields();
