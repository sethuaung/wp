<?php
/**
 * Blogin Aarambha Theme Customizer Site Content Settings
 *
 * @package Blogin_Aarambha
 */

class Blogin_Aarambha_Customize_Global_Site_Content_Fields extends Blogin_Aarambha_Customize_Base_Field {

    /**
     * Arguments for fields.
     *
     * @return void
     */
    public function init() {


        $this->args = [
            // Max Width
            'blogin_aarambha_site_max_width' => [
                'type'              => 'range',
                'default'           => ['desktop' => '1170px'],
                'section'           => 'blogin_aarambha_site_content_section',
                'priority'          => 15,
                'sanitize_callback' => ['Blogin_Aarambha_Customizer_Sanitize_Callback', 'sanitize_range' ],
                'label'             => esc_html__( 'Max Width', 'blogin-aarambha' ),
                'description'       => esc_html__( 'Set Max width for container. Default value is 1170px.', 'blogin-aarambha' ),
                'input_attrs'       => [
                    'min'               => 0,
                    'max'               => 2000
                ]
            ]
        ];
    }

}
new Blogin_Aarambha_Customize_Global_Site_Content_Fields();
