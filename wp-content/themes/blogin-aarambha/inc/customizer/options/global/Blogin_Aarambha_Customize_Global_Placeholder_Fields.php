<?php
/**
 * Blogin Aarambha Theme Customizer Placeholder settings
 *
 * @package Blogin_Aarambha
 */

class Blogin_Aarambha_Customize_Global_Placeholder_Fields extends Blogin_Aarambha_Customize_Base_Field {

    /**
     * Arguments for fields.
     *
     * @return void
     */
    public function init() {
        $this->args = [
            // Color
            'blogin_aarambha_placeholder_color' => [
                'type'              => 'color',
                'default'           => [
                    'color_1'           => '#dbdcdf'
                ],
                'sanitize_callback' => ['Blogin_Aarambha_Customizer_Sanitize_Callback', 'sanitize_color' ],
                'label'             => esc_html__( 'Placeholder Color', 'blogin-aarambha' ),
                'description'       => esc_html__( 'Set color in placeholder if there isn’t a featured image.', 'blogin-aarambha' ),
                'section'           => 'blogin_aarambha_placeholder_section',
                'priority'          => 10,
            ],
            // Image
            'blogin_aarambha_placeholder_image' => [
                'type'              => 'image',
                'default'           => '',
                'sanitize_callback' => 'esc_url_raw',
                'label'             => esc_html__( 'Placeholder Image', 'blogin-aarambha' ),
                'description'       => esc_html__( 'Set placeholder image for no featured image. It will replace the placeholder color.', 'blogin-aarambha' ),
                'section'           => 'blogin_aarambha_placeholder_section',
                'priority'          => 15,
            ]
        ];
    }

}
new Blogin_Aarambha_Customize_Global_Placeholder_Fields();
