<?php
/**
 * Blogin Aarambha Theme Customizer Body settings
 *
 * @package Blogin_Aarambha
 */

class Blogin_Aarambha_Customize_Global_Body_Fields extends Blogin_Aarambha_Customize_Base_Field {

    /**
     * Arguments for fields.
     *
     * @return void
     */
    public function init() {
        // Background
        $this->args = [
            'blogin_aarambha_body_background' => [
                'type'              => 'background',
                'default'           => '',
                'sanitize_callback' => ['Blogin_Aarambha_Customizer_Sanitize_Callback', 'sanitize_background' ],
                'label'             => esc_html__( 'Background', 'blogin-aarambha' ),
                'description'       => esc_html__( 'Color or Image as the background of your site.', 'blogin-aarambha' ),
                'section'           => 'blogin_aarambha_body_section',
                'priority'          => 10,
                'inherits'          => [
                    'color_1'           => 'var(--color-bg-1)',
                ],
				'fields'            => ['background' => true, 'colors' => true,'image' => true, 'position' => true, 'attachment' => true, 'repeat' => true, 'size' => true],
            ]
        ];
    }

}
new Blogin_Aarambha_Customize_Global_Body_Fields();
