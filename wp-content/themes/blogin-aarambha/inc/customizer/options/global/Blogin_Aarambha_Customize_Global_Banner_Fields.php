<?php
/**
 * Blogin Aarambha Theme Customizer Banner settings
 *
 * @package Blogin_Aarambha
 */

class Blogin_Aarambha_Customize_Global_Banner_Fields extends Blogin_Aarambha_Customize_Base_Field {

    /**
     * Arguments for fields.
     *
     * @return void
     */
    public function init() {
        $this->args = [
            // Text Color
            'blogin_aarambha_banner_text_color' => [
                'type'              => 'color',
                'default'           => '',
                'sanitize_callback' => ['Blogin_Aarambha_Customizer_Sanitize_Callback', 'sanitize_color' ],
                'label'             => esc_html__( 'Text', 'blogin-aarambha' ),
                'section'           => 'blogin_aarambha_banner_section',
                'priority'          => 10,
                'colors'            => [
                    'color_1'           => esc_html__( 'Color 1', 'blogin-aarambha' )
                ],
                'inherits'            => [
                    'color_1'           => 'var(--color-2)'
                ]
            ],
            // Link Color
            'blogin_aarambha_banner_link_color' => [
                'type'              => 'color',
                'default'           => '',
                'sanitize_callback' => ['Blogin_Aarambha_Customizer_Sanitize_Callback', 'sanitize_color' ],
                'label'             => esc_html__( 'Link', 'blogin-aarambha' ),
                'section'           => 'blogin_aarambha_banner_section',
                'priority'          => 15,
                'colors'            => [
                    'color_1'           => esc_html__( 'Normal', 'blogin-aarambha' ),
                    'color_2'           => esc_html__( 'Hover', 'blogin-aarambha' ),
                ],
                'inherits'            => [
                    'color_1'           => 'var(--color-2)',
                    'color_2'           => 'var(--color-2)',
                ]
            ],
            // Background Image
			'blogin_aarambha_banner_background' => [
				'type'              => 'background',
				'default'           => '',
				'sanitize_callback' => ['Blogin_Aarambha_Customizer_Sanitize_Callback', 'sanitize_background' ],
				'label'             => esc_html__( 'Background Image', 'blogin-aarambha' ),
				'description'       => esc_html__( 'To set the background image banner container.', 'blogin-aarambha' ),
				'section'           => 'blogin_aarambha_banner_section',
				'priority'          => 25,
				'fields'            => ['image' => true, 'position' => true, 'attachment' => true, 'repeat' => true, 'size' => true ],
			],
			// Background Overlay
			'blogin_aarambha_banner_background_overlay' => [
				'type'              => 'background',
				'default'           => '',
				'sanitize_callback' => ['Blogin_Aarambha_Customizer_Sanitize_Callback', 'sanitize_background' ],
				'label'             => esc_html__( 'Background Overlay', 'blogin-aarambha' ),
				'description'       => esc_html__( 'Set Background overlay color on banner container.', 'blogin-aarambha' ),
				'section'           => 'blogin_aarambha_banner_section',
				'priority'          => 30,
				'inherits'          => [
					'color_1'           => 'var(--color-bg-2)'
				],
				'fields'            => ['colors' => true],
			]
        ];

    }

}
new Blogin_Aarambha_Customize_Global_Banner_Fields();
