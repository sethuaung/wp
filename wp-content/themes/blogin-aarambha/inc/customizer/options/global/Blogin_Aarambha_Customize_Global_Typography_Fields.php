<?php
/**
 * Blogin Aarambha Theme Customizer Typography settings
 *
 * @package Blogin_Aarambha
 */

class Blogin_Aarambha_Customize_Global_Typography_Fields extends Blogin_Aarambha_Customize_Base_Field {

    /**
     * Arguments for fields.
     *
     * @return void
     */
    public function init() {
        $this->args = [
            // Base Typography
            'blogin_aarambha_base_typography' => [
                'type'              => 'typography',
                'default'           => ['font_family'=>'Crimson Text'],
                'sanitize_callback' => ['Blogin_Aarambha_Customizer_Sanitize_Callback', 'sanitize_typography' ],
                'label'             => esc_html__( 'Base', 'blogin-aarambha' ),
                'description'       => esc_html__( 'Set Typography for the base of your website.', 'blogin-aarambha' ),
                'section'           => 'blogin_aarambha_typography_section',
                'priority'          => 10,
                'fields'            => ['font_family'=>true,'font_variant'=>true]
            ],
            // Heading Typography
            'blogin_aarambha_heading_typography' => [
                'type'              => 'typography',
                'default'           => ['font_family'=>'Playfair Display'],
                'sanitize_callback' => ['Blogin_Aarambha_Customizer_Sanitize_Callback', 'sanitize_typography' ],
                'label'             => esc_html__( 'H1 - H6', 'blogin-aarambha' ),
                'description'       => esc_html__( 'Set heading H1 - H6 typography for page content.', 'blogin-aarambha' ),
                'section'           => 'blogin_aarambha_typography_section',
                'priority'          => 10,
                'units'             => [ 'px', 'rem', 'pt', 'em','vw' ],
                'responsive'        => [ 'desktop', 'tablet', 'mobile' ],
                'fields'            => ['font_family'=>true,'font_variant'=>true]
            ],
        ];

    }

}
new Blogin_Aarambha_Customize_Global_Typography_Fields();
