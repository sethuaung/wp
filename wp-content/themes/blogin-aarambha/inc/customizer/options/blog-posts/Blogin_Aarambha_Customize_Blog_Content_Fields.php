<?php
/**
 * Blogin Aarambha Theme Customizer Blog Posts Layout settings
 *
 * @package Blogin_Aarambha
 */

class Blogin_Aarambha_Customize_Blog_Content_Fields extends Blogin_Aarambha_Customize_Base_Field {

    /**
     * Arguments for fields.
     *
     * @return void
     */
    public function init() {
        $this->args = [
            // Posts Elements
            'blogin_aarambha_blog_content_elements' => [
                'type'              => 'sortable',
                'default'           => ['post-excerpt','read-more'],
                'sanitize_callback' => ['Blogin_Aarambha_Customizer_Sanitize_Callback', 'sanitize_sortable' ],
                'label'             => esc_html__( 'Elements', 'blogin-aarambha' ),
				'description'       => esc_html__( 'Enable lists of elements and rearrange the vertical order by drag and drop.', 'blogin-aarambha' ),
                'section'           => 'blogin_aarambha_blog_content_section',
                'priority'          => 10,
                'choices'           => [
                    'post-excerpt'      => esc_html__( 'Post Excerpt', 'blogin-aarambha' ),
                    'read-more'         => esc_html__( 'Read More', 'blogin-aarambha' ),
                    'cats-tags'         => esc_html__( 'Categories & Tags', 'blogin-aarambha' ),
                ],
            ],
        ];
    }

}
new Blogin_Aarambha_Customize_Blog_Content_Fields();
