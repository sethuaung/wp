<?php
/**
 * Blogin Aarambha Theme Customizer Blog Post Featured Image settings
 *
 * @package Blogin_Aarambha
 */

class Blogin_Aarambha_Customize_Blog_Image_Fields extends Blogin_Aarambha_Customize_Base_Field {

    /**
     * Arguments for fields.
     *
     * @return void
     */
    public function init() {
        $this->args = [
            // Image Size
            'blogin_aarambha_blog_image_size' => [
                'type'              => 'buttonset',
                'default'           => ['desktop' => 'medium_large'],
                'sanitize_callback' => ['Blogin_Aarambha_Customizer_Sanitize_Callback', 'sanitize_buttonset' ],
                'label'             => esc_html__( 'Image Size', 'blogin-aarambha' ),
                'description'       => esc_html__( 'Set proper size for featured image. Selecting a bigger image size may display a better appearance but takes more time on loading websites.', 'blogin-aarambha' ),
                'section'           => 'blogin_aarambha_blog_image_section',
                'priority'          => 20,
                'choices' 			=> [
                    'thumbnail'         => esc_html__( 'Small', 'blogin-aarambha' ),
                    'medium'            => esc_html__( 'Medium', 'blogin-aarambha' ),
                    'medium_large'      => esc_html__( 'Medium Large', 'blogin-aarambha' ),
                    'large'             => esc_html__( 'Large', 'blogin-aarambha' ),
                ]
            ]
        ];
    }

}
new Blogin_Aarambha_Customize_Blog_Image_Fields();
