<?php
/**
 * Blogin Aarambha Theme Customizer Blog Page Banner settings
 *
 * @package Blogin_Aarambha
 */

class Blogin_Aarambha_Customize_Blog_Banner_Fields extends Blogin_Aarambha_Customize_Base_Field {

    /**
     * Arguments for fields.
     *
     * @return void
     */
    public function init() {
        $this->args = [
            // Post Banner
            'blogin_aarambha_blog_banner_elements' => [
                'type'              => 'sortable',
                'default'           => ['post-title'],
                'sanitize_callback' => ['Blogin_Aarambha_Customizer_Sanitize_Callback', 'sanitize_sortable' ],
                'label'             => esc_html__( 'Elements', 'blogin-aarambha' ),
                'description'       => esc_html__( 'Enable lists of elements and rearrange the vertical order by drag and drop.', 'blogin-aarambha' ),
                'section'           => 'blogin_aarambha_blog_banner_section',
                'priority'          => 10,
                'choices'           => [
                    'post-title'        => esc_html__( 'Page Title', 'blogin-aarambha' ),
                    'post-desc'         => esc_html__( 'Description', 'blogin-aarambha' ),
                    'breadcrumb'        => esc_html__( 'Breadcrumb', 'blogin-aarambha' )
                ],
            ],
        ];
    }

}
new Blogin_Aarambha_Customize_Blog_Banner_Fields();
