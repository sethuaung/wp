<?php
/**
 * Blogin Aarambha Theme Customizer Blog Pagination settings
 *
 * @package Blogin_Aarambha
 */

class Blogin_Aarambha_Customize_Blog_Pagination_Fields extends Blogin_Aarambha_Customize_Base_Field {

    /**
     * Arguments for fields.
     *
     * @return void
     */
    public function init() {
        $this->args = [
            // Pagination Type
            'blogin_aarambha_blog_pagination_type' => [
                'type'              => 'select',
                'default'           => 'nxt-prv',
                'sanitize_callback' => ['Blogin_Aarambha_Customizer_Sanitize_Callback', 'sanitize_choices' ],
                'label'             => esc_html__( 'Pagination Type', 'blogin-aarambha' ),
                'section'           => 'blogin_aarambha_blog_pagination_section',
                'priority'          => 15,
                'choices'           => [
                    'nxt-prv'           => esc_html__( 'Next/Prev', 'blogin-aarambha' ),
                    'numeric'           => esc_html__( 'Numeric', 'blogin-aarambha' )
                ]
            ]
        ];
    }

}
new Blogin_Aarambha_Customize_Blog_Pagination_Fields();
