<?php
/**
 * Blogin Aarambha Theme Customizer Blog Post Read More settings
 *
 * @package Blogin_Aarambha
 */

class Blogin_Aarambha_Customize_Blog_Read_More_Fields extends Blogin_Aarambha_Customize_Base_Field {

    /**
     * Arguments for fields.
     *
     * @return void
     */
    public function init() {
        $this->args = [
            // Type
            'blogin_aarambha_blog_read_more_type' => [
                'type'              => 'buttonset',
                'default'           => ['desktop'=>'text'],
                'sanitize_callback' => ['Blogin_Aarambha_Customizer_Sanitize_Callback', 'sanitize_buttonset' ],
                'label'             => esc_html__( 'Display as', 'blogin-aarambha' ),
                'section'           => 'blogin_aarambha_blog_read_more_section',
                'priority'          => 20,
                'choices'           => [
                    'text'              => esc_html__( 'Text', 'blogin-aarambha' ),
                    'button'            => esc_html__( 'Button', 'blogin-aarambha' )
                ]
            ],
            // Button Arrow
            'blogin_aarambha_blog_read_more_arrow' => [
                'type'              => 'toggle',
                'default'           => '',
                'sanitize_callback' => ['Blogin_Aarambha_Customizer_Sanitize_Callback', 'sanitize_toggle' ],
                'label'             => esc_html__( 'Button Arrow', 'blogin-aarambha' ),
                'description'       => esc_html__( 'Enable Arrow Icon after Button/Text.', 'blogin-aarambha' ),
                'section'           => 'blogin_aarambha_blog_read_more_section',
                'priority'          => 25,
            ]
        ];
    }

}
new Blogin_Aarambha_Customize_Blog_Read_More_Fields();
