<?php
/**
 * Blogin Aarambha Theme Customizer
 *
 * @package Blogin_Aarambha
 */

/**
 * Blogin_Aarambha_Customizer class
 */
class Blogin_Aarambha_Customizer {

    /**
     * Setup class.
     *
     */
    public function __construct() {

        add_action( 'customize_register', [ $this, 'blogin_aarambha_customize_register' ], 10, 1 );

        add_action( 'customize_preview_init', [ $this, 'blogin_aarambha_customize_preview_js' ], 10 );

        add_action( 'customize_controls_enqueue_scripts', [ $this, 'blogin_aarambha_customize_js' ] );

        add_action( 'after_setup_theme', [ $this, 'blogin_aarambha_customize_option_fields' ] );
    }

    /**
     * Add postMessage support for site title and description for the Theme Customizer.
     *
     * @param WP_Customize_Manager $wp_customize Theme Customizer object.
     */
    public function blogin_aarambha_customize_register( $wp_customize ) {

        $wp_customize->get_setting( 'blogname' )->transport         = 'postMessage';
        $wp_customize->get_setting( 'blogdescription' )->transport  = 'postMessage';

        $wp_customize->remove_control('header_textcolor');
        $wp_customize->remove_control('header_image');

        /** Move default sections to global panel */
        $wp_customize->get_section( 'colors' )->panel           = 'blogin_aarambha_global_panel';
        $wp_customize->get_section( 'colors' )->priority        = 5;
        $wp_customize->get_section( 'title_tagline' )->panel    = 'blogin_aarambha_header';
        $wp_customize->get_section( 'title_tagline' )->priority = 35;
        $wp_customize->get_control( 'custom_logo' )->priority   = 15;
        $wp_customize->get_control( 'blogname' )->priority      = 35;
        $wp_customize->get_control( 'blogname' )->label         = '';
        $wp_customize->get_control( 'blogdescription' )->priority = 45;
        $wp_customize->get_control( 'blogdescription' )->label    = '';
        

        // customizer dir path.
        $customizer_dir = BLOGIN_AARAMBHA_DIR . 'inc/customizer';

        // Selective refresh
        require $customizer_dir . '/selective-refresh.php';

        // Customizer sanitize callback functions
        require_once $customizer_dir . '/Blogin_Aarambha_Customizer_Sanitize_Callback.php';

        // Custom Section
        require_once $customizer_dir . '/sections/Blogin_Aarambha_Customize_Custom_Section.php';

        // Load base class for controls.
        require_once $customizer_dir . '/controls/base/Blogin_Aarambha_Customize_Base_Control.php';
        // Load custom control classes.
        require_once $customizer_dir . '/controls/background/Blogin_Aarambha_Customize_Background_Control.php';
        require_once $customizer_dir . '/controls/border/Blogin_Aarambha_Customize_Border_Control.php';
        require_once $customizer_dir . '/controls/box-shadow/Blogin_Aarambha_Customize_Box_Shadow_Control.php';
        require_once $customizer_dir . '/controls/typography/Blogin_Aarambha_Customize_Typography_Control.php';
        require_once $customizer_dir . '/controls/sortable/Blogin_Aarambha_Customize_Sortable_Control.php';
        require_once $customizer_dir . '/controls/group/Blogin_Aarambha_Customize_Group_Control.php';
        require_once $customizer_dir . '/controls/toggle/Blogin_Aarambha_Customize_Toggle_Control.php';
        require_once $customizer_dir . '/controls/color/Blogin_Aarambha_Customize_Color_Control.php';
        require_once $customizer_dir . '/controls/buttonset/Blogin_Aarambha_Customize_Buttonset_Control.php';
        require_once $customizer_dir . '/controls/range/Blogin_Aarambha_Customize_Range_Control.php';
        require_once $customizer_dir . '/controls/dimensions/Blogin_Aarambha_Customize_Dimensions_Control.php';
        require_once $customizer_dir . '/controls/radio-image/Blogin_Aarambha_Customize_Radio_Image_Control.php';
        require_once $customizer_dir . '/controls/heading/Blogin_Aarambha_Customize_Heading_Control.php';
        require_once $customizer_dir . '/controls/select/Blogin_Aarambha_Customize_Select_Control.php';
        require_once $customizer_dir . '/controls/select/Blogin_Aarambha_Customize_Multi_Select_Control.php';
        require_once $customizer_dir . '/controls/select/Blogin_Aarambha_Customize_Icon_Select_Control.php';
        require_once $customizer_dir . '/controls/repeater/Blogin_Aarambha_Customize_Repeater_Control.php';
        require_once $customizer_dir . '/controls/repeater/Blogin_Aarambha_Customize_Repeater_Setting.php';
        require_once $customizer_dir . '/controls/editor/Blogin_Aarambha_Customize_Editor_Control.php';

        /**
         * Add Panels
         */
        self::blogin_aarambha_add_panels( $wp_customize );

        /**
         * Add Sections
         */
        self::blogin_aarambha_add_sections( $wp_customize );

        /**
         * Add Repeater Fields
         */
        self::blogin_aarambha_add_repeater_fields( $wp_customize );
    }

    /**
     * Binds JS handlers to make Theme Customizer preview reload changes asynchronously.
     */
    public function blogin_aarambha_customize_preview_js() {

        wp_enqueue_script( 'blogin-aarambha-customizer-preview', BLOGIN_AARAMBHA_URI . 'assets/js/customizer-preview' . BLOGIN_AARAMBHA_SCRIPT_PREFIX . '.js', array( 'customize-preview' ), BLOGIN_AARAMBHA_VERSION, true );
    }

    /**
     * heme Customizer JS
     */
    public function blogin_aarambha_customize_js() {

        // Enqueue the style.
        wp_enqueue_style( 'blogin-aarambha-customize-controls', BLOGIN_AARAMBHA_URI . 'assets/css/customize-controls' . BLOGIN_AARAMBHA_SCRIPT_PREFIX . '.css', null, BLOGIN_AARAMBHA_VERSION, 'all' );

        // Add output of Customizer settings as inline style.
        wp_add_inline_style( 'blogin-aarambha-customize-controls', Blogin_Aarambha_Customizer_Inline_Style::css_output('customizer') );

        // Enqueue alpha color picker script
        wp_enqueue_script( 'wp-color-picker-alpha', BLOGIN_AARAMBHA_URI . 'assets/js/wp-color-picker-alpha' . BLOGIN_AARAMBHA_SCRIPT_PREFIX . '.js', [ 'jquery', 'wp-color-picker' ], '2.1.4', true );

        // Enqueue the font awesome style.
        wp_enqueue_style( 'fontawesome', BLOGIN_AARAMBHA_URI . 'assets/css/all.min.css' );

        // Enqueue the scripts.
        wp_enqueue_script( 'blogin-aarambha-customize-controls', BLOGIN_AARAMBHA_URI . 'assets/js/customize-controls' . BLOGIN_AARAMBHA_SCRIPT_PREFIX . '.js', [ 'customize-base', 'wp-color-picker-alpha', 'jquery-ui-sortable' ], BLOGIN_AARAMBHA_VERSION, true );

        wp_enqueue_script( 'blogin-aarambha-customizer', BLOGIN_AARAMBHA_URI . 'assets/js/customizer' . BLOGIN_AARAMBHA_SCRIPT_PREFIX . '.js', array( 'jquery', 'customize-controls' ), BLOGIN_AARAMBHA_VERSION, false );
    }

    /**
     * Render the site title for the selective refresh partial.
     *
     * @return void
     */
    public function blogin_aarambha_customize_partial_blogname() {
        bloginfo( 'name' );
    }

    /**
     * Render the site tagline for the selective refresh partial.
     *
     * @return void
     */
    public function blogin_aarambha_customize_partial_blogdescription() {
        bloginfo( 'description' );
    }

    /**
     * Include customizer options.
     */
    public function blogin_aarambha_customize_option_fields() {

        /**
         * Customizer outputs
         */
        require BLOGIN_AARAMBHA_DIR . 'inc/customizer/Blogin_Aarambha_Customizer_Inline_Style.php';

        /**
         * Base field class.
         */
        require BLOGIN_AARAMBHA_DIR . 'inc/customizer/options/Blogin_Aarambha_Customize_Base_Field.php';

        /**
         * Global Fields
         */
        require BLOGIN_AARAMBHA_DIR . 'inc/customizer/options/global/Blogin_Aarambha_Customize_Global_Social_Fields.php';
        require BLOGIN_AARAMBHA_DIR . 'inc/customizer/options/global/Blogin_Aarambha_Customize_Global_Placeholder_Fields.php';
        require BLOGIN_AARAMBHA_DIR . 'inc/customizer/options/global/Blogin_Aarambha_Customize_Global_Body_Fields.php';
        require BLOGIN_AARAMBHA_DIR . 'inc/customizer/options/global/Blogin_Aarambha_Customize_Global_Site_Content_Fields.php';
        require BLOGIN_AARAMBHA_DIR . 'inc/customizer/options/global/Blogin_Aarambha_Customize_Global_Banner_Fields.php';
        require BLOGIN_AARAMBHA_DIR . 'inc/customizer/options/global/Blogin_Aarambha_Customize_Global_Post_Meta_Fields.php';
        require BLOGIN_AARAMBHA_DIR . 'inc/customizer/options/global/Blogin_Aarambha_Customize_Global_Sidebar_Fields.php';
        require BLOGIN_AARAMBHA_DIR . 'inc/customizer/options/global/Blogin_Aarambha_Customize_Global_Typography_Fields.php';
        require BLOGIN_AARAMBHA_DIR . 'inc/customizer/options/global/Blogin_Aarambha_Customize_Global_Color_Fields.php';
        require BLOGIN_AARAMBHA_DIR . 'inc/customizer/options/global/Blogin_Aarambha_Customize_Global_Button_Fields.php';

        /**
         * Blog Posts Fields
         */
        require BLOGIN_AARAMBHA_DIR . 'inc/customizer/options/blog-posts/Blogin_Aarambha_Customize_Blog_Banner_Fields.php';
        require BLOGIN_AARAMBHA_DIR . 'inc/customizer/options/blog-posts/Blogin_Aarambha_Customize_Blog_Image_Fields.php';
        require BLOGIN_AARAMBHA_DIR . 'inc/customizer/options/blog-posts/Blogin_Aarambha_Customize_Blog_Header_Fields.php';
        require BLOGIN_AARAMBHA_DIR . 'inc/customizer/options/blog-posts/Blogin_Aarambha_Customize_Blog_Content_Fields.php';
        require BLOGIN_AARAMBHA_DIR . 'inc/customizer/options/blog-posts/Blogin_Aarambha_Customize_Blog_Title_Fields.php';
        require BLOGIN_AARAMBHA_DIR . 'inc/customizer/options/blog-posts/Blogin_Aarambha_Customize_Blog_Pagination_Fields.php';
        require BLOGIN_AARAMBHA_DIR . 'inc/customizer/options/blog-posts/Blogin_Aarambha_Customize_Blog_Sidebar_Fields.php';
        require BLOGIN_AARAMBHA_DIR . 'inc/customizer/options/blog-posts/Blogin_Aarambha_Customize_Blog_Read_More_Fields.php';
        
        /**
         * Single Post Fields
         */
        require BLOGIN_AARAMBHA_DIR . 'inc/customizer/options/single-post/Blogin_Aarambha_Customize_Post_Banner_Fields.php';
        require BLOGIN_AARAMBHA_DIR . 'inc/customizer/options/single-post/Blogin_Aarambha_Customize_Post_Image_Fields.php';
        require BLOGIN_AARAMBHA_DIR . 'inc/customizer/options/single-post/Blogin_Aarambha_Customize_Post_Header_Fields.php';
        require BLOGIN_AARAMBHA_DIR . 'inc/customizer/options/single-post/Blogin_Aarambha_Customize_Post_Content_Fields.php';
        require BLOGIN_AARAMBHA_DIR . 'inc/customizer/options/single-post/Blogin_Aarambha_Customize_Post_Title_Fields.php';
        require BLOGIN_AARAMBHA_DIR . 'inc/customizer/options/single-post/Blogin_Aarambha_Customize_Post_Sidebar_Fields.php';

        /**
         * Single Page Fields
         */
        require BLOGIN_AARAMBHA_DIR . 'inc/customizer/options/single-page/Blogin_Aarambha_Customize_Page_Banner_Fields.php';
        require BLOGIN_AARAMBHA_DIR . 'inc/customizer/options/single-page/Blogin_Aarambha_Customize_Page_Content_Fields.php';
        require BLOGIN_AARAMBHA_DIR . 'inc/customizer/options/single-page/Blogin_Aarambha_Customize_Page_Header_Fields.php';
        require BLOGIN_AARAMBHA_DIR . 'inc/customizer/options/single-page/Blogin_Aarambha_Customize_Page_Image_Fields.php';
        require BLOGIN_AARAMBHA_DIR . 'inc/customizer/options/single-page/Blogin_Aarambha_Customize_Page_Title_Fields.php';
        require BLOGIN_AARAMBHA_DIR . 'inc/customizer/options/single-page/Blogin_Aarambha_Customize_Page_Sidebar_Fields.php';
        /**
         * 404 Page Fields
         */
		require BLOGIN_AARAMBHA_DIR . 'inc/customizer/options/404-page/Blogin_Aarambha_Customize_Error_Banner_Fields.php';
        require BLOGIN_AARAMBHA_DIR . 'inc/customizer/options/404-page/Blogin_Aarambha_Customize_Error_Content_Fields.php';
    }

    /**
     * Add customizer panels
     *
     * @access public
     * @param object $wp_customize the object.
     * @return void
     */
    public static function blogin_aarambha_add_panels( $wp_customize ) {

        $panels = [
            'global'        => [
                'title'         => esc_html__( 'Global', 'blogin-aarambha' ),
                'priority'      => 10,
            ],
            'blog'        => [
                'title'         => esc_html__( 'Blog Posts', 'blogin-aarambha' ),
                'priority'      => 30,
            ],
            'post'              => [
                'title'         => esc_html__( 'Single Post', 'blogin-aarambha' ),
                'priority'      => 35,
            ],
            'page'              => [
                'title'         => esc_html__( 'Single Page', 'blogin-aarambha' ),
                'priority'      => 55,
            ],
			'error'             => [
				'title'         => esc_html__( '404 Page', 'blogin-aarambha' ),
				'priority'      => 60,
			],
        ];

        foreach ( $panels as $panel_id => $panel_args ) {
            $wp_customize->add_panel( 'blogin_aarambha_' . str_replace( '-', '_', $panel_id ) . '_panel', $panel_args );
        }
    }

    /**
     * Add customizer sections
     *
     * @access public
     * @param object $wp_customize the object.
     * @return void
     */
    public static function blogin_aarambha_add_sections( $wp_customize ) {

        $sections = [];

        /*--------------------------------------------------------------
        # Global Sections
        --------------------------------------------------------------*/
        // Typography
        $sections['typography']   = [
            'title'                 => esc_html__( 'Typography', 'blogin-aarambha' ),
            'panel'                 => 'blogin_aarambha_global_panel',
            'priority'              => 10
        ];
        // Body
        $sections['body']   = [
            'title'                 => esc_html__( 'Body', 'blogin-aarambha' ),
            'panel'                 => 'blogin_aarambha_global_panel',
            'priority'              => 10
        ];
        // Site Content
        $sections['site_content']   = [
            'title'                 => esc_html__( 'Site Content', 'blogin-aarambha' ),
            'panel'                 => 'blogin_aarambha_global_panel',
            'priority'              => 15
        ];
        // Page Banner
		$sections['banner']   = [
			'title'                 => esc_html__( 'Page Banner', 'blogin-aarambha' ),
			'panel'                 => 'blogin_aarambha_global_panel',
			'priority'              => 20
		];
        // Button
		$sections['button']   = [
			'title'                 => esc_html__( 'Button', 'blogin-aarambha' ),
			'panel'                 => 'blogin_aarambha_global_panel',
			'priority'              => 25
		];
		// Post Meta
		$sections['post_meta']   = [
			'title'                 => esc_html__( 'Post Meta', 'blogin-aarambha' ),
			'panel'                 => 'blogin_aarambha_global_panel',
			'priority'              => 35
		];
        // Sidebar
        $sections['sidebar']   = [
            'title'                 => esc_html__( 'Sidebar', 'blogin-aarambha' ),
            'panel'                 => 'blogin_aarambha_global_panel',
            'priority'              => 40
        ];
        // Social
        $sections['social']   = [
            'title'                 => esc_html__( 'Social', 'blogin-aarambha' ),
            'panel'                 => 'blogin_aarambha_global_panel',
            'priority'              => 45
        ];

        // Placeholder
        $sections['placeholder']   = [
            'title'                 => esc_html__( 'Placeholder Image', 'blogin-aarambha' ),
            'panel'                 => 'blogin_aarambha_global_panel',
            'priority'              => 50
        ];
        /*--------------------------------------------------------------
        # Header Sections
        --------------------------------------------------------------*/
        // Header General
        $sections['header_general']   = [
            'title'                 => esc_html__( 'Header General', 'blogin-aarambha' ),
            'panel'                 => 'blogin_aarambha_header_panel',
            'priority'              => 10
        ];

        /*--------------------------------------------------------------
        # Blog Posts Sections
        --------------------------------------------------------------*/
        // Page Header
        $sections['blog_banner']   = [
            'title'                 => esc_html__( 'Page Banner', 'blogin-aarambha' ),
            'panel'                 => 'blogin_aarambha_blog_panel',
            'priority'              => 5
        ];
        // Featured Image
        $sections['blog_image']   = [
            'title'                 => esc_html__( 'Featured Image', 'blogin-aarambha' ),
            'panel'                 => 'blogin_aarambha_blog_panel',
            'priority'              => 10
        ];
        // Page Header
        $sections['blog_header']   = [
            'title'                 => esc_html__( 'Entry Header', 'blogin-aarambha' ),
            'panel'                 => 'blogin_aarambha_blog_panel',
            'priority'              => 15
        ];
        // Posts layout
        $sections['blog_content']   = [
            'title'                 => esc_html__( 'Entry Content', 'blogin-aarambha' ),
            'panel'                 => 'blogin_aarambha_blog_panel',
            'priority'              => 20
        ];
        // Post title
        $sections['blog_title']   = [
            'title'                 => esc_html__( 'Post Title', 'blogin-aarambha' ),
            'panel'                 => 'blogin_aarambha_blog_panel',
            'priority'              => 25
        ];
        // Read More
        $sections['blog_read_more']   = [
            'title'                 => esc_html__( 'Read More', 'blogin-aarambha' ),
            'panel'                 => 'blogin_aarambha_blog_panel',
            'priority'              => 40
        ];
        // Pagination
        $sections['blog_pagination']   = [
            'title'                 => esc_html__( 'Pagination', 'blogin-aarambha' ),
            'panel'                 => 'blogin_aarambha_blog_panel',
            'priority'              => 45
        ];
        // Sidebar
        $sections['blog_sidebar']   = [
            'title'                 => esc_html__( 'Sidebar', 'blogin-aarambha' ),
            'panel'                 => 'blogin_aarambha_blog_panel',
            'priority'              => 50
        ];

        /*--------------------------------------------------------------
        # Single Post Sections
        --------------------------------------------------------------*/
        // Post Banner
        $sections['post_banner']   = [
            'title'                 => esc_html__( 'Post Banner', 'blogin-aarambha' ),
            'panel'                 => 'blogin_aarambha_post_panel',
            'priority'              => 5
        ];
        // Featured Image
        $sections['post_image']   = [
            'title'                 => esc_html__( 'Featured Image', 'blogin-aarambha' ),
            'panel'                 => 'blogin_aarambha_post_panel',
            'priority'              => 10
        ];
        // Entry Header
        $sections['post_header']   = [
            'title'                 => esc_html__( 'Entry Header', 'blogin-aarambha' ),
            'panel'                 => 'blogin_aarambha_post_panel',
            'priority'              => 15
        ];
        
        // Entry Content
        $sections['post_content']   = [
            'title'                 => esc_html__( 'Entry Content', 'blogin-aarambha' ),
            'panel'                 => 'blogin_aarambha_post_panel',
            'priority'              => 20
        ];
        // Post Title
        $sections['post_title']   = [
            'title'                 => esc_html__( 'Post Title', 'blogin-aarambha' ),
            'panel'                 => 'blogin_aarambha_post_panel',
            'priority'              => 25
        ];
        // Sidebar
        $sections['post_sidebar']   = [
            'title'                 => esc_html__( 'Sidebar', 'blogin-aarambha' ),
            'panel'                 => 'blogin_aarambha_post_panel',
            'priority'              => 30
        ];
        /*--------------------------------------------------------------
        # Single Page Sections
        --------------------------------------------------------------*/
        // Page Banner
        $sections['page_banner']   = [
            'title'                 => esc_html__( 'Page Banner', 'blogin-aarambha' ),
            'panel'                 => 'blogin_aarambha_page_panel',
            'priority'              => 5
        ];
         // Featured Image
         $sections['page_image']   = [
            'title'                 => esc_html__( 'Featured Image', 'blogin-aarambha' ),
            'panel'                 => 'blogin_aarambha_page_panel',
            'priority'              => 10
        ];
        // Entry Header
        $sections['page_header']   = [
            'title'                 => esc_html__( 'Entry Header', 'blogin-aarambha' ),
            'panel'                 => 'blogin_aarambha_page_panel',
            'priority'              => 15
        ];
        // Entry Content
        $sections['page_content']   = [
            'title'                 => esc_html__( 'Entry Content', 'blogin-aarambha' ),
            'panel'                 => 'blogin_aarambha_page_panel',
            'priority'              => 20
        ];

        // Page Title
        $sections['page_title']   = [
            'title'                 => esc_html__( 'Page Title', 'blogin-aarambha' ),
            'panel'                 => 'blogin_aarambha_page_panel',
            'priority'              => 25
        ];

        // Sidebar
        $sections['page_sidebar']   = [
            'title'                 => esc_html__( 'Sidebar', 'blogin-aarambha' ),
            'panel'                 => 'blogin_aarambha_page_panel',
            'priority'              => 30
        ];

        /*--------------------------------------------------------------
        # Error Page Sections
        --------------------------------------------------------------*/
        // Error Page Banner
        $sections['error_banner']   = [
            'title'                 => esc_html__( 'Page Banner', 'blogin-aarambha' ),
			'panel'                 => 'blogin_aarambha_error_panel',
            'priority'              => 10
        ];
		// Error Content
		$sections['error_content']   = [
			'title'                 => esc_html__( 'Page Content', 'blogin-aarambha' ),
			'panel'                 => 'blogin_aarambha_error_panel',
			'priority'              => 20
		];


        // Register sections.
        foreach ( $sections as $section_id => $section_args ) {
            $wp_customize->add_section( 'blogin_aarambha_' . str_replace( '-', '_', $section_id ) . '_section', $section_args );
        }


        // Register sections.
        $wp_customize->add_section(
            new Blogin_Aarambha_Customize_Custom_Section(
                $wp_customize,
                'blogin_aarambha_section_separator_one',
                array(
                    'priority'      => 20,
                    'inline_style'  => 'background:#eee;border-left:0;'
                )
            )
        );
        $wp_customize->add_section(
            new Blogin_Aarambha_Customize_Custom_Section(
                $wp_customize,
                'blogin_aarambha_section_separator_two',
                array(
                    'priority'      => 46,
                    'inline_style'  => 'background:#eee;border-left:0;'
                )
            )
        );
        $wp_customize->add_section(
            new Blogin_Aarambha_Customize_Custom_Section(
                $wp_customize,
                'blogin_aarambha_section_separator_three',
                array(
                    'priority'      => 65,
                    'inline_style'  => 'background:#eee;border-left:0;'
                )
            )
        );

        // Header Builder
        $wp_customize->add_section(
            new Blogin_Aarambha_Customize_Custom_Section(
                $wp_customize,
                'blogin_aarambha_section_separator_five',
                array(
                    'priority'      => 30,
                    'inline_style'  => 'background:#eee;border-left:0;',
                    'panel'         => 'blogin_aarambha_header',
                )
            )
        );

        // Footer Builder
        $wp_customize->add_section(
            new Blogin_Aarambha_Customize_Custom_Section(
                $wp_customize,
                'blogin_aarambha_section_separator_six',
                array(
                    'priority'      => 30,
                    'inline_style'  => 'background:#eee;border-left:0;',
                    'panel'         => 'blogin_aarambha_footer',
                )
            )
        );
        $wp_customize->add_section(
            new Blogin_Aarambha_Customize_Custom_Section(
                $wp_customize,
                'blogin_aarambha_section_separator_seven',
                array(
                    'priority'      => 60,
                    'inline_style'  => 'background:#eee;border-left:0;',
                    'panel'         => 'blogin_aarambha_footer',
                )
            )
        );
    }


    /**
     * Add customizer repeater fields
     *
     * @access public
     * @param object $wp_customize the object.
     * @return void
     */
    public static function blogin_aarambha_add_repeater_fields( $wp_customize ) {

        // Global Social Icons
        $wp_customize->add_setting( new Blogin_Aarambha_Customize_Repeater_Setting(
                $wp_customize,
                'blogin_aarambha_social_icons',
                [
                    'default'           => [
                        [
                            'network'   => 'facebook',
                            'icon'      => '',
                            'link'      => '#'
                        ],
                        [
                            'network'   => 'twitter',
                            'icon'      => '',
                            'link'      => '#'
                        ]
                    ],
                    'sanitize_callback' => ['Blogin_Aarambha_Customizer_Sanitize_Callback', 'sanitize_repeater'],
                ]
            )
        );
        $wp_customize->add_control( new Blogin_Aarambha_Customize_Repeater_Control(
                $wp_customize,
                'blogin_aarambha_social_icons',
                [
                    'section'           => 'blogin_aarambha_social_section',
                    'priority'          => 15,
                    'fields'            => [
                    	'network'		=> [
                    		'type'			=> 'select',
							'label'			=> esc_html__( 'Select Network', 'blogin-aarambha' ),
							'choices'		=> Blogin_Aarambha_Helper::social_network_list()
						],
                        'link'         => [
                            'type'          => 'url',
                            'default'       => '#',
                            'label'         => esc_html__( 'Link', 'blogin-aarambha' )
                        ],
						'icon'         => [
							'type'          => 'font',
							'label'         => esc_html__( 'Custom Icon', 'blogin-aarambha' ),
							'description'   => esc_html__( 'To replace the default social icon, Click below input field and choose the icon. Example: fa-facebook-f', 'blogin-aarambha' )
						]
                    ],
                    'row_label'         => [
                        'type'              => 'field',
                        'value'             => esc_html__( 'Social', 'blogin-aarambha' ),
                        'field'             => 'network'
                    ],
                ]
            )
        );
    }

}
new Blogin_Aarambha_Customizer();
