<?php
/**
 * Blogin Aarambha Customizer Styles
 *
 * @package Blogin_Aarambha
 */

class Blogin_Aarambha_Customizer_Inline_Style {

    /**
     * Get CSS Built from Customizer Options.
     *
     * @access static public
     * @param string $type Whether to return CSS for the "front-end", "block-editor" or "classic-editor".
     * @return string
     */
    public static function css_output( $type = null ) {

        ob_start();

        /*--------------------------------------------------------------
        # Root
        --------------------------------------------------------------*/
        // Accent Colors
        self::customizer_inherit_colors(
            'blogin_aarambha_accent_color',
            null,
            [
                'color_1'   => '--color-accent',
                'color_2'   => '--color-accent-secondary'
            ]
        );
        // Heading H1-H6 Colors
        self::customizer_inherit_colors(
            'blogin_aarambha_heading_color',
            null,
            [
                'color_1'   => '--color-heading'
            ]
        );
        // Text Colors
        self::customizer_inherit_colors(
            'blogin_aarambha_text_color',
            null,
            [
                'color_1'   => '--color-1'
            ]
        );
        // Link Colors
        self::customizer_inherit_colors(
            'blogin_aarambha_link_color',
            null,
            [
                'color_1'   => '--color-link',
                'color_2'   => '--color-link-hover',
                'color_3'   => '--color-link-visited'
            ]
        );

        // Front-End Styles.
        if ('front-end' === $type) {

            /*--------------------------------------------------------------
            # Global -> Body Background
            --------------------------------------------------------------*/
            self::customizer_inherit_background_colors(
                'blogin_aarambha_body_background',
                null,
                [
                    'color_1'   => '--color-bg-1'
                ]
            );
            /*--------------------------------------------------------------
            # Global -> Container
            --------------------------------------------------------------*/
            self::customizer_inherit_colors(
                'blogin_aarambha_site_max_width',
                ['desktop' => '1170px'],
                [
                    'desktop'   => '--container-width'
                ]
            );

            /*--------------------------------------------------------------
            # Global -> Banner
            --------------------------------------------------------------*/
            // text color
            self::color(
                ['
                .site-header .page-title-wrap .archive-description p,
                .site-header .page-title-wrap .page-title,
                .site-header .page-title-wrap .page-title label,
                .site-header .page-title-wrap .breadcrumbs li>span>span
                '],
                'blogin_aarambha_banner_text_color',
                ''
            );

            // link color
            self::color(
                ['
                .site-header .page-title-wrap .breadcrumbs li a>span,
                .site-header .page-title-wrap .entry-meta *
                ','
                .site-header .page-title-wrap .breadcrumbs li a:hover>span,
                .site-header .page-title-wrap .entry-meta a:hover
                '],
                'blogin_aarambha_banner_link_color',
                ''
            );
            self::color(
                ['
                .site-header .page-title-wrap .breadcrumbs li::before
                '],
                'blogin_aarambha_banner_link_color',
                '',
                'border-color'
            );
            self::color(
                ['
                .site-header .page-title-wrap .entry-meta > div::before
                '],
                'blogin_aarambha_banner_link_color',
                '',
                'background-color'
            );
            // Background
            self::background(
                ['.site-header .page-title-wrap'],
                'blogin_aarambha_banner_background',
                ''
            );
            // Overlay Background
            self::background(
                ['.site-header .page-title-wrap::before'],
                'blogin_aarambha_banner_background_overlay',
                ''
            );

            /*--------------------------------------------------------------
            # Global -> Button
            --------------------------------------------------------------*/
            // color
            self::color(
                ['
                .read-more-wrap .read-more-button, .wpcf7-submit[type="submit"], input[type="submit"], button[type="submit"], .comment-form input[type="submit"],
                .post-navigation .nav-links .nav-previous a, .pagination-wrap .nav-links .nav-previous a,
                .post-navigation .nav-links .nav-next a, .pagination-wrap .nav-links .nav-next a,
                .back-to-top button, .reply .comment-reply-link,
                .wp-block-search .wp-block-search__button,
                .pagination .current,.pagination .page-numbers,
                .mc4wp-form input[type=submit],
                button:not(.components-button),
                a.button,
                .wp-block-button__link,
                input[type="button"],
                input[type="reset"],
                input[type="submit"]
                ','
                .read-more-wrap .read-more-button:hover, .wpcf7-submit[type="submit"]:hover, input[type="submit"]:hover, button[type="submit"]:hover, .comment-form input[type="submit"]:hover,
                .post-navigation .nav-links .nav-previous a:hover, .pagination-wrap .nav-links .nav-previous a:hover,
                .post-navigation .nav-links .nav-next a:hover, .pagination-wrap .nav-links .nav-next a:hover,
                .back-to-top button:hover, .reply .comment-reply-link:hover,
                .wp-block-search .wp-block-search__button:hover,
                .pagination a:hover,
                .mc4wp-form input[type=submit]:hover,
                button:not(.components-button):hover,
                a.button:hover,
                .wp-block-button__link:hover,
                input[type="button"]:hover,
                input[type="reset"]:hover,
                input[type="submit"]:hover
                '],
                'blogin_aarambha_button_color',
                '',
                'color'
            );
            // Background color
            self::color(
                ['
                .read-more-wrap .read-more-button, .wpcf7-submit[type="submit"], input[type="submit"], button[type="submit"], .comment-form input[type="submit"],
                .post-navigation .nav-links .nav-previous a, .pagination-wrap .nav-links .nav-previous a,
                .post-navigation .nav-links .nav-next a, .pagination-wrap .nav-links .nav-next a,
                .back-to-top button, .reply .comment-reply-link,
                .wp-block-search .wp-block-search__button,
                .pagination .current,.pagination .page-numbers,
                .mc4wp-form input[type=submit],
                button:not(.components-button),
                a.button,
                .wp-block-button__link,
                input[type="button"],
                input[type="reset"],
                input[type="submit"]
                ','
                .read-more-wrap .read-more-button:hover, .wpcf7-submit[type="submit"]:hover, input[type="submit"]:hover, button[type="submit"]:hover, .comment-form input[type="submit"]:hover,
                .post-navigation .nav-links .nav-previous a:hover, .pagination-wrap .nav-links .nav-previous a:hover,
                .post-navigation .nav-links .nav-next a:hover, .pagination-wrap .nav-links .nav-next a:hover,
                .back-to-top button:hover, .reply .comment-reply-link:hover,
                .wp-block-search .wp-block-search__button:hover,
                .pagination a:hover,
                .mc4wp-form input[type=submit]:hover,
                button:not(.components-button):hover,
                a.button:hover,
                .wp-block-button__link:hover,
                input[type="button"]:hover,
                input[type="reset"]:hover,
                input[type="submit"]:hover
                '],
                'blogin_aarambha_button_bg_color',
                '',
                'background-color'
            );
             // Border
             self::border(
                ['
                .read-more-wrap .read-more-button, .wpcf7-submit[type="submit"], input[type="submit"], button[type="submit"], .comment-form input[type="submit"],
                .post-navigation .nav-links .nav-previous a, .pagination-wrap .nav-links .nav-previous a,
                .post-navigation .nav-links .nav-next a, .pagination-wrap .nav-links .nav-next a,
                .back-to-top button, .reply .comment-reply-link,
                .wp-block-search .wp-block-search__button,
                .pagination .current,.pagination .page-numbers,
                .mc4wp-form input[type=submit],
                button:not(.components-button),
                a.button,
                .wp-block-button__link,
                input[type="button"],
                input[type="reset"],
                input[type="submit"]
                '],
                'blogin_aarambha_button_border',
                ''
            );

            /*--------------------------------------------------------------
            # Header Builder -> Top Row
            --------------------------------------------------------------*/
            // Min Height
            self::range(
                ['.site-header .top-header .site-header-row'],
                'blogin_aarambha_header_top_row_height',
                ['desktop' => '0px'],
                'min-height'
            );
            // Overlay Background
            self::background(
                ['.site-header .top-header::before'],
                'blogin_aarambha_header_top_row_background_overlay',
                ''
            );
            /*--------------------------------------------------------------
            # Header Builder -> Main Row
            --------------------------------------------------------------*/
            // Min Height
            self::range(
                ['.site-header .main-header .site-header-row'],
                'blogin_aarambha_header_main_row_height',
                ['desktop' => '80px'],
                'min-height'
            );
            // Background Overlay Color
            self::background(
                ['.site-header .main-header::before'],
                'blogin_aarambha_header_main_row_background_overlay'
            );
            /*--------------------------------------------------------------
            # Header Builder -> Bottom Row
            --------------------------------------------------------------*/
            // Min Height
            self::range(
                ['.site-header .bottom-header .site-header-row'],
                'blogin_aarambha_header_bottom_row_height',
                ['desktop' => '0px'],
                'min-height'
            );
            // Background Overlay
            self::background(
                ['.site-header .bottom-header::before'],
                'blogin_aarambha_header_bottom_row_background_overlay'
            );
            /*--------------------------------------------------------------
            # Header Builder -> HTML
            --------------------------------------------------------------*/
            // Text Typography
            self::typography(
                ['.site-header .header-html-wrap'],
                'blogin_aarambha_header_html_text_typo',
                ''
            );
            // Link Color
            self::color(
                ['.site-header .header-html-wrap a','.site-header .header-html-wrap a:hover'],
                'blogin_aarambha_header_html_text_link_color',
                ''
            );
            // Container Padding
            self::dimensions(
                ['.site-header .header-html-wrap'],
                'blogin_aarambha_header_html_padding',
                [
                    'desktop'           => [
                        'side_1'            => '10px',
                        'side_3'            => '10px',
                        'linked'            => 'off'
                    ]
                ]
            );
            // Container Margin
            self::dimensions(
                ['.site-header .header-html-wrap'],
                'blogin_aarambha_header_html_margin',
                '',
                'margin'
            );

            /*--------------------------------------------------------------
            # Header Builder -> Site Identify
            --------------------------------------------------------------*/
            // logo margin
            self::generate_css(
                ['.site-header .site-branding.flex-row .site-logo'],
                ['margin-right'],
                '10px'
            );
            // Site Title
            self::typography(
                [
                '.site-title a',
                '.site-title>a:hover'],
                'blogin_aarambha_header_site_title_typo',
                ''
            );
            // Site Tagline
            self::typography(
                ['.site-header .site-branding .site-title-wrap .site-description'],
                'blogin_aarambha_header_site_tagline_typo',
                ''
            );
            // Site Identify Padding
            self::dimensions(
                ['.site-header .site-branding'],
                'blogin_aarambha_header_site_identify_padding',
                ''
            );
            // Site Identify Margin
            self::dimensions(
                ['.site-header .site-branding'],
                'blogin_aarambha_header_site_identify_margin',
                '',
                'margin'
            );
            /*--------------------------------------------------------------
            # Header Builder -> Social Icons
            --------------------------------------------------------------*/
            // Container Padding
            self::dimensions(
                ['.site-header .header-social-wrap'],
                'blogin_aarambha_header_social_icon_padding',
                ''
            );
            // Container Margin
            self::dimensions(
                ['.site-header .header-social-wrap'],
                'blogin_aarambha_header_social_icon_margin',
                '',
                'margin'
            );
            // Icon color
            self::color(
                ['.site-header .header-social-wrap li a','.site-header .header-social-wrap li:hover a'],
                'blogin_aarambha_header_social_icon_item_icon_color',
                [
                    'color_1'           => 'var(--color-link)',
                    'color_2'           => 'var(--color-2)'
                ]
            );
            // Item Background color
            self::color(
                ['.site-header .header-social-wrap li a','.site-header .header-social-wrap li:hover a'],
                'blogin_aarambha_header_social_icon_item_background',
                [
                    'color_1' => 'var(--color-bg-1)',
                    'color_2' => 'var(--color-bg-3)'
                ],
                'background-color'
            );
            // Item Padding
            self::dimensions(
                ['.site-header .header-social-wrap li a'],
                'blogin_aarambha_header_social_icon_item_padding',
                [
                    'desktop'           => [
                        'side_1'            => '11px',
                        'side_2'            => '18px',
                        'side_3'            => '11px',
                        'side_4'            => '18px',
                        'linked'            => 'off'
                    ]
                ]
            );
            // Item Gap
            self::range(
                ['.site-header ul.header-social-wrap >*:not(:last-child)'],
                'blogin_aarambha_header_social_icon_gap',
                ['desktop' => '2px'],
                'margin-right'
            );
            /*--------------------------------------------------------------
            # Header Builder -> Primary Menu
            --------------------------------------------------------------*/
            // Container Padding
            self::dimensions(
                ['.site-header .primary-navbar'],
                'blogin_aarambha_header_primary_menu_container_padding',
                ''
            );
            // Container Margin
            self::dimensions(
                ['.site-header .primary-navbar'],
                'blogin_aarambha_header_primary_menu_container_margin',
                '',
                'margin'
            );
            // Parent Menu Spacing
            self::range(
                ['.site-header .primary-navbar .main-navigation ul.menu-wrapper >*:not(:last-child)'],
                'blogin_aarambha_header_primary_parent_menu_spacing',
                '',
                'margin-right'
            );
            // Parent Menu Colors
            self::color(
                ['
                .site-header .primary-navbar .menu-top-menu-container>ul>li>a,
                .site-header .main-navigation .menu-item-has-children::before,
                .main-navigation.enable-submenu .menu-top-menu-container>ul>li.menu-item-has-children::before
                ','
                .site-header .primary-navbar .menu-top-menu-container>ul>li:hover>a,
                .site-header .main-navigation .menu-item-has-children:hover::before,
                .main-navigation.enable-submenu .menu-top-menu-container>ul>li.menu-item-has-children:hover::before
                '
                ],
                'blogin_aarambha_header_primary_parent_menu_colors',
                '',
                'color'
            );
            $parent_menu_color = get_theme_mod('blogin_aarambha_header_primary_parent_menu_colors','');
            $parent_menu_background = get_theme_mod('blogin_aarambha_header_primary_parent_menu_background_color','');
            if ( !empty($parent_menu_color['color_2'] ) ) {
                self::generate_css(
                    ['
                    .site-header .primary-navbar .menu-top-menu-container>ul>li.current_page_item>a,
                    .site-header .primary-navbar .menu-top-menu-container>ul>li.current_page_item.menu-item-has-children::before,
                    .site-header .primary-navbar .menu-top-menu-container>ul>li.current-menu-item>a,
                    .site-header .primary-navbar .menu-top-menu-container>ul>li.current-menu-item.menu-item-has-children::before
                    '],
                    ['color'],
                    $parent_menu_color['color_2']
                );
            }
            if ( !empty($parent_menu_background['color_2'] ) ) {
                self::generate_css(
                    ['.site-header .primary-navbar .menu-top-menu-container>ul>li.current_page_item>a,.site-header .primary-navbar .menu-top-menu-container>ul>li.current-menu-item>a'],
                    ['background-color'],
                    $parent_menu_background['color_2']
                );
            }
            // Parent Menu Background
            self::color(
                ['.site-header .primary-navbar .menu-top-menu-container>ul>li>a','.site-header .primary-navbar .menu-top-menu-container>ul>li:hover>a'],
                'blogin_aarambha_header_primary_parent_menu_background_color',
                '',
                'background-color'
            );

            // child Menu Colors
            self::color(
                ['
                .site-header .primary-navbar .main-navigation ul li ul li a,
                .site-header .primary-navbar .main-navigation ul li ul li.menu-item-has-children::before
                ',
                    '.site-header .primary-navbar .main-navigation ul li ul li:hover a,
                .site-header .primary-navbar .main-navigation ul li ul li.menu-item-has-children:hover::before
                '
                ],
                'blogin_aarambha_header_primary_child_menu_colors',
                '',
                'color'
            );

            // Child Menu Background
            self::color(
                ['.site-header .primary-navbar .main-navigation ul li ul li a',
                    '.site-header .primary-navbar .main-navigation ul li ul li:hover a'],
                'blogin_aarambha_header_primary_child_menu_background_colors',
                '',
                'background-color'
            );
            $child_menu_color = get_theme_mod('blogin_aarambha_header_primary_child_menu_colors','');
            $child_menu_background = get_theme_mod('blogin_aarambha_header_primary_child_menu_background_colors','');
            if ( !empty($child_menu_color['color_2'] ) ) {
                self::generate_css(
                    ['
                    .site-header .primary-navbar .main-navigation ul li ul li.current_page_item>a,
                    .site-header .primary-navbar .main-navigation ul li ul li.current_page_item.menu-item-has-children::before,
                    .site-header .primary-navbar .main-navigation ul li ul li.current-menu-item>a,
                    .site-header .primary-navbar .main-navigation ul li ul li.current-menu-item.menu-item-has-children::before
                    '],
                    ['color'],
                    $child_menu_color['color_2']
                );
            }
            if ( !empty($child_menu_background['color_2'] ) ) {
                self::generate_css(
                    ['
                    .site-header .primary-navbar .main-navigation ul li ul li.current_page_item>a,
                    .site-header .primary-navbar .main-navigation ul li ul li.current-menu-item>a,
                    '],
                    ['background-color'],
                    $child_menu_background['color_2']
                );
            }
           
            // Child item border
            self::border(
                ['.site-header .primary-navbar .main-navigation ul li.menu-item-has-children ul li'],
                'blogin_aarambha_header_primary_child_menu_border',
                ''
            );

            /*--------------------------------------------------------------
           # Header Builder -> Toggle Menu
           --------------------------------------------------------------*/
           // Icon Color
           self::color(
                ['
                .site-header .mobile-navbar .mean-container .meanmenu-reveal span,
                .site-header .mobile-navbar .mean-container .meanmenu-reveal span:before,
                .site-header .mobile-navbar .mean-container .meanmenu-reveal span:after
                '],
                'blogin_aarambha_header_toggle_menu_icon_color',
                ['color_1'=> 'var(--color-accent-secondary)'],
                'background'
            );
            // Icon background color
            self::color(
                ['.site-header .mobile-navbar .mean-container a.meanmenu-reveal'],
                'blogin_aarambha_header_toggle_menu_icon_background_color',
                '',
                'background-color'
            );
           // Menu Typography
           self::typography(
                ['.site-header .mobile-navbar .mean-container .mean-nav ul li a'],
                'blogin_aarambha_header_toggle_menu_text_typo',
                ''
            );
            // Menu Background
            self::color(
                ['.site-header .mobile-navbar .mean-container .mean-nav>ul,.site-header .mobile-navbar .main-navigation ul li ul li:hover>a','.site-header .mobile-navbar .mean-container .mean-nav>ul>li>a:hover'],
                'blogin_aarambha_header_toggle_menu_dropdown_container_menu_background',
                '',
                'background-color'
            );
            // Container Padding
            self::dimensions(
                ['.site-header .header-toggle-menu-wrap'],
                'blogin_aarambha_header_toggle_menu_padding',
                '',
                'padding'
            );
            // Container Padding
            self::dimensions(
                ['.site-header .header-toggle-menu-wrap'],
                'blogin_aarambha_header_toggle_menu_margin',
                '',
                'margin'
            );
            /*--------------------------------------------------------------
            # Header Builder -> Button
            --------------------------------------------------------------*/
            // Icon color
            self::color(
                ['.site-header .header-button-wrap a','.site-header .header-button-wrap a:hover'],
                'blogin_aarambha_header_button_color',
                [
                    'color_1'   => 'var(--color-link)',
                    'color_2'   => 'var(--color-2)'
                ]
            );
            // Background color
            self::color(
                ['.site-header .header-button-wrap a','.site-header .header-button-wrap a:hover'],
                'blogin_aarambha_header_button_background',
                [ 'color_2' => 'var(--color-bg-3)' ],
                'background-color'
            );
            // Padding
            self::dimensions(
                ['.site-header .header-button-wrap a'],
                'blogin_aarambha_header_button_padding',
                [
                    'desktop'           => [
                        'side_1'            => '12px',
                        'side_2'            => '18px',
                        'side_3'            => '12px',
                        'side_4'            => '18px',
                        'linked'            => 'off'
                    ]
                ]
            );
            // Container Padding
            self::dimensions(
                ['.site-header .header-button-wrap'],
                'blogin_aarambha_header_button_container_padding',
                '',
                'padding'
            );
            // Container Margin
            self::dimensions(
                ['.site-header .header-button-wrap'],
                'blogin_aarambha_header_button_container_margin',
                '',
                'margin'
            );

            if ( Blogin_Aarambha_Helper::is_woocommerce() ) {
                /*--------------------------------------------------------------
                # Header Builder -> WC Cart
                --------------------------------------------------------------*/
                // Icon Size
                self::range(
                    ['.site-header .header-wc-cart-wrap a.cart-icon'],
                    'blogin_aarambha_header_woocommerce_cart_icon_size',
                    ['desktop' => '16px'],
                    'font-size'
                );
                 // Icon color
                 self::color(
                    ['.site-header .header-wc-cart-wrap a.cart-icon','.site-header .header-wc-cart-wrap a.cart-icon:hover'],
                    'blogin_aarambha_header_woocommerce_cart_icon_color',
                    ''
                );
                // Background color
                self::color(
                    ['.site-header .header-wc-cart-wrap a.cart-icon','.site-header .header-wc-cart-wrap a.cart-icon:hover'],
                    'blogin_aarambha_header_woocommerce_cart_icon_background',
                    '',
                    'background-color'
                );
                // Padding
                self::dimensions(
                    ['.site-header .header-wc-cart-wrap a.cart-icon'],
                    'blogin_aarambha_header_woocommerce_cart_padding',
                    [
                        'desktop'           => [
                            'side_1'            => '12px',
                            'side_2'            => '18px',
                            'side_3'            => '12px',
                            'side_4'            => '18px',
                            'linked'            => 'off'
                        ]
                    ]
                );
                // Margin
                self::dimensions(
                    ['.site-header .header-wc-cart-wrap a.cart-icon'],
                    'blogin_aarambha_header_woocommerce_cart_margin',
                    '',
                    'margin'
                );
            }

            /*--------------------------------------------------------------
            # Header Builder -> Account
            --------------------------------------------------------------*/
            // text color
            self::color(
                ['.site-header .header-account-wrap a','.site-header .header-account-wrap a:hover'],
                'blogin_aarambha_header_account_icon_color',
                [ 'color_2' => 'var(--color-2)' ],
                'color'
            );
            // Background color
            self::color(
                ['.site-header .header-account-wrap a','.site-header .header-account-wrap a:hover'],
                'blogin_aarambha_header_account_background',
                [ 'color_2' => 'var(--color-bg-3)' ],
                'background-color'
            );
            // Padding
            self::dimensions(
                ['.site-header .header-account-wrap a'],
                'blogin_aarambha_header_account_padding',
                [
                    'desktop'           => [
                        'side_1'            => '12px',
                        'side_2'            => '18px',
                        'side_3'            => '12px',
                        'side_4'            => '18px',
                        'linked'            => 'off'
                    ]
                ]
            );
            // Padding
            self::dimensions(
                ['.site-header .header-account-wrap'],
                'blogin_aarambha_header_account_container_padding',
                '',
                'padding'
            );
            // Margin
            self::dimensions(
                ['.site-header .header-account-wrap'],
                'blogin_aarambha_header_account_container_margin',
                '',
                'margin'
            );
            /*--------------------------------------------------------------
            # Header Builder -> Menu Trigger
            --------------------------------------------------------------*/
            // Icon Text Gap
            self::range(
                ['.site-header .header-menu-trigger-wrap a.flex-row-reverse .icon'],
                'blogin_aarambha_header_menu_trigger_icon_text_gap',
                '',
                'padding-left'
            );
            self::range(
                ['.site-header .header-menu-trigger-wrap a.flex-row .icon'],
                'blogin_aarambha_header_menu_trigger_icon_text_gap',
                '',
                'padding-right'
            );
            // Icon Size
            self::range(
                ['.site-header .header-menu-trigger-wrap a .icon'],
                'blogin_aarambha_header_menu_trigger_icon_size',
                '',
                'font-size'
            );
            // Text Size
            self::range(
                ['.site-header .header-menu-trigger-wrap a label'],
                'blogin_aarambha_header_menu_trigger_text_size',
                '',
                'font-size'
            );
            // Icon color
            self::color(
                ['.site-header .header-menu-trigger-wrap a','.site-header .header-menu-trigger-wrap a:hover'],
                'blogin_aarambha_header_menu_trigger_color'
            );
            // Background color
            self::color(
                ['.site-header .header-menu-trigger-wrap a','.site-header .header-menu-trigger-wrap a:hover'],
                'blogin_aarambha_header_menu_trigger_background',
                '',
                'background-color'
            );
            // Border
            self::border(
                ['.site-header .header-menu-trigger-wrap a'],
                'blogin_aarambha_header_menu_trigger_border',
                ''
            );
            // box shadow
            self::box_shadow(
                ['.site-header .header-menu-trigger-wrap a'],
                'blogin_aarambha_header_menu_trigger_box_shadow',
                ''
            );
            // Padding
            self::dimensions(
                ['.site-header .header-menu-trigger-wrap a'],
                'blogin_aarambha_header_menu_trigger_padding',
                ''
            );
            // Margin
            self::dimensions(
                ['.site-header .header-menu-trigger-wrap a'],
                'blogin_aarambha_header_menu_trigger_margin',
                '',
                'margin'
            );

            /*--------------------------------------------------------------
            # Header Builder -> Search Icon
            --------------------------------------------------------------*/
            // Icon color
            self::color(
                ['.site-header .site-header-section .header-search-icon-wrap .search-toggle','.site-header .site-header-section .header-search-icon-wrap .search-toggle:hover'],
                'blogin_aarambha_header_search_icon_color',
                ''
            );
            // Background color
            self::color(
                ['.site-header .site-header-section .header-search-icon-wrap .search-toggle','.site-header .site-header-section .header-search-icon-wrap .search-toggle:hover'],
                'blogin_aarambha_header_search_icon_background',
                '',
                'background-color'
            );
            // Container Padding
            self::dimensions(
                ['.site-header .site-header-section .header-search-icon-wrap'],
                'blogin_aarambha_header_search_icon_container_padding',
                ''
            );
            // Container Margin
            self::dimensions(
                ['.site-header .site-header-section .header-search-icon-wrap'],
                'blogin_aarambha_header_search_icon_container_margin',
                '',
                'margin'
            );
            // Padding
            self::dimensions(
                ['.site-header .site-header-section .header-search-icon-wrap .search-toggle'],
                'blogin_aarambha_header_search_icon_padding',
                [
                    'desktop'           => [
                        'side_1'            => '11px',
                        'side_2'            => '18px',
                        'side_3'            => '11px',
                        'side_4'            => '18px',
                        'linked'            => 'off'
                    ]
                ]
            );
            /*--------------------------------------------------------------
            # Header Builder -> Search Icon
            --------------------------------------------------------------*/
            // Icon Size
            self::range(
                ['.site-header .header-color-mode-wrap #theme-toggle span'],
                'blogin_aarambha_header_color_mode_icon_size',
                ['desktop' => '13px'],
                'font-size'
            );
            // Background color
            self::color(
                ['.site-header .header-color-mode-wrap #theme-toggle span'],
                'blogin_aarambha_header_color_mode_icon_bg_color',
                '',
                'background-color'
            );
            // Padding
            self::dimensions(
                ['.site-header .header-color-mode-wrap #theme-toggle span'],
                'blogin_aarambha_header_color_mode_padding',
                ''
            );
            // Margin
            self::dimensions(
                ['.site-header .header-color-mode-wrap'],
                'blogin_aarambha_header_color_mode_margin',
                '',
                'margin'
            );


            /*--------------------------------------------------------------
            # Global -> Body
            --------------------------------------------------------------*/
            self::background( ['body'], 'blogin_aarambha_body_background' );

            /*--------------------------------------------------------------
            # Global -> Typography
            --------------------------------------------------------------*/
            // Base
            self::typography(
                ['body'],
                'blogin_aarambha_base_typography',
                ['font_family'=>'Crimson Text']
            );
            // Heading
            self::typography(
                ['h1,h2,h3,h4,h5,h6,.widget_block.widget_search .wp-block-search__label,.comment-author .url'],
                'blogin_aarambha_heading_typography',
                ['font_family'=>'Playfair Display']
            );
            /*--------------------------------------------------------------
            # Global -> Featured Image Color
            --------------------------------------------------------------*/
            // Background Overlay Color
            self::color(
                ['figure.featured-image a::before'],
                'blogin_aarambha_placeholder_color',
                ['color_1' => '#dbdcdf'],
                'background-color'
            );

            // Is Home Page or archive page or search page
            if ( is_home() || is_archive() || is_search() || is_404() ) {
                /*--------------------------------------------------------------
                # Post Content
                --------------------------------------------------------------*/
                // Read More button icon gap
                self::generate_css(
                    ['.blogin-aarambha-blog #primary .post .post-detail-wrap .read-more-wrap a .icon'],
                    ['margin-left'],
                    '10px'
                );
            }

            // Is 404 Page
            if ( is_404() ) {
                /*--------------------------------------------------------------
                # Page Content
                --------------------------------------------------------------*/
                // Image Height
                self::generate_css(
                    ['.error404 .error-404 .error-page-content figure img'],
                    ['height'],
                    '150px'
                );
                // Spacing
                self::generate_css(
                    ['.error404 .error-404 .error-page-content figure'],
                    ['margin-bottom'],
                    '15px'
                );
                self::generate_css(
                    ['.error404 .error-404 .error-page-content a.home-button'],
                    ['margin-bottom'],
                    '15px'
                );
                self::generate_css(
                    ['.error404 .error-404 .error-page-content form.search-form'],
                    ['margin-bottom'],
                    '15px'
                );
				// Background
				self::background(
					['.error404 .error-404.not-found'],
					'blogin_aarambha_error_container_background'
				);
            }
            // Sidebar
            if ( is_active_sidebar( 'sidebar-1' ) && Blogin_Aarambha_Helper::get_sidebar_layout() ) {

                /*--------------------------------------------------------------
                # Sidebar Container
                --------------------------------------------------------------*/
                // Sidebar Width
                self::generate_css(
                    ['.have-sidebar #secondary'],
                    ['width'],
                    '380px',
                    '',
                    '',
                    '@media only screen and (min-width: 1024px)'
                );
                self::generate_css(
                    ['.have-sidebar #primary'],
                    ['width'],
                    '380px',
                    'calc(100% - ',
                    ')',
                    '@media only screen and (min-width: 1024px)'
                );
                // Sidebar Gap
                if ( Blogin_Aarambha_Helper::get_sidebar_layout() == 'right' ) {
                    self::generate_css(
                        ['.have-sidebar #secondary.right-sidebar'],
                        ['padding-left'],
                        '25px',
                        '',
                        '',
                        '@media only screen and (min-width: 1024px)'
                    );
                    self::generate_css(
                        ['.have-sidebar #primary.content-area'],
                        ['padding-right'],
                        '25px',
                        '',
                        '',
                        '@media only screen and (min-width: 1024px)'
                    );

                }
                elseif ( Blogin_Aarambha_Helper::get_sidebar_layout() == 'left' ) {
                    self::generate_css(
                        ['.have-sidebar #secondary.left-sidebar'],
                        ['padding-right'],
                        '25px',
                        '',
                        '',
                        '@media only screen and (min-width: 1024px)'
                    );
                    self::generate_css(
                        ['.have-sidebar #primary.content-area'],
                        ['padding-left'],
                        '25px',
                        '',
                        '',
                        '@media only screen and (min-width: 1024px)'
                    );
                }
            }
            /*--------------------------------------------------------------
            # Footer Builder -> Top Row
            --------------------------------------------------------------*/
			// Background Overlay
            self::background(
                ['.site-footer .top-footer::before'],
                'blogin_aarambha_footer_top_row_background_overlay',
                [
                    'background'        => 'color',
                    'colors'            => [
                        'color_1'           => 'var(--color-1)'
                    ]
                ]
            );
            // Padding
            self::dimensions(
                ['.site-footer .top-footer .container>.row.columns'],
                'blogin_aarambha_footer_top_row_padding',
                [
                    'desktop'           => [
                        'side_1'            => '25px',
                        'side_3'            => '25px',
                        'linked'            => 'off'
                    ]
                ]
            );
            /*--------------------------------------------------------------
            # Footer Builder -> Main Row
            --------------------------------------------------------------*/
			// Background Overlay
            self::background(
                ['.site-footer .main-footer::before'],
                'blogin_aarambha_footer_main_row_background_overlay',
                [
                    'background'        => 'color',
                    'colors'            => [
                        'color_1'           => 'rgba(0,0,0,0.22)'
                    ]
                ]
            );
            // Padding
            self::dimensions(
                ['.site-footer .main-footer .container>.row.columns'],
                'blogin_aarambha_footer_main_row_padding',
                [
                    'desktop'           => [
                        'side_1'            => '25px',
                        'side_3'            => '25px',
                        'linked'            => 'off'
                    ]
                ]
            );
            /*--------------------------------------------------------------
            # Footer Builder -> Bottom Row
            --------------------------------------------------------------*/
            self::background(
                ['.site-footer .bottom-footer::before'],
                'blogin_aarambha_footer_bottom_row_background_overlay',
				[
					'background'        => 'color',
					'colors'            => [
						'color_1'           => 'var(--color-bg-3)'
					]
				]
            );
            // Padding
            self::dimensions(
                ['.site-footer .bottom-footer .container>.row.columns'],
                'blogin_aarambha_footer_bottom_row_padding',
                ''
            );
            /*--------------------------------------------------------------
            # Footer Builder -> Footer HTML
            --------------------------------------------------------------*/
            // Text Typography
            self::typography(
                ['.site-footer .footer-html-wrap'],
                'blogin_aarambha_footer_html_text_typo',
                ''
            );
            // Link Color
            self::color(
                ['.site-footer .footer-html-wrap a','.site-footer .footer-html-wrap a:hover'],
                'blogin_aarambha_footer_html_text_link_color',
                ''
            );
            // Container Padding
            self::dimensions(
                ['.site-footer .footer-html-wrap'],
                'blogin_aarambha_footer_html_padding',
                [
                    'desktop'           => [
                        'side_1'            => '10px',
                        'side_3'            => '10px',
                        'linked'            => 'off'
                    ]
                ]
            );
            // Container Margin
            self::dimensions(
                ['.site-footer .footer-html-wrap'],
                'blogin_aarambha_footer_html_margin',
                '',
                'margin'
            );

            /*--------------------------------------------------------------
            # Footer Builder -> Footer Menu
            --------------------------------------------------------------*/
            // Container Padding
            self::dimensions(
                ['.site-footer .footer-navbar'],
                'blogin_aarambha_footer_menu_container_padding',
                ''
            );
            // Container Margin
            self::dimensions(
                ['.site-footer .footer-navbar'],
                'blogin_aarambha_footer_menu_container_margin',
                '',
                'margin'
            );
            // Item Gap
            self::range(
                ['.site-footer .footer-navbar .main-navigation ul.menu-wrapper >*:not(:last-child)'],
                'blogin_aarambha_footer_menu_spacing',
                '',
                'margin-right'
            );
            // Menu Colors
            self::color(
                ['.site-footer .footer-navbar .main-navigation li,
                .site-footer .footer-navbar .main-navigation li a',
                    '.site-footer .footer-navbar .main-navigation li a:hover'],
                'blogin_aarambha_footer_menu_font_colors',
                ''
            );
            // Menu Background
            self::color(
                ['.site-footer .footer-navbar .main-navigation li,
                .site-footer .footer-navbar .main-navigation li a',
                    '.site-footer .footer-navbar .main-navigation li a:hover'],
                'blogin_aarambha_footer_menu_background_color',
                '',
                'background-color'
            );

            /*--------------------------------------------------------------
            # Footer Builder -> Button
            --------------------------------------------------------------*/
			// Icon color
            self::color(
                ['.site-footer .footer-button-wrap a','.site-footer .footer-button-wrap a:hover'],
                'blogin_aarambha_footer_button_color',
                ['color_2' => 'var(--color-link)']
            );
            // Background color
            self::color(
                ['.site-footer .footer-button-wrap a','.site-footer .footer-button-wrap a:hover'],
                'blogin_aarambha_footer_button_background',
                [ 'color_2' => 'var(--color-bg-3)' ],
                'background-color'
            );
            // Padding
            self::dimensions(
                ['.site-footer .footer-button-wrap a'],
                'blogin_aarambha_footer_button_padding',
                [
                    'desktop'           => [
                        'side_1'            => '7px',
                        'side_2'            => '15px',
                        'side_3'            => '7px',
                        'side_4'            => '15px',
                        'linked'            => 'off'
                    ]
                ]
            );
            // Margin
            self::dimensions(
                ['.site-footer .footer-button-wrap a'],
                'blogin_aarambha_footer_button_margin',
                [
                    'desktop'           => [
                        'side_1'            => '5px',
                        'side_2'            => '5px',
                        'side_3'            => '5px',
                        'side_4'            => '5px',
                        'linked'            => 'on'
                    ]
                ],
                'margin'
            );

            /*--------------------------------------------------------------
            # Footer Builder -> Copyright Text
            --------------------------------------------------------------*/
            // Text Typography
            self::typography(
                ['.site-footer .site-info,.site-footer .site-info a','.site-footer .site-info a:hover'],
                'blogin_aarambha_footer_copyright_text_typo',
                [
                    'colors'            => [
                        'color_1'           => 'var(--color-1)',
                        'color_2'           => 'var(--color-2)'
                    ]
                ]
            );
            // Padding
            self::dimensions(
                ['.site-footer .site-info'],
                'blogin_aarambha_footer_copyright_padding',
                [
                    'desktop'           => [
                        'side_1'            => '10px',
                        'side_3'            => '10px',
                        'linked'            => 'off'
                    ]
                ],
                'padding'
            );
            // Margin
            self::dimensions(
                ['.site-footer .site-info'],
                'blogin_aarambha_footer_copyright_margin',
                '',
                'margin'
            );

            /*--------------------------------------------------------------
            # Footer Builder -> Social Icons
            --------------------------------------------------------------*/
            // Icon Size
            self::generate_css(
                ['.site-footer .footer-social-wrap li a .icon'],
                ['font-size'],
                '18px'
            );
            // Container Padding
            self::dimensions(
                ['.site-footer .footer-social-wrap'],
                'blogin_aarambha_footer_social_icon_padding',
                ''
            );
            // Container Margin
            self::dimensions(
                ['.site-footer .footer-social-wrap'],
                'blogin_aarambha_footer_social_icon_margin',
                '',
                'margin'
            );
            // Item Gap
            self::range(
                ['.site-footer ul.footer-social-wrap >*:not(:last-child)'],
                'blogin_aarambha_footer_social_icon_gap',
                ['desktop' => '2px'],
                'margin-right'
            );
			// Icon color
            self::color(
                ['.site-footer .footer-social-wrap li a','.site-footer .footer-social-wrap li:hover a'],
                'blogin_aarambha_footer_social_icon_item_icon_color',
                [
                    'color_1'           => 'var(--color-link)',
                    'color_2'           => 'var(--color-2)'
                ]
            );
            // Item Background color
            self::color(
                ['.site-footer .footer-social-wrap li','.site-footer .footer-social-wrap li:hover'],
                'blogin_aarambha_footer_social_icon_item_background',
                [
                    'color_2'           => 'var(--color-link)'
                ],
                'background-color'
            );
            // Item Padding
            self::dimensions(
                ['.site-footer .footer-social-wrap li'],
                'blogin_aarambha_footer_social_icon_item_padding',
                [
                    'desktop'           => [
                        'side_1'            => '10px',
                        'side_2'            => '15px',
                        'side_3'            => '10px',
                        'side_4'            => '15px',
                        'linked'            => 'off'
                    ]
                ]
            );
            
            /*--------------------------------------------------------------
            # Footer Builder -> Sidebar 1, Sidebar 2, Sidebar 3, Sidebar 4, Sidebar 5, Sidebar 6
            --------------------------------------------------------------*/
            // Sidebar Widgets Typography
            self::typography(
                ['
                    .site-footer .footer-sidebar-wrap .widget .widget-title,
                    .site-footer .footer-sidebar-wrap .widget_block h2,
                    .site-footer .footer-sidebar-wrap .widget .widget-title a,
                    .site-footer .footer-sidebar-wrap .widget_block h2 a
                    '],
                'blogin_aarambha_footer_builder_widget_title_typo',
                ''
            );
            // Sidebar Widget Content
            self::typography(
                ['
                    .site-footer .footer-sidebar-wrap .widget ul li,
                    .site-footer .footer-sidebar-wrap .widget ul li a,
                    .site-footer .footer-sidebar-wrap .widget .calendar_wrap,
                    .site-footer .footer-sidebar-wrap .widget input,
                    .site-footer .footer-sidebar-wrap .widget input::placeholder,
                    .site-footer .footer-sidebar-wrap .widget textarea,
                    .site-footer .footer-sidebar-wrap .widget input::placeholder,
                    .site-footer .footer-sidebar-wrap .widget select,
                    .site-footer .footer-sidebar-wrap .widget .wp-caption-text,
                    .site-footer .footer-sidebar-wrap .widget .textwidget,
                    .site-footer .widget p,
                    .site-footer .widget li,
                    .site-footer .widget span,
                    .site-footer .footer-sidebar-wrap .widget .textwidget a,
                    .site-footer .footer-sidebar-wrap .widget_block .wp-block-group__inner-container ul li,
                    .site-footer .footer-sidebar-wrap .widget_tag_cloud a,
                    .site-footer .footer-sidebar-wrap .widget_block .wp-block-group__inner-container ul li a
                    ','
                    .site-footer .footer-sidebar-wrap .widget ul li a:hover,
                    .site-footer .footer-sidebar-wrap .widget .textwidget a:hover,
                    .site-footer .footer-sidebar-wrap .widget_tag_cloud a:hover,
                    .site-footer .footer-sidebar-wrap .widget_block .wp-block-group__inner-container ul li a:hover
                    '],
                'blogin_aarambha_footer_builder_widget_content_typo',
                ''
            );
            // Widget Padding
            self::dimensions(
                ['.site-footer .footer-sidebar-wrap .widget'],
                'blogin_aarambha_footer_builder_widget_padding',
                ''
            );
        }

        // Blog Editor
        if ( 'editor' === $type ) {

            /*--------------------------------------------------------------
            # Block Editor Styles
            --------------------------------------------------------------*/
            // color
            self::color(
                ['
                div.editor-styles-wrapper button:not(.components-button),
                div.editor-styles-wrapper a.button,
                div.editor-styles-wrapper .wp-block-button__link,
                div.editor-styles-wrapper input[type="button"],
                div.editor-styles-wrapper input[type="reset"],
                div.editor-styles-wrapper input[type="submit"]
                ','
                div.editor-styles-wrapper button:not(.components-button):hover,
                div.editor-styles-wrapper a.button:hover,
                div.editor-styles-wrapper .wp-block-button__link:hover,
                div.editor-styles-wrapper input[type="button"]:hover,
                div.editor-styles-wrapper input[type="reset"]:hover,
                div.editor-styles-wrapper input[type="submit"]:hover
                '],
                'blogin_aarambha_button_color',
                [
                    'color_1'           => 'var(--color-link-hover)',
                    'color_2'           => 'var(--color-link)',
                ],
                'color'
            );
             // Background color
             self::color(
                ['
                div.editor-styles-wrapper button:not(.components-button),
                div.editor-styles-wrapper a.button,
                div.editor-styles-wrapper .wp-block-button__link,
                div.editor-styles-wrapper input[type="button"],
                div.editor-styles-wrapper input[type="reset"],
                div.editor-styles-wrapper input[type="submit"]
                ','
                div.editor-styles-wrapper button:not(.components-button):hover,
                div.editor-styles-wrapper a.button:hover,
                div.editor-styles-wrapper .wp-block-button__link:hover,
                div.editor-styles-wrapper input[type="button"]:hover,
                div.editor-styles-wrapper input[type="reset"]:hover,
                div.editor-styles-wrapper input[type="submit"]:hover
                '],
                'blogin_aarambha_button_bg_color',
                [
                    'color_1'           => 'var(--color-bg-3)',
                    'color_2'           => 'var(--color-accent-secondary)',
                ],
                'background-color'
            );
            // Border
            self::border(
                ['
                div.editor-styles-wrapper button:not(.components-button),
                div.editor-styles-wrapper a.button,
                div.editor-styles-wrapper .wp-block-button__link,
                div.editor-styles-wrapper input[type="button"],
                div.editor-styles-wrapper input[type="reset"],
                div.editor-styles-wrapper input[type="submit"]
                '],
                'blogin_aarambha_button_border',
                ''
            );

            // Base Typo
            self::typography(
                ['.editor-styles-wrapper > *'],
                'blogin_aarambha_base_typography',
                ['font_family'=>'Crimson Text']
            );
            // Heading
            self::typography(
                ['
                .editor-styles-wrapper h1,
                .editor-styles-wrapper h2,
                .editor-styles-wrapper h3,
                .editor-styles-wrapper h4,
                .editor-styles-wrapper h5,
                .editor-styles-wrapper h6,
                .editor-styles-wrapper .widget_block.widget_search .wp-block-search__label,
                .editor-styles-wrapper .comment-author .url',
                ],
                'blogin_aarambha_heading_typography',
                ['font_family'=>'Playfair Display']
            );
        }

        // Return the results.
        return ob_get_clean();

    }

	/**
	 * Inherit Color for the root
	 *
	 * @access static public
	 * @param string $setting
	 * @param null $default
	 * @param array $inheritColors
	 * @return void echo style
	 */
	public static function customizer_inherit_colors( $setting, $default = null, $inheritColors = [] ) {

		$values = get_theme_mod( $setting, $default );
		$output = '';
		if ( $values && $values != $default ) {
			foreach ( $values as $index => $val ) {

				if ( isset( $inheritColors[$index] ) ) {
					$output .= $inheritColors[$index] . ':' . esc_attr( $val ) . ';';
				}
			}
		}

		// Output
		$output = ( $output != '' ) ? ':root{ ' . $output . ' }' : '';

		echo $output; // // phpcs:ignore WordPress.Security.EscapeOutput.OutputNotEscaped
	}

    public static function customizer_inherit_background_colors( $setting, $default = null, $inheritColors = [] ) {

        $values = get_theme_mod( $setting, $default );
        $output = '';
        if ( $values && $values != $default ) {
            foreach ( $values as $index => $val ) {
                if ( is_array( $values['colors'] ) && !empty( $values['colors'] ) && $index == 'colors' ) {

                    foreach ( $val as $color_index => $color_val ) {

                        if ( isset( $inheritColors[$color_index] ) ) {
                            $output .= $inheritColors[$color_index] . ':' . esc_attr( $color_val ) . ';';
                        }
                    }

                }
            }
        }

        // Output
        $output = ( $output != '' ) ? ':root{ ' . $output . ' }' : '';

        echo $output; // // phpcs:ignore WordPress.Security.EscapeOutput.OutputNotEscaped
    }

	/**
	 * Background control value output
	 *
	 * @access static public
	 * @param array $selectors
	 * @param string $setting
	 * @param null $default
	 * @return void echo style
	 */
	public static function background( $selectors, $setting, $default = null ) {

		$values = get_theme_mod( $setting, $default );
		$output = '';

		if ( $values ) {

			// Execute only sectors is array type
			if ( is_array( $selectors ) ) {

				$display_type = isset( $values['background'] )
					? $values['background']
					: ( isset( $values['image'] ) ? 'image' : 'color' );

				foreach ( $selectors as $s_index => $selector ) { $s_index++;

					// for color
					if ( $display_type == 'color' && isset( $values['colors'] ) ) {

						$output .= isset( $values['colors']['color_'.$s_index] ) ? $selector . '{ background-color:' . esc_attr( $values['colors']['color_'.$s_index] ) . ';}' : '' ;
					}
					// for gradient
					elseif ( $display_type == 'gradient' && isset( $values['gradient'] ) ) {

						$output .= $selector . '{';

						$output .= 'background:';
						$output .= isset( $values['gradient']['color_'.$s_index] ) ? esc_attr( $values['gradient']['color_'.$s_index] ) : '' ;
						$output .= ';';

						$output .= 'background:-webkit-linear-gradient(to right,';
						$output .= isset( $values['gradient']['color_1'] ) ? esc_attr( $values['gradient']['color_1'] ) . ', ' : '' ;
						$output .= isset( $values['gradient']['color_2'] ) ? esc_attr( $values['gradient']['color_2'] ) : '' ;
						$output .= ');';

						$output .= 'background:linear-gradient(to right,';
						$output .= isset( $values['gradient']['color_1'] ) ? esc_attr( $values['gradient']['color_1'] ) . ', ' : '' ;
						$output .= isset( $values['gradient']['color_2'] ) ? esc_attr( $values['gradient']['color_2'] ) : '' ;
						$output .= ');';

						$output .= '}';
					}
					// for image
					elseif ( $display_type == 'image' && isset( $values['image'] ) ) {
						$output .= $selector . '{ background-image:url("' .esc_url( $values['image'] ). '");';
						$output .= isset( $values['position'] ) ? 'background-position:'. esc_attr( $values['position'] ) .';' : '';
						$output .= isset( $values['size'] ) ? 'background-size:'. esc_attr( $values['size'] ) .';' : '';
						$output .= isset( $values['repeat'] ) ? 'background-repeat:'. esc_attr( $values['repeat'] ) .';' : '';
						$output .= isset( $values['attachment'] ) ? 'background-attachment:'. esc_attr( $values['attachment'] ) .';' : '';
						$output .= '}';
					}
				}
			}
		}

		echo $output; // phpcs:ignore WordPress.Security.EscapeOutput.OutputNotEscaped
	}

	/**
	 * Border control value output
	 *
	 * @access static public
	 * @param array $selectors
	 * @param string $setting
	 * @param null $default
	 * @return void echo style
	 */
	public static function border( $selectors, $setting, $default = null ) {

		$values = get_theme_mod( $setting, $default );
		$output = '';
		$properties = '';

		if ( $values ) {

			// border radius
			$properties .= isset( $values['radius'] ) ? 'border-radius: ' . esc_attr( $values['radius'] ) . ';' : '';

			// execute if linked is "on"
			if ( isset( $values['width'] ) && ( count( $values['width'] ) > 4 ) && $values['width']['linked'] == 'on' ) {

				$properties .= isset( $values['style'] ) && (isset($values['colors']) && !empty($values['colors']) )  ? 'border: ' : 'border-width: ';
				// width
				$width = '';
				foreach ( [ 'side_1', 'side_2', 'side_3', 'side_4' ] as $side ) {
					if ( isset( $values['width'] ) && isset( $values['width'][$side] ) ) {
						$width .= esc_attr( $values['width'][$side] ) . ' ';
						break;
					}
				}

				// Width
				$properties .= esc_attr( $width ) . ' ';

				// style
				$properties .= isset( $values['style'] ) ? esc_attr( $values['style'] ) . ' ' : '';

				$properties .= ';';

			}
			// Execute if linked is "off"
			else {

				// border width
				$widths = '';
				foreach ( [ 'top', 'right', 'bottom', 'left' ] as $index => $key ) { $index++;
					$widths .= isset( $values['width'] ) && isset( $values['width']['side_'.$index] ) ? 'border-' . $key . '-width: ' . esc_attr( $values['width']['side_'.$index] ) . ';' : '';
				}
				$properties .= ( $widths != '' ) ? 'border-width: 0;' : '';
				$properties .= esc_attr( $widths );
				// border style
				$properties .= isset( $values['style'] ) ? 'border-style: ' . esc_attr( $values['style'] ) . ';' : '';
			}

			// Execute only sectors is array type
			if ( is_array( $selectors ) ) {
				foreach ( $selectors as $s_index => $selector ) { $s_index++;
					$border_color = isset( $values['colors'] ) && isset( $values['colors']['color_'.$s_index] ) ? 'border-color: ' . esc_attr( $values['colors']['color_'.$s_index] ) . ';' : '';
					$output .= $selector . '{' . $properties . $border_color .'}';
				}
			}

		}

		// output
		$output = ( '' != $output ) ? $output : '';

		echo $output; // phpcs:ignore WordPress.Security.EscapeOutput.OutputNotEscaped
	}

	/**
	 * Box Shadow control value output
	 *
	 * @access static public
	 * @param array $selectors
	 * @param string $setting
	 * @param null $default
	 * @return void echo style
	 */
	public static function box_shadow( $selectors, $setting, $default = null ) {

		$values = get_theme_mod( $setting, $default );
		$output = '';
		$properties = '';

		if ( $values ) {

			// Execute only if blur value is set and value > 0
			if ( isset( $values['blur'] ) ) {

				// Inset
				$properties .= isset( $values['inset'] ) ? 'inset ' : '';

				// Horizontal Length
				$properties .= isset( $values['h_length'] ) && floatval($values['h_length'] ) != 0 ? esc_attr( $values['h_length'] ) . ' ' : '0 ';

				// Vertical Length
				$properties .= isset( $values['v_length'] ) && floatval($values['v_length'] ) != 0 ? esc_attr( $values['v_length'] ) . ' ' : '0 ';

				// Blur
				$properties .= floatval($values['blur'] ) != 0 ? esc_attr( $values['blur'] ) . ' ' : '0 ';

				// spread
				$properties .= isset( $values['spread'] ) && floatval($values['spread'] ) != 0 ? esc_attr( $values['spread'] ) . ' ' : '0 ';

				// Execute only sectors is array type
				if ( is_array( $selectors ) ) {

					foreach ( $selectors as $s_index => $selector ) { $s_index++;

						$output .= $selector . '{box-shadow: ';
						$output .= $properties;
						$output .= isset( $values['colors'] ) && isset( $values['colors']['color_'.$s_index] ) ? esc_attr( $values['colors']['color_'.$s_index] ) . ';' : ';';
						$output .= '}';
					}
				}
			}


		}

		// Output
		$output = ( '' != $output ) ? $output : '';

		echo $output; // phpcs:ignore WordPress.Security.EscapeOutput.OutputNotEscaped
	}

	/**
	 * Typography control value output
	 *
	 * @param   array $selectors
	 * @param   string $control
	 * @param   null $default
	 * @param   null $media_query
	 * @return  void echo style
	 */
	public static function typography( $selectors, $control, $default = null, $media_query = null ) {

		$values         = get_theme_mod( $control, $default );
		$sm_css         = '';
		$md_css         = '';
		$lg_css         = '';
		$output         = '';
		$media_query    = isset( $media_query ) ? $media_query : ['@media only screen and (min-width:720px)','@media only screen and (min-width:1024px)'];

		if ( $values ) {

			if ( is_array( $selectors ) ) {

				foreach ( $selectors as $s_index => $selector ) { $s_index++;

					// Font Family
					$sm_css .= isset( $values['font_family'] ) ? 'font-family: ' . esc_attr( $values['font_family'] ) . ';' : '';
					// Font Weight
					$sm_css .= isset( $values['weight'] ) ? 'font-weight: ' . esc_attr( $values['weight'] ) . ';' : '';
					// Font Style
					$sm_css .= isset( $values['style'] ) ? 'font-style: ' . esc_attr( $values['style'] ) . ';' : '';
					// Text Transform
					$sm_css .= isset( $values['text_transform'] ) ? 'text-transform: ' . esc_attr( $values['text_transform'] ) . ';' : '';
					// Text Decoration
					$sm_css .= isset( $values['text_decoration'] ) ? 'text-decoration: ' . esc_attr( $values['text_decoration'] ) . ';' : '';

					// font size
					$sm_css .= isset( $values['font_size']['mobile'] )
						? 'font-size: ' . esc_attr( $values['font_size']['mobile'] ) . ';'
						: ( isset( $values['font_size']['tablet'] )
							? 'font-size: ' . esc_attr( $values['font_size']['tablet'] ) . ';'
							: ( isset( $values['font_size']['desktop'] )
								? 'font-size: ' . esc_attr( $values['font_size']['desktop'] ) . ';'
								: ''
							)
						);

					$md_css .= isset( $values['font_size']['tablet'] )
						? 'font-size: ' . esc_attr( $values['font_size']['tablet'] ) . ';'
						: ( isset( $values['font_size']['desktop'] )
							? 'font-size: ' . esc_attr( $values['font_size']['desktop'] ) . ';'
							: ''
						);

					$lg_css .= isset( $values['font_size']['desktop'] )
						? 'font-size: ' . esc_attr( $values['font_size']['desktop'] ) . ';'
						: '';

					// letter spacing
					$sm_css .= isset( $values['letter_spacing']['mobile'] )
						? 'letter-spacing: ' . esc_attr( $values['letter_spacing']['mobile'] ) . ';'
						: ( isset( $values['letter_spacing']['tablet'] )
							? 'letter-spacing: ' . esc_attr( $values['letter_spacing']['tablet'] ) . ';'
							: ( isset( $values['letter_spacing']['desktop'] )
								? 'letter-spacing: ' . esc_attr( $values['letter_spacing']['desktop'] ) . ';'
								: ''
							)
						);

					$md_css .= isset( $values['letter_spacing']['tablet'] )
						? 'letter-spacing: ' . esc_attr( $values['letter_spacing']['tablet'] ) . ';'
						: ( isset( $values['letter_spacing']['desktop'] )
							? 'letter-spacing: ' . esc_attr( $values['letter_spacing']['desktop'] ) . ';'
							: ''
						);

					$lg_css .= isset( $values['letter_spacing']['desktop'] )
						? 'letter-spacing: ' . esc_attr( $values['letter_spacing']['desktop'] ) . ';'
						: '';

					// Line Height
					$sm_css .= isset( $values['line_height']['mobile'] )
						? 'line-height: ' . esc_attr( $values['line_height']['mobile'] ) . ';'
						: ( isset( $values['line_height']['tablet'] )
							? 'line-height: ' . esc_attr( $values['line_height']['tablet'] ) . ';'
							: ( isset( $values['line_height']['desktop'] )
								? 'line-height: ' . esc_attr( $values['line_height']['desktop'] ) . ';'
								: ''
							)
						);

					$md_css .= isset( $values['line_height']['tablet'] )
						? 'line-height: ' . esc_attr( $values['line_height']['tablet'] ) . ';'
						: ( isset( $values['line_height']['desktop'] )
							? 'line-height: ' . esc_attr( $values['line_height']['desktop'] ) . ';'
							: ''
						);

					$lg_css .= isset( $values['line_height']['desktop'] )
						? 'line-height: ' . esc_attr( $values['line_height']['desktop'] ) . ';'
						: '';


					if ( $s_index == 1 ) {
						// Color
						$sm_css .= isset( $values['colors'] ) && isset( $values['colors']['color_'.$s_index] ) ? 'color: ' . esc_attr( $values['colors']['color_'.$s_index] ) . ';' : '';

						// Base CSS
						if ( $sm_css != '' ) {
							$output .= $selector . '{' . $sm_css . '}';
						}
						// For Medium Device
						if ( $md_css != '' ) {
							$output .= $media_query[0] . '{' . $selector . '{' . $md_css . '}}';
						}
						// For Large Device
						if ( $lg_css != '' ) {
							$output .= $media_query[1] . '{' . $selector . '{' . $lg_css . '}}';
						}
					}
					else {
						// Base CSS
						$output .= isset( $values['colors'] ) && isset( $values['colors']['color_'.$s_index] ) ? $selector . '{color: ' . esc_attr( $values['colors']['color_'.$s_index] ) . ';}' : '';
					}

				}

			}
		}

		// Output
		$output = ( '' != $output ) ? $output : '';

		echo $output; // phpcs:ignore WordPress.Security.EscapeOutput.OutputNotEscaped
	}

	/**
	 * Color control value output
	 *
	 * @access static public
	 * @param array $selectors
	 * @param string $setting
	 * @param null $default
	 * @param string $property default is 'color'
	 * @param string $prefix
	 * @param string $suffix
	 * @return void echo style
	 */
	public static function color( $selectors, $setting, $default = null, $property = 'color', $prefix = '', $suffix = '' ) {

		$values = get_theme_mod( $setting, $default );
		$output = '';

		if ( $values ) {

			// Execute only sectors is array type
			if ( is_array( $selectors ) ) {

				foreach ( $selectors as $s_index => $selector ) { $s_index++;

					$output .= isset( $values ) && isset( $values['color_'.$s_index] ) ? $selector . '{' . $property . ': ' . esc_attr( $prefix . $values['color_'.$s_index] .$suffix ) . ';}' : '';
				}
			}

		}

		// Output
		$output = ( '' != $output ) ? $output : '';

		echo $output; // phpcs:ignore WordPress.Security.EscapeOutput.OutputNotEscaped
	}

	/**
	 * Range control value output
	 *
	 * @param   string|array $selector
	 * @param   string $control
	 * @param   null $default
	 * @param   string $property default is 'padding'
	 * @param   string $prefix
	 * @param   string $suffix
	 * @param   null $media_query
	 * @return  void echo style
	 */
	public static function range($selector, $control, $default = null, $property = 'padding', $prefix = '', $suffix = '', $media_query = null ) {

		$values         = get_theme_mod( $control, $default );
		$sm_css         = '';
		$md_css         = '';
		$lg_css         = '';
		$output         = '';
		$media_query    = isset( $media_query ) ? $media_query : ['@media only screen and (min-width:720px)','@media only screen and (min-width:1024px)'];

		if ( $values ) {

			$selector = is_array( $selector ) ? join( ',', $selector ) : $selector;

			// font size
			$sm_css .= isset( $values['mobile'] )
				? $property . ': ' . esc_attr( $prefix . $values['mobile'] . $suffix ) . ';'
				: ( isset( $values['tablet'] )
					? $property . ': ' . esc_attr( $prefix . $values['tablet'] . $suffix ) . ';'
					: ( isset( $values['desktop'] )
						? $property . ': ' . esc_attr( $prefix . $values['desktop'] . $suffix ) . ';'
						: ''
					)
				);

			$md_css .= isset( $values['tablet'] )
				? $property . ': ' . esc_attr( $prefix . $values['tablet'] . $suffix ) . ';'
				: ( isset( $values['desktop'] )
					? $property . ': ' . esc_attr( $prefix . $values['desktop'] . $suffix ) . ';'
					: ''
				);

			$lg_css .= isset( $values['desktop'] )
				? $property . ': ' . esc_attr( $prefix . $values['desktop'] . $suffix ) . ';'
				: '';

			// Base CSS
			if ( $sm_css != '' ) {
				$output .= $selector . '{' . $sm_css . '}';
			}
			// For Medium Device
			if ( $md_css != '' ) {
				$output .= $media_query[0] . '{' . $selector . '{' . $md_css . '}}';
			}
			// For Large Device
			if ( $lg_css != '' ) {
				$output .= $media_query[1] . '{' . $selector . '{' . $lg_css . '}}';
			}
		}

		// Output
		$output = ( '' != $output ) ? $output : '';

		echo $output; // phpcs:ignore WordPress.Security.EscapeOutput.OutputNotEscaped
	}

	/**
	 * Dimensions control value output
	 *
	 * @param   string|array $selector
	 * @param   string $control
	 * @param   null $default
	 * @param   string $property default is 'padding'
	 * @param   string $prefix
	 * @param   string $suffix
	 * @param   null $media_query
	 * @return  void echo style
	 */
	public static function dimensions($selector, $control, $default = null, $property = 'padding', $prefix = '', $suffix = '', $media_query = null ) {

		$values         = get_theme_mod( $control, $default );
		$sm_css         = '';
		$md_css         = '';
		$lg_css         = '';
		$output         = '';
		$media_query    = isset( $media_query ) ? $media_query : ['@media only screen and (min-width:720px)','@media only screen and (min-width:1024px)'];

		if ( $values ) {

			$selector = is_array( $selector ) ? join( ',', $selector ) : $selector;

			// width
			foreach ( [ 'top', 'right', 'bottom', 'left' ] as $index => $key ) { $index++;

				$sm_css .= isset( $values['mobile'] ) && isset( $values['mobile']['side_'.$index] )
					? $property . '-' . $key . ': ' . esc_attr( $prefix . $values['mobile']['side_'.$index] . $suffix ) . ';'
					: ( isset( $values['tablet'] ) && isset( $values['tablet']['side_'.$index] )
						? $property . '-' . $key . ': ' . esc_attr( $prefix . $values['tablet']['side_'.$index] . $suffix ) . ';'
						: ( isset( $values['desktop'] ) && isset( $values['desktop']['side_'.$index] )
							? $property . '-' . $key . ': ' . esc_attr( $prefix . $values['desktop']['side_'.$index] . $suffix ) . ';'
							: ''
						)
					);

				$md_css .= isset( $values['tablet'] ) && isset( $values['tablet']['side_'.$index] )
					? $property . '-' . $key . ': '. esc_attr( $prefix . $values['tablet']['side_'.$index] . $suffix ) . ';'
					: ( isset( $values['desktop'] ) && isset( $values['desktop']['side_'.$index] ) && isset( $values['mobile'] ) && isset( $values['mobile']['side_'.$index] )
						? $property . '-' . $key . ': ' . esc_attr( $prefix . $values['desktop']['side_'.$index] . $suffix ) . ';'
						: ''
					);

				$lg_css .= isset( $values['desktop'] ) && isset( $values['desktop']['side_'.$index] ) && isset( $values['tablet'] ) && isset( $values['tablet']['side_'.$index] ) && isset( $values['mobile'] ) && isset( $values['mobile']['side_'.$index] )
					? $property . '-' . $key . ': ' . esc_attr( $prefix . $values['desktop']['side_'.$index] . $suffix ) . ';'
					: '';
			}

			// Base CSS
			if ( $sm_css != '' ) {
				$output .= $selector . '{' . $sm_css . '}';
			}
			// For Medium Device
			if ( $md_css != '' ) {
				$output .= $media_query[0] . '{' . $selector . '{' . $md_css . '}}';
			}
			// For Large Device
			if ( $lg_css != '' ) {
				$output .= $media_query[1] . '{' . $selector . '{' . $lg_css . '}}';
			}
		}

		// Output
		$output = ( '' != $output ) ? $output : '';

		echo $output; // phpcs:ignore WordPress.Security.EscapeOutput.OutputNotEscaped
	}

	/**
	 * Generate CSS.
	 *
	 * @param array|string $selector The CSS selector.
	 * @param array $property  The CSS style.
	 * @param string $values The CSS value.
	 * @param string $prefix The CSS prefix.
	 * @param string $suffix The CSS suffix.
	 * @param void echo style
	 */
	public static function generate_css( $selector, $property , $values, $prefix = '', $suffix = '', $media = null ) {

		$output = '';

		/*
         * Bail early if we have no $selector elements or properties and $value.
         */
		if ( ! $values || ! $selector ) {
			return;
		}

		if ( $media ) {
			$output .= $media .'{';
		}

		$selector = is_array( $selector ) ? join( ',', $selector ) : $selector;

		$output .= $selector . '{';
		foreach ( $property  as $key => $style ) {
			$output .= $style . ':' . esc_attr( $prefix . $values . $suffix ) . ';';
		}
		$output .= '}';

		if ( $media ) {
			$output .= '}';
		}

		echo $output; // phpcs:ignore WordPress.Security.EscapeOutput.OutputNotEscaped
	}

}
new Blogin_Aarambha_Customizer_Inline_Style();
