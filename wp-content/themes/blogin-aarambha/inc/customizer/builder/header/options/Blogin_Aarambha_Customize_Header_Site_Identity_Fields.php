<?php
/**
 * Blogin Aarambha Theme Customizer Header Site Identify settings
 *
 * @package Blogin_Aarambha
 */

class Blogin_Aarambha_Customize_Header_Site_Identity_Fields extends Blogin_Aarambha_Customize_Base_Field {

    /**
     * Arguments for fields.
     *
     * @return void
     */
    public function init() {
        $this->args = [
            // Grouping Settings
            'blogin_aarambha_header_site_identity_group_settings' => [
                'type'              => 'group',
                'section'           => 'title_tagline',
                'priority'          => 10,
                'choices'           => [
                    'normal'            => array(
                        'tab-title'     => esc_html__( 'General', 'blogin-aarambha' ),
                        'controls'      => array(
                            'custom_logo',
                            'blogin_aarambha_header_site_title_enable',
                            'blogname',
                            'blogin_aarambha_header_site_tagline_enable',
                            'blogdescription',
                            'site_icon'
                        )
                    ),
                    'hover'         => array(
                        'tab-title'     => esc_html__( 'Style', 'blogin-aarambha' ),
                        'controls'      => array(
                            'blogin_aarambha_header_site_identify_note_two',
                            'blogin_aarambha_header_site_title_typo',
                            'blogin_aarambha_header_site_tagline_typo',
                            'blogin_aarambha_header_site_identify_note_three',
                            'blogin_aarambha_header_site_identify_padding',
                            'blogin_aarambha_header_site_identify_margin'
                        )
                    )
                ]
            ],
            // Site title
            'blogin_aarambha_header_site_title_enable' => [
                'type'              => 'toggle',
                'default'           => ['desktop'=> 'true'],
                'sanitize_callback' => ['Blogin_Aarambha_Customizer_Sanitize_Callback', 'sanitize_toggle' ],
                'label'             => esc_html__( 'Site Title', 'blogin-aarambha' ),
                'section'           => 'title_tagline',
                'priority'          => 30
            ],
            // Site tagline
            'blogin_aarambha_header_site_tagline_enable' => [
                'type'              => 'toggle',
                'default'           => ['desktop'=> 'true'],
                'sanitize_callback' => ['Blogin_Aarambha_Customizer_Sanitize_Callback', 'sanitize_toggle' ],
                'label'             => esc_html__( 'Tagline', 'blogin-aarambha' ),
                'section'           => 'title_tagline',
                'priority'          => 40
            ],
            // Note Two
            'blogin_aarambha_header_site_identify_note_two' => [
                'type'              => 'heading',
                'label'             => esc_html__( 'SITE TITLE & TAGLINE', 'blogin-aarambha' ),
                'section'           => 'title_tagline',
                'priority'          => 65,
            ],
            // Site Title
            'blogin_aarambha_header_site_title_typo' => [
                'type'              => 'typography',
                'default'           => '',
                'sanitize_callback' => ['Blogin_Aarambha_Customizer_Sanitize_Callback', 'sanitize_typography' ],
                'label'             => esc_html__( 'Site Title', 'blogin-aarambha' ),
                'section'           => 'title_tagline',
                'priority'          => 70,
                'responsive'        => [ 'desktop', 'tablet', 'mobile' ],
                'units'             => [ 'px', 'rem', 'pt', 'em','vw' ],
                'colors'            => [
                    'color_1'           => esc_html__( 'Normal', 'blogin-aarambha' ),
                    'color_2'           => esc_html__( 'Hover', 'blogin-aarambha' )
                ],
                'inherits'          => [
                    'color_1'           => 'var(--color-link)',
                    'color_2'           => 'var(--color-link-hover)'
                ],
				'fields'            => ['font_family'=>true,'font_variant'=>true,'font_size'=>true,'letter_spacing'=>true,'colors'=>true]
            ],
            // Site Tagline
            'blogin_aarambha_header_site_tagline_typo' => [
                'type'              => 'typography',
                'default'           => '',
                'sanitize_callback' => ['Blogin_Aarambha_Customizer_Sanitize_Callback', 'sanitize_typography' ],
                'label'             => esc_html__( 'Tagline', 'blogin-aarambha' ),
                'section'           => 'title_tagline',
                'priority'          => 75,
                'responsive'        => [ 'desktop', 'tablet', 'mobile' ],
                'units'             => [ 'px', 'rem', 'pt', 'em','vw' ],
                'inherits'          => [
                    'color_1'           => 'var(--color-link)'
                ],
				'fields'            => ['font_size'=>true,'colors'=>true]
            ],
            // Note Three
            'blogin_aarambha_header_site_identify_note_three' => [
                'type'              => 'heading',
                'label'             => esc_html__( 'SITE IDENTIFY CONTAINER', 'blogin-aarambha' ),
                'section'           => 'title_tagline',
                'priority'          => 80,
            ],
            // Padding
            'blogin_aarambha_header_site_identify_padding' => [
                'type'              => 'dimensions',
                'default'           => '',
                'sanitize_callback' => ['Blogin_Aarambha_Customizer_Sanitize_Callback', 'sanitize_dimensions' ],
                'label'             => esc_html__( 'Padding', 'blogin-aarambha' ),
				'description'       => esc_html__( 'Set container padding.', 'blogin-aarambha' ),
                'section'           => 'title_tagline',
                'priority'          => 85,
                'responsive'        => [ 'desktop', 'tablet', 'mobile' ],
            ],
            // Margin
            'blogin_aarambha_header_site_identify_margin' => [
                'type'              => 'dimensions',
                'default'           => '',
                'sanitize_callback' => ['Blogin_Aarambha_Customizer_Sanitize_Callback', 'sanitize_dimensions' ],
                'label'             => esc_html__( 'Margin', 'blogin-aarambha' ),
				'description'       => esc_html__( 'Set container margin.', 'blogin-aarambha' ),
                'section'           => 'title_tagline',
                'priority'          => 90,
                'responsive'        => [ 'desktop', 'tablet', 'mobile' ],
            ]
        ];
    }

}
new Blogin_Aarambha_Customize_Header_Site_Identity_Fields();
