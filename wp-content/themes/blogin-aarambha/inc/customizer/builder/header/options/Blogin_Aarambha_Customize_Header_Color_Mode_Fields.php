<?php
/**
 * Blogin Aarambha Theme Customizer Header Color Mode settings
 *
 * @package Blogin_Aarambha
 */

class Blogin_Aarambha_Customize_Header_Color_Mode_Fields extends Blogin_Aarambha_Customize_Base_Field {

    /**
     * Arguments for fields.
     *
     * @return void
     */
    public function init() {
        $this->args = [
            // Grouping Settings
            'blogin_aarambha_header_color_mode_group_settings' => [
                'type'              => 'group',
                'section'           => 'color_mode',
                'priority'          => 10,
                'choices'           => [
                    'normal'            => array(
                        'tab-title'     => esc_html__( 'General', 'blogin-aarambha' ),
                        'controls'      => array(
                            'custom_logo',
                            'blogin_aarambha_header_color_mode_icon_size'
                        )
                    ),
                    'hover'         => array(
                        'tab-title'     => esc_html__( 'Style', 'blogin-aarambha' ),
                        'controls'      => array(
                            'blogin_aarambha_header_color_mode_icon_bg_color',
                            'blogin_aarambha_header_color_mode_padding',
                            'blogin_aarambha_header_color_mode_margin'
                        )
                    )
                ]
            ],
            // Font Size
			'blogin_aarambha_header_color_mode_icon_size' => [
				'type'              => 'range',
				'default'           => ['desktop' => '13px'],
				'sanitize_callback' => ['Blogin_Aarambha_Customizer_Sanitize_Callback', 'sanitize_range' ],
				'label'             => esc_html__( 'Icon Size', 'blogin-aarambha' ),
				'section'           => 'color_mode',
				'priority'          => 15,
				'responsive'        => [ 'desktop', 'tablet', 'mobile' ],
			],
            // Icon backgorund color
            'blogin_aarambha_header_color_mode_icon_bg_color' => [
                'type'              => 'color',
                'default'           => '',
                'sanitize_callback' => ['Blogin_Aarambha_Customizer_Sanitize_Callback', 'sanitize_color' ],
                'label'             => esc_html__( 'Background Color', 'blogin-aarambha' ),
                'section'           => 'color_mode',
                'priority'          => 30,
                'inherits'          => [
                    'color_1'           => 'var(--color-link)',
                ]
            ],
            // Padding
            'blogin_aarambha_header_color_mode_padding' => [
                'type'              => 'dimensions',
                'default'           => '',
                'sanitize_callback' => ['Blogin_Aarambha_Customizer_Sanitize_Callback', 'sanitize_dimensions' ],
                'label'             => esc_html__( 'Padding', 'blogin-aarambha' ),
                'description'       => esc_html__( 'Set container padding.', 'blogin-aarambha' ),
                'section'           => 'color_mode',
                'priority'          => 35,
                'responsive'        => [ 'desktop', 'tablet', 'mobile' ],
            ],
            // Margin
            'blogin_aarambha_header_color_mode_margin' => [
                'type'              => 'dimensions',
                'default'           => '',
                'sanitize_callback' => ['Blogin_Aarambha_Customizer_Sanitize_Callback', 'sanitize_dimensions' ],
                'label'             => esc_html__( 'Margin', 'blogin-aarambha' ),
                'description'       => esc_html__( 'Set container margin.', 'blogin-aarambha' ),
                'section'           => 'color_mode',
                'priority'          => 40,
                'responsive'        => [ 'desktop', 'tablet', 'mobile' ],
            ]
        ];
    }

}
new Blogin_Aarambha_Customize_Header_Color_Mode_Fields();
