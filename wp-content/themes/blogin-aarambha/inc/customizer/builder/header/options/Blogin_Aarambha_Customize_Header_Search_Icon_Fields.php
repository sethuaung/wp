<?php
/**
 * Blogin Aarambha Theme Customizer Header Search Icon settings
 *
 * @package Blogin_Aarambha
 */

class Blogin_Aarambha_Customize_Header_Search_Icon_Fields extends Blogin_Aarambha_Customize_Base_Field {

    /**
     * Arguments for fields.
     *
     * @return void
     */
    public function init() {
        $this->args = [
            // Grouping Settings
            'blogin_aarambha_header_search_icon_group_settings' => [
                'type'              => 'group',
                'section'           => 'search_icon',
                'priority'          => 10,
                'choices'           => [
                    'normal'            => array(
                        'tab-title'     => esc_html__( 'General', 'blogin-aarambha' ),
                        'controls'      => array(
                            'blogin_aarambha_header_search_icon_placeholder'
                        )
                    ),
                    'hover'         => array(
                        'tab-title'     => esc_html__( 'Style', 'blogin-aarambha' ),
                        'controls'      => array(
                            'blogin_aarambha_header_search_icon_container_padding',
                            'blogin_aarambha_header_search_icon_container_margin',
                            'blogin_aarambha_header_search_icon_color',
                            'blogin_aarambha_header_search_icon_background',
                            'blogin_aarambha_header_search_icon_padding'
                        )
                    )
                ]
            ],
            // Placeholder
            'blogin_aarambha_header_search_icon_placeholder' => [
                'type'              => 'text',
                'default'           => esc_html__( 'Search...', 'blogin-aarambha' ),
                'sanitize_callback' => 'sanitize_text_field',
                'label'             => esc_html__( 'Placeholder', 'blogin-aarambha' ),
                'description'       => esc_html__( 'Set Search Model with placeholder.', 'blogin-aarambha' ),
                'section'           => 'search_icon',
                'priority'          => 15,
            ],
            // Padding
            'blogin_aarambha_header_search_icon_container_padding' => [
                'type'              => 'dimensions',
                'default'           => '',
                'sanitize_callback' => ['Blogin_Aarambha_Customizer_Sanitize_Callback', 'sanitize_dimensions' ],
                'label'             => esc_html__( 'Padding', 'blogin-aarambha' ),
                'description'       => esc_html__( 'Set search icon container padding.', 'blogin-aarambha' ),
                'section'           => 'search_icon',
                'priority'          => 20,
                'responsive'        => [ 'desktop', 'tablet', 'mobile' ],
            ],
            // Margin
            'blogin_aarambha_header_search_icon_container_margin' => [
                'type'              => 'dimensions',
                'default'           => '',
                'sanitize_callback' => ['Blogin_Aarambha_Customizer_Sanitize_Callback', 'sanitize_dimensions' ],
                'label'             => esc_html__( 'Margin', 'blogin-aarambha' ),
                'description'       => esc_html__( 'Set search icon container margin.', 'blogin-aarambha' ),
                'section'           => 'search_icon',
                'priority'          => 25,
                'responsive'        => [ 'desktop', 'tablet', 'mobile' ],
            ],
            // Icon Color
            'blogin_aarambha_header_search_icon_color' => [
                'type'              => 'color',
                'default'           => '',
                'sanitize_callback' => ['Blogin_Aarambha_Customizer_Sanitize_Callback', 'sanitize_color' ],
                'label'             => esc_html__( 'Icon', 'blogin-aarambha' ),
                'description'       => esc_html__( 'Set icon color.', 'blogin-aarambha' ),
                'section'           => 'search_icon',
                'colors'            => [
                    'color_1'           => esc_html__( 'Normal', 'blogin-aarambha' ),
                    'color_2'           => esc_html__( 'Hover', 'blogin-aarambha' ),
                ],
                'priority'          => 30,
                'inherits'          => [
                    'color_1'           => 'var(--color-link)',
                    'color_2'           => 'var(--color-link-hover)',
                ]
            ],
            // Background
            'blogin_aarambha_header_search_icon_background' => [
                'type'              => 'color',
                'default'           => '',
                'sanitize_callback' => ['Blogin_Aarambha_Customizer_Sanitize_Callback', 'sanitize_color' ],
                'label'             => esc_html__( 'Background', 'blogin-aarambha' ),
                'description'       => esc_html__( 'Set icon  background.', 'blogin-aarambha' ),
                'section'           => 'search_icon',
                'colors'            => [
                    'color_1'           => esc_html__( 'Normal', 'blogin-aarambha' ),
                    'color_2'           => esc_html__( 'Hover', 'blogin-aarambha' ),
                ],
                'priority'          => 35,
                'inherits'          => [
                    'color_1'           => 'var(--color-bg-1)',
                    'color_2'           => 'var(--color-bg-1)',
                ]
            ],
            // Padding
            'blogin_aarambha_header_search_icon_padding' => [
                'type'              => 'dimensions',
                'default'           => [
                    'desktop'           => [
                        'side_1'            => '11px',
                        'side_2'            => '18px',
                        'side_3'            => '11px',
                        'side_4'            => '18px',
                        'linked'            => 'off'
                    ]
                ],
                'sanitize_callback' => ['Blogin_Aarambha_Customizer_Sanitize_Callback', 'sanitize_dimensions' ],
                'label'             => esc_html__( 'Padding', 'blogin-aarambha' ),
                'description'       => esc_html__( 'Set search icon padding.', 'blogin-aarambha' ),
                'section'           => 'search_icon',
                'priority'          => 40,
                'responsive'        => [ 'desktop', 'tablet', 'mobile' ],
            ],
            
        ];
    }

}
new Blogin_Aarambha_Customize_Header_Search_Icon_Fields();
