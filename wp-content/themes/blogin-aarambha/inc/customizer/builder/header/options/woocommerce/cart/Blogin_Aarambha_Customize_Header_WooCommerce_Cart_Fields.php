<?php
/**
 * Blogin Aarambha Theme Customizer Header WooCommerce Cart settings
 *
 * @package Blogin_Aarambha
 */

class Blogin_Aarambha_Customize_Header_WooCommerce_Cart_Fields extends Blogin_Aarambha_Customize_Base_Field {

    /**
     * Arguments for fields.
     *
     * @return void
     */
    public function init() {

        $this->args = [
            // Grouping Settings
            'blogin_aarambha_header_woocommerce_cart_group_settings' => [
                'type'              => 'group',
                'section'           => Blogin_Aarambha_WooCommerce_Cart_Header()->element,
                'priority'          => 10,
                'choices'           => [
                    'normal'            => array(
                        'tab-title'     => esc_html__( 'General', 'blogin-aarambha' ),
                        'controls'      => array(
                            'blogin_aarambha_header_woocommerce_cart_icon',
                            'blogin_aarambha_header_woocommerce_cart_icon_size'
                        )
                    ),
                    'hover'         => array(
                        'tab-title'     => esc_html__( 'Style', 'blogin-aarambha' ),
                        'controls'      => array(
                            'blogin_aarambha_header_woocommerce_cart_icon_color',
                            'blogin_aarambha_header_woocommerce_cart_icon_background',
                            'blogin_aarambha_header_woocommerce_cart_padding',
                            'blogin_aarambha_header_woocommerce_cart_margin'

                        )
                    )
                ]
            ],
            // Icon
            'blogin_aarambha_header_woocommerce_cart_icon' => [
                'type'              => 'icon_select',
                'default'           => 'fas fa-bullhorn',
                'sanitize_callback' => 'sanitize_text_field',
                'label'             => esc_html__( 'Icon', 'blogin-aarambha' ),
                'section'           => Blogin_Aarambha_WooCommerce_Cart_Header()->element,
                'priority'          => 10,
                'choices'           => [
                    'fas fa-shopping-cart'      => 'fas fa-shopping-cart',
                    'fas fa-shopping-basket'    => 'fas fa-shopping-basket',
                    'fas fa-shopping-bag'       => 'fas fa-shopping-bag',
                    'fas fa-cart-arrow-down'    => 'fas fa-cart-arrow-down',
                    'fas fa-cart-plus'          => 'fas fa-cart-plus',
                    'fas fa-truck'              => 'fas fa-truck'
                ]
            ],
            // Icon Size
            'blogin_aarambha_header_woocommerce_cart_icon_size' => [
                'type'              => 'range',
                'default'           => ['desktop' => '16px'],
                'sanitize_callback' => ['Blogin_Aarambha_Customizer_Sanitize_Callback', 'sanitize_range' ],
                'label'             => esc_html__( 'Icon Size', 'blogin-aarambha' ),
                'description'       => esc_html__( 'Set WooCommerce cart icon size.', 'blogin-aarambha' ),
                'section'           => Blogin_Aarambha_WooCommerce_Cart_Header()->element,
                'priority'          => 10,
                'responsive'        => [ 'desktop', 'tablet', 'mobile' ],
            ],

            // Icon Color
            'blogin_aarambha_header_woocommerce_cart_icon_color' => [
                'type'              => 'color',
                'default'           => '',
                'sanitize_callback' => ['Blogin_Aarambha_Customizer_Sanitize_Callback', 'sanitize_color' ],
                'label'             => esc_html__( 'Cart Icon', 'blogin-aarambha' ),
                'description'       => esc_html__( 'Set WooCommerce cart icon color.', 'blogin-aarambha' ),
                'section'           => Blogin_Aarambha_WooCommerce_Cart_Header()->element,
                'colors'            => [
                    'color_1'           => esc_html__( 'Normal', 'blogin-aarambha' ),
                    'color_2'           => esc_html__( 'Hover', 'blogin-aarambha' ),
                ],
                'priority'          => 55,
                'inherits'          => [
                    'color_1'           => 'var(--color-link)',
                    'color_2'           => 'var(--color-link-hover)',
                ]
            ],
            // Background
            'blogin_aarambha_header_woocommerce_cart_icon_background' => [
                'type'              => 'color',
                'default'           => '',
                'sanitize_callback' => ['Blogin_Aarambha_Customizer_Sanitize_Callback', 'sanitize_color' ],
                'label'             => esc_html__( 'Cart Background', 'blogin-aarambha' ),
                'description'       => esc_html__( 'Set WooCommerce cart icon background.', 'blogin-aarambha' ),
                'section'           => Blogin_Aarambha_WooCommerce_Cart_Header()->element,
                'colors'            => [
                    'color_1'           => esc_html__( 'Normal', 'blogin-aarambha' ),
                    'color_2'           => esc_html__( 'Hover', 'blogin-aarambha' ),
                ],
                'priority'          => 60,
                'inherits'          => [
                    'color_1'           => 'var(--color-bg-1)',
                    'color_2'           => 'var(--color-bg-1)',
                ]
            ],
            // Padding
            'blogin_aarambha_header_woocommerce_cart_padding' => [
                'type'              => 'dimensions',
                'default'           => [
                    'desktop'           => [
                        'side_1'            => '12px',
                        'side_2'            => '18px',
                        'side_3'            => '12px',
                        'side_4'            => '18px',
                        'linked'            => 'off'
                    ]
                ],
                'sanitize_callback' => ['Blogin_Aarambha_Customizer_Sanitize_Callback', 'sanitize_dimensions' ],
                'label'             => esc_html__( 'Padding', 'blogin-aarambha' ),
                'description'       => esc_html__( 'Set WooCommerce cart padding.', 'blogin-aarambha' ),
                'section'           => Blogin_Aarambha_WooCommerce_Cart_Header()->element,
                'priority'          => 75,
                'responsive'        => [ 'desktop', 'tablet', 'mobile' ],
            ],
            // Margin
            'blogin_aarambha_header_woocommerce_cart_margin' => [
                'type'              => 'dimensions',
                'default'           => '',
                'sanitize_callback' => ['Blogin_Aarambha_Customizer_Sanitize_Callback', 'sanitize_dimensions' ],
                'label'             => esc_html__( 'Margin', 'blogin-aarambha' ),
                'description'       => esc_html__( 'Set WooCommerce cart margin.', 'blogin-aarambha' ),
                'section'           => Blogin_Aarambha_WooCommerce_Cart_Header()->element,
                'priority'          => 80,
                'responsive'        => [ 'desktop', 'tablet', 'mobile' ],
            ]
        ];
    }

}
new Blogin_Aarambha_Customize_Header_WooCommerce_Cart_Fields();
