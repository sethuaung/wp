<?php
/**
 * Blogin Aarambha Theme Customizer Header Primary Menu settings
 *
 * @package Blogin_Aarambha
 */

class Blogin_Aarambha_Customize_Header_Primary_Menu_Fields extends Blogin_Aarambha_Customize_Base_Field {

    /**
     * Arguments for fields.
     *
     * @return void
     */
    public function init() {
        $this->args = [
            // Grouping Settings
            'blogin_aarambha_header_primary_menu_group_settings' => [
                'type'              => 'group',
                'section'           => 'primary_menu',
                'priority'          => 10,
                'choices'           => [
                    'normal'            => array(
                        'tab-title'     => esc_html__( 'General', 'blogin-aarambha' ),
                        'controls'      => array(
                            'blogin_aarambha_header_primary_menu_note_one',
                            'blogin_aarambha_header_primary_parent_menu_spacing'
                        )
                    ),
                    'hover'         => array(
                        'tab-title'     => esc_html__( 'Style', 'blogin-aarambha' ),
                        'controls'      => array(
                            'blogin_aarambha_header_primary_menu_note_five',
                            'blogin_aarambha_header_primary_parent_menu_colors',
                            'blogin_aarambha_header_primary_parent_menu_background_color',
                            'blogin_aarambha_header_primary_menu_note_three',
                            'blogin_aarambha_header_primary_child_menu_colors',
                            'blogin_aarambha_header_primary_child_menu_background_colors',
                            'blogin_aarambha_header_primary_child_menu_border',
                            'blogin_aarambha_header_primary_menu_note_four',
							'blogin_aarambha_header_primary_menu_container_padding',
							'blogin_aarambha_header_primary_menu_container_margin',

                        )
                    )
                ]
            ],
            // Note One
            'blogin_aarambha_header_primary_menu_note_one' => [
                'type'              => 'heading',
				'description'       => sprintf(__( 'To set menu, go to <a data-type="section" data-id="menu_locations" class="customizer-focus"><strong>Primary Menu</strong></a>', 'blogin-aarambha' )),
                'section'           => 'primary_menu',
                'priority'          => 10,
            ],
            // Items Spacing
            'blogin_aarambha_header_primary_parent_menu_spacing' => [
                'type'              => 'range',
                'default'           => '',
                'sanitize_callback' => ['Blogin_Aarambha_Customizer_Sanitize_Callback', 'sanitize_range' ],
                'label'             => esc_html__( 'Menu Spacing', 'blogin-aarambha' ),
                'description'       => esc_html__( 'Slide to change the value of Parent Menu Spacing.', 'blogin-aarambha' ),
                'section'           => 'primary_menu',
                'responsive'        => [ 'desktop', 'tablet', 'mobile' ],
                'priority'          => 20
            ],
            // Heading Three
            'blogin_aarambha_header_primary_menu_note_five' => [
                'type'              => 'heading',
                'label'             => esc_html__( 'PARENT MENU', 'blogin-aarambha' ),
                'section'           => 'primary_menu',
                'priority'          => 53,
            ],
            // Menu Colors
            'blogin_aarambha_header_primary_parent_menu_colors' => [
                'type'              => 'color',
                'default'           => '',
                'sanitize_callback' => ['Blogin_Aarambha_Customizer_Sanitize_Callback', 'sanitize_color' ],
                'label'             => esc_html__( 'Colors', 'blogin-aarambha' ),
                'description'       => esc_html__( 'Set parent menu each item normal and hover colors.', 'blogin-aarambha' ),
                'section'           => 'primary_menu',
                'colors'            => [
                    'color_1'           => esc_html__( 'Normal', 'blogin-aarambha' ),
                    'color_2'           => esc_html__( 'Hover', 'blogin-aarambha' )
                ],
                'inherits'            => [
                    'color_1'           => 'var(--color-link)',
                    'color_2'           => 'var(--color-link-hover)',
                ],
                'priority'          => 55,
            ],
            // Menu Background
            'blogin_aarambha_header_primary_parent_menu_background_color' => [
                'type'              => 'color',
                'default'           => '',
                'sanitize_callback' => ['Blogin_Aarambha_Customizer_Sanitize_Callback', 'sanitize_color' ],
                'label'             => esc_html__( 'Background', 'blogin-aarambha' ),
                'description'       => esc_html__( 'Set parent menu each item background.', 'blogin-aarambha' ),
                'section'           => 'primary_menu',
                'colors'            => [
                    'color_1'           => esc_html__( 'Normal', 'blogin-aarambha' ),
                    'color_2'           => esc_html__( 'Hover', 'blogin-aarambha' )
                ],
                'priority'          => 60,
                'inherits'            => [
                    'color_1'           => 'var(--color-bg-1)',
                    'color_2'           => 'var(--color-bg-1)',
                ],
            ],
            // Heading Three
            'blogin_aarambha_header_primary_menu_note_three' => [
                'type'              => 'heading',
                'label'             => esc_html__( 'CHILD MENU', 'blogin-aarambha' ),
                'section'           => 'primary_menu',
                'priority'          => 70,
            ],
            // Child Menu Colors
            'blogin_aarambha_header_primary_child_menu_colors' => [
                'type'              => 'color',
                'default'           => '',
                'sanitize_callback' => ['Blogin_Aarambha_Customizer_Sanitize_Callback', 'sanitize_color' ],
                'label'             => esc_html__( 'Colors', 'blogin-aarambha' ),
                'description'       => esc_html__( 'Set child menu each item normal and hover colors.', 'blogin-aarambha' ),
                'section'           => 'primary_menu',
                'colors'            => [
                    'color_1'           => esc_html__( 'Normal', 'blogin-aarambha' ),
                    'color_2'           => esc_html__( 'Hover', 'blogin-aarambha' )
                ],
                'inherits'            => [
                    'color_1'           => 'var(--color-link)',
                    'color_2'           => 'var(--color-bg-1)',
                ],
                'priority'          => 75,
            ],
            // SubMenu Background
            'blogin_aarambha_header_primary_child_menu_background_colors' => [
                'type'              => 'color',
                'default'           => '',
                'sanitize_callback' => ['Blogin_Aarambha_Customizer_Sanitize_Callback', 'sanitize_color' ],
                'label'             => esc_html__( 'Background Colors', 'blogin-aarambha' ),
                'description'       => esc_html__( 'Set child menu each item background colors.', 'blogin-aarambha' ),
                'section'           => 'primary_menu',
                'colors'            => [
                    'color_1'           => esc_html__( 'Normal', 'blogin-aarambha' ),
                    'color_2'           => esc_html__( 'Hover', 'blogin-aarambha' )
                ],
                'priority'          => 80,
                'inherits'            => [
                    'color_1'           => 'var(--color-bg-1)',
                    'color_2'           => 'var(--color-bg-3)',
                ],
            ],
            // child menu border
            'blogin_aarambha_header_primary_child_menu_border' => [
                'type'              => 'border',
                'default'           => '',
                'sanitize_callback' => ['Blogin_Aarambha_Customizer_Sanitize_Callback', 'sanitize_border' ],
                'label'             => esc_html__( 'Border Color', 'blogin-aarambha' ),
                'description'       => esc_html__( 'Set child menu bottom border color.', 'blogin-aarambha' ),
                'section'           => 'primary_menu',
                'priority'          => 90,
                'fields'            => ['colors'=>true],
                'inherits'            => [
                    'color_1'           => 'var(--color-1)'
                ],
            ],
            // Heading four
            'blogin_aarambha_header_primary_menu_note_four' => [
                'type'              => 'heading',
                'label'             => esc_html__( 'CONTAINER', 'blogin-aarambha' ),
                'section'           => 'primary_menu',
                'priority'          => 105,
            ],
			// Container Padding
			'blogin_aarambha_header_primary_menu_container_padding' => [
				'type'              => 'dimensions',
				'default'           => '',
				'sanitize_callback' => ['Blogin_Aarambha_Customizer_Sanitize_Callback', 'sanitize_dimensions' ],
				'label'             => esc_html__( 'Padding', 'blogin-aarambha' ),
				'description'       => esc_html__( 'Set primary menu container padding.', 'blogin-aarambha' ),
				'section'           => 'primary_menu',
				'responsive'        => [ 'desktop', 'tablet', 'mobile' ],
				'priority'          => 110,
			],
			// Container Margin
			'blogin_aarambha_header_primary_menu_container_margin' => [
				'type'              => 'dimensions',
				'default'           => '',
				'sanitize_callback' => ['Blogin_Aarambha_Customizer_Sanitize_Callback', 'sanitize_dimensions' ],
				'label'             => esc_html__( 'Margin', 'blogin-aarambha' ),
				'description'       => esc_html__( 'Set primary menu container margin.', 'blogin-aarambha' ),
				'section'           => 'primary_menu',
				'responsive'        => [ 'desktop', 'tablet', 'mobile' ],
				'priority'          => 115,
			]
        ];
    }

}
new Blogin_Aarambha_Customize_Header_Primary_Menu_Fields();
