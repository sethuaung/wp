<?php
if ( ! defined( 'ABSPATH' ) ) {
    exit;
}

/**
 * WooCommerce Header Cart Header Customizer Options
 * @package Blogin_Aarambha
 */

if ( ! class_exists( 'Blogin_Aarambha_WooCommerce_Cart_Header' ) ) :

    class Blogin_Aarambha_WooCommerce_Cart_Header {

        /**
         * Panel ID
         *
         * @var string
         * @access public
         * @since 1.0.0
         *
         */
        public $element = 'wc_cart';

        /**
         * Main Instance
         *
         * Insures that only one instance of Blogin_Aarambha_WooCommerce_Cart_Header exists in memory at any one
         * time. Also prevents needing to define globals all over the place.
         *
         * @since    1.0.0
         * @access   public
         *
         * @return object
         */
        public static function instance() {

            // Store the instance locally to avoid private static replication
            static $instance = null;

            // Only run these methods if they haven't been ran previously
            if ( null === $instance ) {
                $instance = new Blogin_Aarambha_WooCommerce_Cart_Header;
            }

            // Always return the instance
            return $instance;
        }

        /**
         *  Run functionality with hooks
         *
         * @since    1.0.0
         * @access   public
         *
         * @return void
         */
        public function run() {

            add_filter( 'Blogin_Aarambha_Customizer_Header_Builder_items', array( $this, 'add_blogin_aarambha_header_builder_item' ) );
            add_action( 'customize_register', array( $this, 'customize_register' ), 3 );
            add_filter( 'blogin_aarambha_get_template_part', array( $this, 'get_template_part' ), 10, 2 );
        }

        /**
         * Add Item on Header Builder.
         *
         * @param $blogin_aarambha_header_builder_item
         * @return array
         * @since    1.0.0
         */
        public function add_blogin_aarambha_header_builder_item( $blogin_aarambha_header_builder_item ) {
            $blogin_aarambha_header_builder_item[ $this->element ] = array(
                'icon'    => 'dashicons dashicons-cart',
                'name'    => esc_html__( 'Cart', 'blogin-aarambha' ),
                'id'      => $this->element,
                'section' => $this->element,
            );

            return $blogin_aarambha_header_builder_item;
        }

        /**
         * Callback functions for customize_register
         *
         * @since    1.0.2
         * @access   public
         *
         * @param WP_Customize_Manager $wp_customize
         * @return void
         */
        public function customize_register( $wp_customize ) {

            $wp_customize->add_section(
                Blogin_Aarambha_WooCommerce_Cart_Header()->element,
                array(
                    'title'    => esc_html__( 'WC Cart', 'blogin-aarambha' ),
                    'priority' => 80,
                    'panel'    => Blogin_Aarambha_Customizer_Header_Builder()->panel,
                )
            );
            require BLOGIN_AARAMBHA_DIR  . 'inc/customizer/builder/header/options/woocommerce/cart/Blogin_Aarambha_Customize_Header_WooCommerce_Cart_Fields.php';
        }

        /**
         * Load template part
         *
         * @param $template
         * @param $id
         * @return void
         * @since    1.0.0
         */
        function get_template_part( $template, $id ) {
            if ( ! $template && file_exists( BLOGIN_AARAMBHA_DIR . "/template-parts/header/woocommerce/{$id}.php" ) ) {
                $template = BLOGIN_AARAMBHA_DIR . "/template-parts/header/woocommerce/{$id}.php";
            }
            return $template;
        }
    }
endif;

/**
 * Create Instance for Blogin_Aarambha_WooCommerce_Cart_Header
 *
 * @since    1.0.0
 * @access   public
 *
 * @param
 * @return object
 */
if ( ! function_exists( 'Blogin_Aarambha_WooCommerce_Cart_Header' ) ) {

    function Blogin_Aarambha_WooCommerce_Cart_Header() {
        return Blogin_Aarambha_WooCommerce_Cart_Header::instance();
    }
    if( Blogin_Aarambha_Helper::is_woocommerce() ){
        Blogin_Aarambha_WooCommerce_Cart_Header()->run();
    }
}
