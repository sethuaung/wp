<?php
/**
 * Blogin Aarambha Theme Customizer Header Toggle Menu settings
 *
 * @package Blogin_Aarambha
 */

class Blogin_Aarambha_Customize_Header_Toggle_Menu_Fields extends Blogin_Aarambha_Customize_Base_Field {

    /**
     * Arguments for fields.
     *
     * @return void
     */
    public function init() {

        $this->args = [
            // Grouping Settings
            'blogin_aarambha_header_toggle_menu_group_settings' => [
                'type'              => 'group',
                'section'           => 'toggle_menu',
                'priority'          => 10,
                'choices'           => [
                    'normal'            => array(
                        'tab-title'     => esc_html__( 'General', 'blogin-aarambha' ),
                        'controls'      => array(
                            'blogin_aarambha_header_toggle_menu_note_one'
                        )
                    ),
                    'hover'         => array(
                        'tab-title'     => esc_html__( 'Style', 'blogin-aarambha' ),
                        'controls'      => array(
                            'blogin_aarambha_header_toggle_menu_note_two',
                            'blogin_aarambha_header_toggle_menu_icon_color',
                            'blogin_aarambha_header_toggle_menu_icon_background_color',
                            'blogin_aarambha_header_toggle_menu_note_three',
                            'blogin_aarambha_header_toggle_menu_text_typo',
                            'blogin_aarambha_header_toggle_menu_dropdown_container_menu_background',
                            'blogin_aarambha_header_toggle_menu_padding',
                            'blogin_aarambha_header_toggle_menu_margin'

                        )
                    )
                ]
            ],
            // Note One
            'blogin_aarambha_header_toggle_menu_note_one' => [
                'type'              => 'heading',
				'description'       => sprintf(__( 'To set menu, go to <a data-type="section" data-id="menu_locations" class="customizer-focus"><strong>Mobile Menu</strong></a>', 'blogin-aarambha' )),
                'section'           => 'toggle_menu',
                'priority'          => 10,
            ],
            // Note two
            'blogin_aarambha_header_toggle_menu_note_two' => [
                'type'              => 'heading',
                'description'       => esc_html__( 'Menu Icon', 'blogin-aarambha' ),
                'section'           => 'toggle_menu',
                'priority'          => 40,
            ],
            // Icon Color
            'blogin_aarambha_header_toggle_menu_icon_color' => [
                'type'              => 'color',
                'default'           => [
                    'color_1'           => 'var(--color-accent-secondary)'
                ],
                'sanitize_callback' => ['Blogin_Aarambha_Customizer_Sanitize_Callback', 'sanitize_color' ],
                'label'             => esc_html__( 'Color', 'blogin-aarambha' ),
                'description'       => esc_html__( 'Set trigger menu icon color.', 'blogin-aarambha' ),
                'section'           => 'toggle_menu',
                'priority'          => 40,
            ],
            // Icon Background Color
            'blogin_aarambha_header_toggle_menu_icon_background_color' => [
                'type'              => 'color',
                'default'           => '',
                'sanitize_callback' => ['Blogin_Aarambha_Customizer_Sanitize_Callback', 'sanitize_color' ],
                'label'             => esc_html__( 'Background Color', 'blogin-aarambha' ),
                'description'       => esc_html__( 'Set trigger menu icon background color.', 'blogin-aarambha' ),
                'section'           => 'toggle_menu',
                'priority'          => 40,
                'inherits'            => [
                    'color_1'           => 'var(--color-2)',
                ],
            ],
            // Note three
            'blogin_aarambha_header_toggle_menu_note_three' => [
                'type'              => 'heading',
                'description'       => esc_html__( 'Dropdown Container', 'blogin-aarambha' ),
                'section'           => 'toggle_menu',
                'priority'          => 60,
            ],
            // Menu Typography
            'blogin_aarambha_header_toggle_menu_text_typo' => [
                'type'              => 'typography',
                'default'           => '',
                'sanitize_callback' => ['Blogin_Aarambha_Customizer_Sanitize_Callback', 'sanitize_typography' ],
                'label'             => esc_html__( 'Menu Color', 'blogin-aarambha' ),
                'description'       => esc_html__( 'Set menu & submenu text color.', 'blogin-aarambha' ),
                'section'           => 'toggle_menu',
                'priority'          => 61,
                'units'             => [ 'px', 'rem', 'pt', 'em','vw' ],
                'inherits'            => [
                    'color_1'           => 'var(--color-2)'
                ],
                'fields'            => ['colors'=>true],
            ],
            // Menu Background
            'blogin_aarambha_header_toggle_menu_dropdown_container_menu_background' => [
                'type'              => 'color',
                'default'           => '',
                'sanitize_callback' => ['Blogin_Aarambha_Customizer_Sanitize_Callback', 'sanitize_color' ],
                'label'             => esc_html__( 'Menu Background', 'blogin-aarambha' ),
                'description'       => esc_html__( 'Set dropdown container each menu background colors.', 'blogin-aarambha' ),
                'section'           => 'toggle_menu',
                'priority'          => 65,
                'colors'            => [
                    'color_1'           => esc_html__( 'Normal', 'blogin-aarambha' ),
                    'color_2'           => esc_html__( 'Hover', 'blogin-aarambha' )
                ],
                'inherits'            => [
                    'color_1'           => 'rgba(0, 0, 0, 0.8)',
                    'color_2'           => 'var(--color-bg-3)'
                ],
            ],
            // Container Padding
            'blogin_aarambha_header_toggle_menu_padding' => [
                'type'              => 'dimensions',
                'default'           => '',
                'sanitize_callback' => ['Blogin_Aarambha_Customizer_Sanitize_Callback', 'sanitize_dimensions' ],
                'label'             => esc_html__( 'Padding', 'blogin-aarambha' ),
                'description'       => esc_html__( 'Set toggle menu container padding.', 'blogin-aarambha' ),
                'section'           => 'toggle_menu',
                'responsive'        => [ 'desktop', 'tablet', 'mobile' ],
                'priority'          => 70,
            ],
            // Container Margin
            'blogin_aarambha_header_toggle_menu_margin' => [
                'type'              => 'dimensions',
                'default'           => '',
                'sanitize_callback' => ['Blogin_Aarambha_Customizer_Sanitize_Callback', 'sanitize_dimensions' ],
                'label'             => esc_html__( 'Margin', 'blogin-aarambha' ),
                'description'       => esc_html__( 'Set toggle menu container margin.', 'blogin-aarambha' ),
                'section'           => 'toggle_menu',
                'responsive'        => [ 'desktop', 'tablet', 'mobile' ],
                'priority'          => 75,
            ],

        ];
    }

}
new Blogin_Aarambha_Customize_Header_Toggle_Menu_Fields();
