<?php
/**
 * Blogin Aarambha Theme Customizer Header Account settings
 *
 * @package Blogin_Aarambha
 */

class Blogin_Aarambha_Customize_Header_Account_Fields extends Blogin_Aarambha_Customize_Base_Field {


    /**
     * Arguments for fields.
     *
     * @return void
     */
    public function init() {
        $this->args = [
            // Grouping Settings
            'blogin_aarambha_header_account_group_settings' => [
                'type'              => 'group',
                'section'           => 'account',
                'priority'          => 10,
                'choices'           => [
                    'normal'            => array(
                        'tab-title'     => esc_html__( 'General', 'blogin-aarambha' ),
                        'controls'      => array(
                            'blogin_aarambha_header_account_note_one',
                            'blogin_aarambha_header_account_login_text',
                            'blogin_aarambha_header_account_login_url',
                            'blogin_aarambha_header_account_note_two',
                            'blogin_aarambha_header_account_logout_text',
                            'blogin_aarambha_header_account_logout_url',
                            'blogin_aarambha_header_account_note_three',
                            'blogin_aarambha_header_account_url_target'
                        )
                    ),
                    'hover'         => array(
                        'tab-title'     => esc_html__( 'Style', 'blogin-aarambha' ),
                        'controls'      => array(
                            'blogin_aarambha_header_account_container_padding',
                            'blogin_aarambha_header_account_container_margin',
                            'blogin_aarambha_header_account_icon_color',
                            'blogin_aarambha_header_account_background',
                            'blogin_aarambha_header_account_padding',
                            
                        )
                    )
                ]
            ],
            // Note One
            'blogin_aarambha_header_account_note_one' => [
                'type'              => 'heading',
                'label'             => esc_html__( 'LOGIN', 'blogin-aarambha' ),
                'section'           => 'account',
                'priority'          => 15,
            ],
            // Login Text
            'blogin_aarambha_header_account_login_text' => [
                'type'              => 'text',
                'default'           => esc_html__( 'My Account', 'blogin-aarambha' ),
                'sanitize_callback' => 'sanitize_text_field',
                'label'             => esc_html__( 'Text', 'blogin-aarambha' ),
                'section'           => 'account',
                'priority'          => 20,
            ],
            // Account URL
            'blogin_aarambha_header_account_login_url' => [
                'type'              => 'url',
                'default'           => '#',
                'sanitize_callback' => 'esc_url_raw',
                'label'             => esc_html__( 'URL', 'blogin-aarambha' ),
                'section'           => 'account',
                'priority'          => 25,
            ],
            // Note Two
            'blogin_aarambha_header_account_note_two' => [
                'type'              => 'heading',
                'label'             => esc_html__( 'LOGOUT', 'blogin-aarambha' ),
                'section'           => 'account',
                'priority'          => 30,
            ],
            // Logout Text
            'blogin_aarambha_header_account_logout_text' => [
                'type'              => 'text',
                'default'           => esc_html__( 'Log In', 'blogin-aarambha' ),
                'sanitize_callback' => 'sanitize_text_field',
                'label'             => esc_html__( 'Text', 'blogin-aarambha' ),
                'section'           => 'account',
                'priority'          => 35,
            ],
            //  Logout URL
            'blogin_aarambha_header_account_logout_url' => [
                'type'              => 'url',
                'default'           => wp_login_url(),
                'sanitize_callback' => 'esc_url_raw',
                'label'             => esc_html__( 'URL', 'blogin-aarambha' ),
                'section'           => 'account',
                'priority'          => 40,
            ],
            // Note Three
            'blogin_aarambha_header_account_note_three' => [
                'type'              => 'heading',
                'label'             => esc_html__( 'SETTINGS', 'blogin-aarambha' ),
                'section'           => 'account',
                'priority'          => 45,
            ],
            // Link Open
            'blogin_aarambha_header_account_url_target' => [
                'type'              => 'toggle',
                'default'           => '',
                'section'           => 'account',
                'priority'          => 50,
                'sanitize_callback' => ['Blogin_Aarambha_Customizer_Sanitize_Callback', 'sanitize_toggle' ],
                'label'             => esc_html__( 'Link Open', 'blogin-aarambha' ),
                'description'       => esc_html__( 'Toggle to enable link open in new window tab.', 'blogin-aarambha' ),
            ],
            // Padding
            'blogin_aarambha_header_account_container_padding' => [
                'type'              => 'dimensions',
                'default'           => '',
                'sanitize_callback' => ['Blogin_Aarambha_Customizer_Sanitize_Callback', 'sanitize_dimensions' ],
                'label'             => esc_html__( 'Padding', 'blogin-aarambha' ),
                'description'       => esc_html__( 'Set account container padding.', 'blogin-aarambha' ),
                'section'           => 'account',
                'responsive'        => [ 'desktop', 'tablet', 'mobile' ],
                'priority'          => 55,
            ],
            // Margin
            'blogin_aarambha_header_account_container_margin' => [
                'type'              => 'dimensions',
                'default'           => '',
                'sanitize_callback' => ['Blogin_Aarambha_Customizer_Sanitize_Callback', 'sanitize_dimensions' ],
                'label'             => esc_html__( 'Margin', 'blogin-aarambha' ),
                'description'       => esc_html__( 'Set account container margin.', 'blogin-aarambha' ),
                'section'           => 'account',
                'responsive'        => [ 'desktop', 'tablet', 'mobile' ],
                'priority'          => 55,
            ],
            // Icon Color
            'blogin_aarambha_header_account_icon_color' => [
                'type'              => 'color',
                'default'           => [
                    'color_2'           => 'var(--color-2)'
                ],
                'sanitize_callback' => ['Blogin_Aarambha_Customizer_Sanitize_Callback', 'sanitize_color' ],
                'label'             => esc_html__( 'Account', 'blogin-aarambha' ),
                'description'       => esc_html__( 'Set account text color.', 'blogin-aarambha' ),
                'section'           => 'account',
                'colors'            => [
                    'color_1'           => esc_html__( 'Normal', 'blogin-aarambha' ),
                    'color_2'           => esc_html__( 'Hover', 'blogin-aarambha' ),
                ],
                'priority'          => 60,
                'inherits'          => [
                    'color_1'           => 'var(--color-link)',
                    'color_2'           => 'var(--color-2)',
                ]
            ],

            // Background
            'blogin_aarambha_header_account_background' => [
                'type'              => 'color',
                'default'           => [
                    'color_2'           => 'var(--color-bg-3)'
                ],
                'sanitize_callback' => ['Blogin_Aarambha_Customizer_Sanitize_Callback', 'sanitize_color' ],
                'label'             => esc_html__( 'Background', 'blogin-aarambha' ),
                'description'       => esc_html__( 'Set account background.', 'blogin-aarambha' ),
                'section'           => 'account',
                'colors'            => [
                    'color_1'           => esc_html__( 'Normal', 'blogin-aarambha' ),
                    'color_2'           => esc_html__( 'Hover', 'blogin-aarambha' ),
                ],
                'priority'          => 65,
                'inherits'          => [
                    'color_1'           => 'var(--color-bg-1)',
                    'color_2'           => 'var(--color-bg-3)',
                ]
            ],
		
            // Padding
            'blogin_aarambha_header_account_padding' => [
                'type'              => 'dimensions',
                'default'           => [
                    'desktop'           => [
                        'side_1'            => '12px',
                        'side_2'            => '18px',
                        'side_3'            => '12px',
                        'side_4'            => '18px',
                        'linked'            => 'off'
                    ]
                ],
                'sanitize_callback' => ['Blogin_Aarambha_Customizer_Sanitize_Callback', 'sanitize_dimensions' ],
                'label'             => esc_html__( 'Padding', 'blogin-aarambha' ),
                'description'       => esc_html__( 'Set account padding.', 'blogin-aarambha' ),
                'section'           => 'account',
                'responsive'        => [ 'desktop', 'tablet', 'mobile' ],
                'priority'          => 70,
            ],
        ];
    }

}
new Blogin_Aarambha_Customize_Header_Account_Fields();
