<?php
/**
 * Blogin Aarambha Theme Customizer Header Social Icons settings
 *
 * @package Blogin_Aarambha
 */

class Blogin_Aarambha_Customize_Header_Social_Icons_Fields extends Blogin_Aarambha_Customize_Base_Field {

    /**
     * Arguments for fields.
     *
     * @return void
     */
    public function init() {
        $this->args = [
            // Grouping Settings
            'blogin_aarambha_header_social_icon_group_settings' => [
                'type'              => 'group',
                'section'           => 'social_icons',
                'priority'          => 10,
                'choices'           => [
                    'normal'            => array(
                        'tab-title'     => esc_html__( 'General', 'blogin-aarambha' ),
                        'controls'      => array(
                            'blogin_aarambha_header_social_icon_note_one',
                            'blogin_aarambha_header_social_icon_gap',
                            'blogin_aarambha_header_social_icon_link_open'
                        )
                    ),
                    'hover'         => array(
                        'tab-title'     => esc_html__( 'Style', 'blogin-aarambha' ),
                        'controls'      => array(
                            'blogin_aarambha_header_social_icon_padding',
                            'blogin_aarambha_header_social_icon_margin',
                            'blogin_aarambha_header_social_icon_note_two',
                            'blogin_aarambha_header_social_icon_item_icon_color',
                            'blogin_aarambha_header_social_icon_item_background',
                            'blogin_aarambha_header_social_icon_item_padding'
                        )
                    )
                ]
            ],
            // Heading One
            'blogin_aarambha_header_social_icon_note_one' => [
                'type'              => 'heading',
				'description'       => sprintf(__( 'Configure social icons in Global &raquo; Social &raquo; <a data-type="control" data-id="blogin_aarambha_social_icons" class="customizer-focus"><strong> Social Icons </strong></a>.', 'blogin-aarambha' )),
                'section'           => 'social_icons',
                'priority'          => 15,
            ],
            // Item Gap
            'blogin_aarambha_header_social_icon_gap' => [
                'type'              => 'range',
                'default'           => [
                    'desktop'           => '2px'
                ],
                'sanitize_callback' => ['Blogin_Aarambha_Customizer_Sanitize_Callback', 'sanitize_range' ],
                'label'             => esc_html__( 'Item Gap', 'blogin-aarambha' ),
                'description'       => esc_html__( 'Set gap between each social icon lists.', 'blogin-aarambha' ),
                'section'           => 'social_icons',
                'priority'          => 35,
                'input_attrs'       => [
                    'min'               => 0,
                    'max'               => 50
                ],
                'responsive'        => [ 'desktop', 'tablet', 'mobile' ],
            ],
            // Link Open
            'blogin_aarambha_header_social_icon_link_open' => [
                'type'              => 'toggle',
                'default'           => '',
                'section'           => 'social_icons',
                'priority'          => 40,
                'sanitize_callback' => ['Blogin_Aarambha_Customizer_Sanitize_Callback', 'sanitize_toggle' ],
                'label'             => esc_html__( 'Link Open', 'blogin-aarambha' ),
                'description'       => esc_html__( 'Enable to open the link in the new tab.', 'blogin-aarambha' ),
            ],
            // Padding
            'blogin_aarambha_header_social_icon_padding' => [
                'type'              => 'dimensions',
                'default'           => '',
                'sanitize_callback' => ['Blogin_Aarambha_Customizer_Sanitize_Callback', 'sanitize_dimensions' ],
                'label'             => esc_html__( 'Padding', 'blogin-aarambha' ),
                'description'       => esc_html__( 'Set social container padding.', 'blogin-aarambha' ),
                'section'           => 'social_icons',
                'priority'          => 42,
                'responsive'        => [ 'desktop', 'tablet', 'mobile' ],
            ],
            // Margin
            'blogin_aarambha_header_social_icon_margin' => [
                'type'              => 'dimensions',
                'default'           => '',
                'sanitize_callback' => ['Blogin_Aarambha_Customizer_Sanitize_Callback', 'sanitize_dimensions' ],
                'label'             => esc_html__( 'Margin', 'blogin-aarambha' ),
                'description'       => esc_html__( 'Set social container margin.', 'blogin-aarambha' ),
                'section'           => 'social_icons',
                'priority'          => 45,
                'responsive'        => [ 'desktop', 'tablet', 'mobile' ],
            ],
            // Heading One
            'blogin_aarambha_header_social_icon_note_two' => [
                'type'              => 'heading',
                'label'             => esc_html__( 'ITEM', 'blogin-aarambha' ),
                'section'           => 'social_icons',
                'priority'          => 50,
            ],
            // Icon Color
            'blogin_aarambha_header_social_icon_item_icon_color' => [
                'type'              => 'color',
                'default'           => [
                    'color_1'           => 'var(--color-link)',
                    'color_2'           => 'var(--color-2)'
                ],
                'sanitize_callback' => ['Blogin_Aarambha_Customizer_Sanitize_Callback', 'sanitize_color' ],
                'label'             => esc_html__( 'Icon', 'blogin-aarambha' ),
                'description'       => esc_html__( 'Set each items icon as same color.', 'blogin-aarambha' ),
                'section'           => 'social_icons',
                'colors'            => [
                    'color_1'           => esc_html__( 'Normal', 'blogin-aarambha' ),
                    'color_2'           => esc_html__( 'Hover', 'blogin-aarambha' ),
                ],
                'inherits'          => [
                    'color_1'           => 'var(--color-link)',
                    'color_2'           => 'var(--color-2)'
                ],
                'priority'          => 55,
            ],
            // Background Color
            'blogin_aarambha_header_social_icon_item_background' => [
                'type'              => 'color',
                'default'           => [
                    'color_1'           => 'var(--color-bg-1)',
                    'color_2'           => 'var(--color-bg-3)'
                ],
                'sanitize_callback' => ['Blogin_Aarambha_Customizer_Sanitize_Callback', 'sanitize_color' ],
                'label'             => esc_html__( 'Background', 'blogin-aarambha' ),
                'description'       => esc_html__( 'Set each item background color.', 'blogin-aarambha' ),
                'section'           => 'social_icons',
                'colors'            => [
                    'color_1'           => esc_html__( 'Normal', 'blogin-aarambha' ),
                    'color_2'           => esc_html__( 'Hover', 'blogin-aarambha' ),
                ],
                'inherits'          => [
                    'color_1'           => 'var(--color-bg-1)',
                    'color_2'           => 'var(--color-bg-3)'
                ],
                'priority'          => 60,
            ],
            // Padding
            'blogin_aarambha_header_social_icon_item_padding' => [
                'type'              => 'dimensions',
                'default'           => [
                    'desktop'           => [
                        'side_1'            => '11px',
                        'side_2'            => '18px',
                        'side_3'            => '11px',
                        'side_4'            => '18px',
                        'linked'            => 'off'
                    ]
                ],
                'sanitize_callback' => ['Blogin_Aarambha_Customizer_Sanitize_Callback', 'sanitize_dimensions' ],
                'label'             => esc_html__( 'Padding', 'blogin-aarambha' ),
                'description'       => esc_html__( 'Set each item padding.', 'blogin-aarambha' ),
                'section'           => 'social_icons',
                'priority'          => 80,
                'responsive'        => [ 'desktop', 'tablet', 'mobile' ],
            ]
        ];
    }

}
new Blogin_Aarambha_Customize_Header_Social_Icons_Fields();
