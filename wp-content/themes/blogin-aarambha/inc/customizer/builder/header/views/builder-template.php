<?php
/**
 * Add Header Builder Template
 *
 * @package Blogin_Aarambha
 */

/*----------------------------------------------------------------------
# Exit if accessed directly
-------------------------------------------------------------------------*/
if ( ! defined( 'ABSPATH' ) ) {
    exit;
}



?>

<script type="text/html" id="tmpl-blogin-aarambha-builder-panel">
    <div class="blogin-aarambha-customize-builder">
        <div class="blogin-aarambha-inner">
            <div class="blogin-aarambha-header">
                <div class="blogin-aarambha-devices-switcher">
                </div>
                <div class="blogin-aarambha-actions">
                    <a class="button button-secondary blogin-aarambha-panel-close" href="#">
                        <span class="close-text"><?php esc_html_e( 'Close', 'blogin-aarambha' ); ?></span>
                        <span class="panel-name-text">{{ data.title }}</span>
                    </a>
                </div>
            </div>
            <div class="blogin-aarambha-body"></div>
        </div>
    </div>
</script>

<script type="text/html" id="tmpl-blogin-aarambha-panel">
    <div class="blogin-aarambha-rows">

        <# if ( data.device != 'all' ) { #>
            <# if ( ! _.isUndefined( data.rows.top ) ) { #>
            <div class="blogin-aarambha-row-top blogin-aarambha-row" data-row-id ="top" data-cols="{{ data.cols.top }}" data-id="{{ data.id }}_top">
                <div class="blogin-aarambha-row-inner">
                    <# for ( let i = 0; i < data.cols.top; i++ ) { #>
                    <div class="col-items-wrapper"><div data-id="col-{{ i }}" class="col-items col-{{ i }} d-flex justify-content-center"></div></div>
                    <# } #>
                </div>
                <a class="blogin-aarambha-row-settings" title="{{ data.rows.top }}" data-id="top" href="#"></a>
            </div>
            <#  } #>

            <# if ( ! _.isUndefined( data.rows.main ) ) { #>
            <div class="blogin-aarambha-row-main blogin-aarambha-row" data-row-id ="main" data-cols="{{ data.cols.main }}" data-id="{{ data.id }}_main">
                <div class="blogin-aarambha-row-inner">
                    <# for ( let i = 0; i < data.cols.main; i++ ) { #>
                    <div class="col-items-wrapper"><div data-id="col-{{ i }}" class="col-items col-{{ i }} d-flex justify-content-center"></div></div>
                    <# } #>
                </div>
                <a class="blogin-aarambha-row-settings" title="{{ data.rows.main }}" data-id="main" href="#"></a>
            </div>
            <#  } #>

            <# if ( ! _.isUndefined( data.rows.bottom ) ) { #>
            <div class="blogin-aarambha-row-bottom blogin-aarambha-row" data-row-id ="bottom" data-cols="{{ data.cols.bottom }}" data-id="{{ data.id }}_bottom">
                <div class="blogin-aarambha-row-inner">
                    <# for ( let i = 0; i < data.cols.bottom; i++ ) { #>
                    <div class="col-items-wrapper"><div data-id="col-{{ i }}" class="col-items col-{{ i }} d-flex justify-content-center"></div></div>
                    <# } #>
                </div>
                <a class="blogin-aarambha-row-settings" title="{{ data.rows.bottom }}" data-id="bottom" href="#"></a>
            </div>
            <#  } #>
        <# } #>

        <# if ( data.device == 'all' ) { #>
            <# if ( ! _.isUndefined( data.rows.sidebar ) ) { #>
            <div class="blogin-aarambha-row-sidebar blogin-aarambha-row" data-row-id ="sidebar" data-cols="{{ data.cols.sidebar }}" data-id="{{ data.id }}_sidebar">
                <div class="blogin-aarambha-row-inner">
                    <# for ( let i = 0; i < data.cols.sidebar; i++ ) { #>
                    <div class="col-items-wrapper"><div data-id="col-{{ i }}" class="col-items col-{{ i }}"></div></div>
                    <# } #>
                </div>
            </div>
            <#  } #>
        <# } #>

    </div>
</script>

<script type="text/html" id="tmpl-blogin-aarambha-item">
    <# const widgetsArea = ['sidebar-widgets-slide-in-box']; #>
    <div class="grid-stack-item item-from-list for-s-{{ data.section }}"
         title="{{ data.name }}"
         data-id="{{ data.id }}"
         data-section="{{ data.section }}"
    >
        <div class="item-tooltip" data-section="{{ data.section }}">{{ data.name }}</div>
        <div class="grid-stack-item-content">
            <div class="blogin-aarambha-customizer-builder-item-desc">
                <h3 class="blogin-aarambha-item-name" data-section="{{ data.section }}">{{ data.name }}</h3>
                <# if ( data.desc ) { #>
                <span class="blogin-aarambha-customizer-builder-item-desc">{{ data.desc }}</span>
                <# } #>
            </div>
            <span class="blogin-aarambha-item-remove blogin-aarambha-icon"></span>
            <span class="blogin-aarambha-item-setting blogin-aarambha-icon" data-section="{{ data.section }}"></span>
            <# if ( jQuery.inArray(data.section, widgetsArea) !== -1 ) { #>
                <span class="blogin-aarambha-item-design dashicons dashicons-admin-settings" data-tooltip="Design" data-section="{{ data.section }}-settings"></span>
            <# } #>

        </div>
    </div>
</script>
