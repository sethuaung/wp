<?php
/**
 * Blogin Aarambha Theme Customizer Footer Builder
 *
 * @package Blogin_Aarambha
 */

/**
 * Header Builder and Customizer Options
 *
 */

class Blogin_Aarambha_Customizer_Footer_Builder {

    /**
     * Panel ID, use for builder ID too
     *
     * @var string
     */
    public $panel = 'blogin_aarambha_footer';

    /**
     * Builder Sections and Controller ID
     *
     * @var string
     *
     */
    public $builder_section_controller = 'blogin_aarambha_footer_builder_controller_section';

    /*Builder Rows and Customizer Settings*/

    /**
     * Header Top Row and Its setting
     *
     * @var string
     *
     */
    public $footer_top = 'blogin_aarambha_footer_top';

    /**
     * Header Main Row and Its setting
     *
     * @var string
     *
     */
    public $footer_main = 'blogin_aarambha_footer_main';

    /**
     * Header Bottom Row and Its setting
     *
     * @var string
     *
     */
    public $footer_bottom = 'blogin_aarambha_footer_bottom';


    /**
     * Footer HTML Row and Its setting
     *
     * @var string
     *
     *
     */
    public $footer_html = 'footer_html';

    /*Footer Elements Section, Setting and Control ID*/
    /**
     * Copyright Section/Setting/Control ID
     *
     * @var string
     *
     *
     */
    public $footer_copyright = 'footer_copyright';

    /**
     * Footer Menu Section/Setting/Control ID
     *
     * @var string
     *
     *
     */
    public $footer_menu = 'footer_menu';

    /**
     * Footer Socials Section/Setting/Control ID
     *
     * @var string
     *
     *
     */
    public $footer_social = 'footer_social';

    /**
     * Footer Socials Section/Setting/Control ID
     *
     * @var string
     *
     *
     */
    public $footer_button = 'footer_button';

    /**
     * Footer Sidebars Section/Setting/Control ID
     *
     * @var string
     *
     *
     */
    public $footer_sidebar_1 = 'sidebar-widgets-footer-sidebar-1';
    public $footer_sidebar_2 = 'sidebar-widgets-footer-sidebar-2';
    public $footer_sidebar_3 = 'sidebar-widgets-footer-sidebar-3';
    public $footer_sidebar_4 = 'sidebar-widgets-footer-sidebar-4';
    public $footer_sidebar_5 = 'sidebar-widgets-footer-sidebar-5';
    public $footer_sidebar_6 = 'sidebar-widgets-footer-sidebar-6';


    /**
     * Main Instance
     *
     * Insures that only one instance of Blogin_Aarambha_Customizer_Footer_Builder exists in memory at any one
     * time. Also prevents needing to define globals all over the place.
     *
     * @return object
     */
    public static function instance() {

        // Store the instance locally to avoid private static replication
        static $instance = null;

        // Only run these methods if they haven't been ran previously
        if ( null === $instance ) {
            $instance = new Blogin_Aarambha_Customizer_Footer_Builder;
        }

        // Always return the instance
        return $instance;
    }

    /**
     *  Run functionality with hooks
     *
     * @return void
     */
    public function run() {

        add_action( 'customize_register', array( $this, 'set_customizer' ), 1 );

        add_action( 'customize_register', array( $this, 'customize_register' ), 3 );

        add_filter( 'blogin_aarambha_default_theme_options', array( $this, 'footer_defaults' ) );
        add_filter( 'blogin_aarambha_builders', array( $this, 'footer_builder' ) );

        add_action( 'blogin_aarambha_footer', array( $this, 'blogin_aarambha_footer_display' ), 100 );
    }

    /**
     * Callback functions for customize_register,
     * Fixed previous array issue
     *
     * @param null
     * @return void
     */
    public function set_customizer() {
        $builder = blogin_aarambha_get_footer_builder_options( Blogin_Aarambha_Customizer_Footer_Builder()->builder_section_controller );
        if ( is_array( $builder ) ) {
            $builder = json_encode( urldecode_deep( $builder ), true );
        }
        set_theme_mod( Blogin_Aarambha_Customizer_Footer_Builder()->builder_section_controller, $builder );
    }

    /**
     * Get footer builder
     *
     * @param null
     * @return void
     */
    public function get_builder() {
        $builder = blogin_aarambha_get_footer_builder_options( Blogin_Aarambha_Customizer_Footer_Builder()->builder_section_controller );
        if ( ! is_array( $builder ) ) {
            $builder = json_decode( urldecode_deep( $builder ), true );
        }
        return $builder;
    }


    /**
     * Callback functions for blogin_aarambha_default_theme_options,
     * Add Footer Builder defaults values
     *
     * @param array $default_options
     * @return array
     */
    public function footer_defaults( $default_options = array() ) {

        $footer_defaults = [

            $this->builder_section_controller => [
                'desktop'   => [
                    'bottom'      => [
                        'col-0'      => [
                            [
                                'id'    => 'footer_copyright'
                            ]
                        ],
                        'col-2'      => [
                            [
                                'id'    => 'footer_social'
                            ]
                        ]
                    ]
                ]
            ]
        ];
        return array_merge( $default_options, $footer_defaults );
    }

    /**
     * Callback functions for blogin_aarambha_builders,
     * Add Header Builder elements
     *
     * @param array $builder builder fields
     * @return array
     */
    public function footer_builder( $builder ) {

        $items = apply_filters(
            'Blogin_Aarambha_Customizer_Footer_Builder_items',
            array(
                Blogin_Aarambha_Customizer_Footer_Builder()->footer_copyright   => array(
                    'name'    => esc_html__( 'Copyright', 'blogin-aarambha' ),
                    'id'      => Blogin_Aarambha_Customizer_Footer_Builder()->footer_copyright,
                    'section' => Blogin_Aarambha_Customizer_Footer_Builder()->footer_copyright,
                ),
                Blogin_Aarambha_Customizer_Footer_Builder()->footer_menu => array(
                    'name'    => esc_html__( 'Footer Menu', 'blogin-aarambha' ),
                    'id'      => Blogin_Aarambha_Customizer_Footer_Builder()->footer_menu,
                    'section' => Blogin_Aarambha_Customizer_Footer_Builder()->footer_menu,
                ),
                Blogin_Aarambha_Customizer_Footer_Builder()->footer_social    => array(
                    'name'    => esc_html__( 'Social Icons', 'blogin-aarambha' ),
                    'id'      => Blogin_Aarambha_Customizer_Footer_Builder()->footer_social,
                    'section' => Blogin_Aarambha_Customizer_Footer_Builder()->footer_social,
                ),
                Blogin_Aarambha_Customizer_Footer_Builder()->footer_button    => array(
                    'name'    => esc_html__( 'Button', 'blogin-aarambha' ),
                    'id'      => Blogin_Aarambha_Customizer_Footer_Builder()->footer_button,
                    'section' => Blogin_Aarambha_Customizer_Footer_Builder()->footer_button,
                ),
                Blogin_Aarambha_Customizer_Footer_Builder()->footer_html  => array(
                    'name'    => esc_html__( 'HTML', 'blogin-aarambha' ),
                    'id'      => Blogin_Aarambha_Customizer_Footer_Builder()->footer_html,
                    'section' => Blogin_Aarambha_Customizer_Footer_Builder()->footer_html,
                ),
                Blogin_Aarambha_Customizer_Footer_Builder()->footer_sidebar_1   => array(
                    'name'    => esc_html__( 'Footer Sidebar 1', 'blogin-aarambha' ),
                    'id'      => Blogin_Aarambha_Customizer_Footer_Builder()->footer_sidebar_1,
                    'section' => Blogin_Aarambha_Customizer_Footer_Builder()->footer_sidebar_1,
                ),
                Blogin_Aarambha_Customizer_Footer_Builder()->footer_sidebar_2   => array(
                    'name'    => esc_html__( 'Footer Sidebar 2', 'blogin-aarambha' ),
                    'id'      => Blogin_Aarambha_Customizer_Footer_Builder()->footer_sidebar_2,
                    'section' => Blogin_Aarambha_Customizer_Footer_Builder()->footer_sidebar_2,
                ),
                Blogin_Aarambha_Customizer_Footer_Builder()->footer_sidebar_3   => array(
                    'name'    => esc_html__( 'Footer Sidebar 3', 'blogin-aarambha' ),
                    'id'      => Blogin_Aarambha_Customizer_Footer_Builder()->footer_sidebar_3,
                    'section' => Blogin_Aarambha_Customizer_Footer_Builder()->footer_sidebar_3,
                ),
                Blogin_Aarambha_Customizer_Footer_Builder()->footer_sidebar_4   => array(
                    'name'    => esc_html__( 'Footer Sidebar 4', 'blogin-aarambha' ),
                    'id'      => Blogin_Aarambha_Customizer_Footer_Builder()->footer_sidebar_4,
                    'section' => Blogin_Aarambha_Customizer_Footer_Builder()->footer_sidebar_4,
                ),
                Blogin_Aarambha_Customizer_Footer_Builder()->footer_sidebar_5   => array(
                    'name'    => esc_html__( 'Footer Sidebar 5', 'blogin-aarambha' ),
                    'id'      => Blogin_Aarambha_Customizer_Footer_Builder()->footer_sidebar_5,
                    'section' => Blogin_Aarambha_Customizer_Footer_Builder()->footer_sidebar_5,
                ),
                Blogin_Aarambha_Customizer_Footer_Builder()->footer_sidebar_6   => array(
                    'name'    => esc_html__( 'Footer Sidebar 6', 'blogin-aarambha' ),
                    'id'      => Blogin_Aarambha_Customizer_Footer_Builder()->footer_sidebar_6,
                    'section' => Blogin_Aarambha_Customizer_Footer_Builder()->footer_sidebar_6,
                )
            )
        );

        $footer_builder = array(
            Blogin_Aarambha_Customizer_Footer_Builder()->panel => array(
                'id'         => Blogin_Aarambha_Customizer_Footer_Builder()->panel,
                'title'      => esc_html__( 'Footer Builder', 'blogin-aarambha' ),
                'control_id' => Blogin_Aarambha_Customizer_Footer_Builder()->builder_section_controller,
                'panel'      => Blogin_Aarambha_Customizer_Footer_Builder()->panel,
                'section'    => Blogin_Aarambha_Customizer_Footer_Builder()->builder_section_controller,
                'devices'    => array(
                    'desktop' => esc_html__( 'Global', 'blogin-aarambha' )
                ),
                'items'      => $items,
                'rows'       => array(
                    'top'       => esc_html__( 'Top Row', 'blogin-aarambha' ),
                    'main'      => esc_html__( 'Main Row', 'blogin-aarambha' ),
                    'bottom'    => esc_html__( 'Bottom Row', 'blogin-aarambha' )
                ),
                'cols'       => array(
                    'top'       => 3,
                    'main'      => 3,
                    'bottom'    => 3
                )
            ),
        );
        $footer_builder = apply_filters( 'Blogin_Aarambha_Customizer_Footer_Builder', $footer_builder );
        return array_merge( $builder, $footer_builder );

    }

    /**
     * Callback functions for customize_register,
     * Add Panel Section control
     *
     * @param object $wp_customize
     * @return void
     */
    public function customize_register( $wp_customize ) {

        $footer_defaults = self::footer_defaults();
        /**
         * Add Panels
         */
        $wp_customize->add_panel(
            $this->panel,
            array(
                'title'     => esc_html__( 'Footer Builder', 'blogin-aarambha' ),
                'priority'  => 20
            ) );

        /**
         * Add Sections.
         */

        $wp_customize->add_section(
            $this->builder_section_controller,
            array(
                'title'    => esc_html__( 'Footer Builder', 'blogin-aarambha' ),
                'priority' => 10,
                'panel'    => $this->panel,
            )
        );

        $wp_customize->add_section(
            $this->footer_top,
            array(
                'title'    => esc_html__( 'Top Row', 'blogin-aarambha' ),
                'priority' => 20,
                'panel'    => $this->panel,
            )
        );
        $wp_customize->add_section(
            $this->footer_main,
            array(
                'title'    => esc_html__( 'Main Row', 'blogin-aarambha' ),
                'priority' => 25,
                'panel'    => $this->panel,
            )
        );
        $wp_customize->add_section(
            $this->footer_bottom,
            array(
                'title'    => esc_html__( 'Bottom Row', 'blogin-aarambha' ),
                'priority' => 25,
                'panel'    => $this->panel,
            )
        );

        $wp_customize->add_section(
            $this->footer_copyright,
            array(
                'title'    => esc_html__( 'Copyright', 'blogin-aarambha' ),
                'priority' => 35,
                'panel'    => $this->panel,
            )
        );
        $wp_customize->add_section(
            $this->footer_social,
            array(
                'title'    => esc_html__( 'Social Icons', 'blogin-aarambha' ),
                'priority' => 36,
                'panel'    => $this->panel,
            )
        );
        $wp_customize->add_section(
            $this->footer_button,
            array(
                'title'    => esc_html__( 'Button', 'blogin-aarambha' ),
                'priority' => 37,
                'panel'    => $this->panel,
            )
        );
        $wp_customize->add_section(
            $this->footer_html,
            array(
                'title'    => esc_html__( 'HTML', 'blogin-aarambha' ),
                'priority' => 38,
                'panel'    => $this->panel,
            )
        );
        $wp_customize->add_section(
            $this->footer_menu,
            array(
                'title'    => esc_html__( 'Footer Menu', 'blogin-aarambha' ),
                'priority' => 39,
                'panel'    => $this->panel,
            )
        );

        $wp_customize->add_section(
            'blogin_aarambha_footer_builder_widget_section',
            array(
                'title'    => esc_html__( 'Widget Settings', 'blogin-aarambha' ),
                'priority' => 65,
                'panel'    => $this->panel,
            )
        );

        $wp_customize->add_section(
            'blogin_aarambha_footer_builder_back_to_top_section',
            array(
                'title'    => esc_html__( 'Scroll to Top', 'blogin-aarambha' ),
                'priority' => 65,
                'panel'    => $this->panel,
            )
        );

        /**
         * Builder control and setting
         */
        $wp_customize->add_setting(
            $this->builder_section_controller,
            array(
                'default'           => $footer_defaults[ $this->builder_section_controller ],
                'sanitize_callback' => 'blogin_aarambha_sanitize_field',
                'transport'         => 'postMessage',
            )
        );

        $wp_customize->add_control(
            $this->builder_section_controller,
            array(
                'label'    => esc_html__( 'Header Builder', 'blogin-aarambha' ),
                'section'  => $this->builder_section_controller,
                'settings' => $this->builder_section_controller,
                'type'     => 'text',
            )
        );

        // Footer Builder Options
        require BLOGIN_AARAMBHA_DIR  . 'inc/customizer/builder/footer/options/Blogin_Aarambha_Customize_Footer_Top_Row_Fields.php';
        require BLOGIN_AARAMBHA_DIR  . 'inc/customizer/builder/footer/options/Blogin_Aarambha_Customize_Footer_Main_Row_Fields.php';
        require BLOGIN_AARAMBHA_DIR  . 'inc/customizer/builder/footer/options/Blogin_Aarambha_Customize_Footer_Bottom_Row_Fields.php';

        require BLOGIN_AARAMBHA_DIR  . 'inc/customizer/builder/footer/options/Blogin_Aarambha_Customize_Footer_Copyright_Fields.php';
        require BLOGIN_AARAMBHA_DIR  . 'inc/customizer/builder/footer/options/Blogin_Aarambha_Customize_Footer_Social_Icons_Fields.php';
        require BLOGIN_AARAMBHA_DIR  . 'inc/customizer/builder/footer/options/Blogin_Aarambha_Customize_Footer_Button_Fields.php';
        require BLOGIN_AARAMBHA_DIR  . 'inc/customizer/builder/footer/options/Blogin_Aarambha_Customize_Footer_Menu_Fields.php';
        require BLOGIN_AARAMBHA_DIR  . 'inc/customizer/builder/footer/options/Blogin_Aarambha_Customize_Footer_Html_Fields.php';

        // Back to top
        require BLOGIN_AARAMBHA_DIR  . 'inc/customizer/builder/footer/options/Blogin_Aarambha_Customize_Footer_Widget_Fields.php';
        require BLOGIN_AARAMBHA_DIR  . 'inc/customizer/builder/footer/options/Blogin_Aarambha_Customize_Footer_Back_To_Top_Fields.php';
    }

	/**
	 *Column Element
	 *
	 * @param $column_elements
	 */
	public function column_elements( $column_elements) {
		foreach ( $column_elements as $element ) {
			$id     = $element['id'];
			if ( file_exists( trailingslashit( get_template_directory() ) . 'template-parts/footer/' . esc_html( $id ). '.php' ) ) {
				get_template_part( 'template-parts/footer/' . esc_html( $id ) );
			} else {
				echo esc_html__( 'Create New File ', 'blogin-aarambha' ) . 'template-parts/footer/' . esc_html( $id ) . '.php';
			}
		}
	}

    /**
     * Callback Function For blogin_aarambha_action_footer
     * Display Header Content
     *
     * @return void
     */
    public function blogin_aarambha_footer_display() {

        $builder = $this->get_builder();

        if ( isset( $builder['desktop'] ) && ! empty( $builder['desktop'] ) ) {

            $footer_builder_data    = [];
            $active_sidebar         = [];
            $footer_builder         = $builder['desktop'];

            foreach ( $footer_builder as $key => $single_row ) {

                if ( ! empty( $single_row ) ) {

                    foreach ( $single_row as $col_key => $columns ) {

                        if ( ! empty( $columns ) ) {

                            $footer_builder_data[$key][$col_key]    = $columns;
                            $active_sidebar[]                       = $columns[0]['id'];
                        }

                    }

                }

            }
            if ( ! empty( $footer_builder_data ) ) {

                $this->footer_content( $footer_builder_data );
            }
            // Load sidebar template parts
            self::get_elements($active_sidebar);
        }

    }

    /**
     * Display Desktop Header Content
     *
     * @param $footer_builder
     * @return void
     */
    public function footer_content( $footer_builder ) {

        ?>
        <div id="desktop-footer" class="desktop-footer-wrap" data-device="desktop">
            <?php
            if ( isset( $footer_builder['top'] ) ) {
                $top_elements       = $footer_builder['top'];
                $top_elements_count = count( $top_elements );
                $top_col_per_row    = [
                    'desktop'           => $top_elements_count,
                    'tablet'            => $top_elements_count <= 2 ? $top_elements_count : 2,
                    'mobile'            => 1
                ];
                ?>
				<div class="d-flex align-items-center top-footer">
                    <div class="container">
						<div class="row columns"<?php Blogin_Aarambha_Helper::get_data_columns( $top_col_per_row );?>>
							<?php if ( array_key_exists('col-0', $top_elements ) ) :
								// Left Column Content Justify
								$top_row_left_col = get_theme_mod(
									'blogin_aarambha_footer_top_row_left_col_content_justify',
									[
										'desktop'   => 'start',
										'tablet'    => 'start',
										'mobile'    => 'start'
									]
								);
								$top_lef_col_class     = ['column d-flex flex-wrap'];
								$top_lef_col_class[]   = blogin_aarambha_get_classes($top_row_left_col,'justify-content-');
								?>
								<div class="<?php echo esc_attr( implode( ' ', $top_lef_col_class) );?>">
									<?php $this->column_elements( $top_elements['col-0'] ); ?>
								</div>
							<?php endif; ?>
							<?php if ( array_key_exists('col-1', $top_elements ) ) :
								// Center Column Content Justify
								$top_row_center_col = get_theme_mod(
									'blogin_aarambha_footer_top_row_center_col_content_justify',
									[
										'desktop'   => 'center',
										'tablet'    => 'center',
										'mobile'    => 'center'
									]
								);
								$top_center_col_class      = ['column d-flex flex-wrap'];
								$top_center_col_class[]    = blogin_aarambha_get_classes($top_row_center_col,'justify-content-');
								?>
								<div class="<?php echo esc_attr( implode( ' ', $top_center_col_class) );?>">
									<?php $this->column_elements( $top_elements['col-1'] ); ?>
								</div>
							<?php endif; ?>
							<?php if ( array_key_exists('col-2', $top_elements ) ) :
								// Right Column Content Justify
								$top_row_right_col = get_theme_mod(
									'blogin_aarambha_footer_top_row_right_col_content_justify',
									[
										'desktop'   => 'end',
										'tablet'    => 'end',
										'mobile'    => 'end'
									]
								);
								$top_right_col_class   = ['column d-flex flex-wrap'];
								$top_right_col_class[] = blogin_aarambha_get_classes($top_row_right_col,'justify-content-');
								?>
								<div class="<?php echo esc_attr( implode( ' ', $top_right_col_class) );?>">
									<?php $this->column_elements( $top_elements['col-2'] ); ?>
								</div>
							<?php endif; ?>
						</div>
                    </div>
                </div><!-- .top-footer -->
                <?php

            }
            if ( isset( $footer_builder['main'] ) ) {
                $main_elements       = $footer_builder['main'];
                $main_elements_count = count( $main_elements );
                $main_col_per_row    = [
                    'desktop'           => $main_elements_count,
                    'tablet'            => $main_elements_count <= 2 ? $main_elements_count : 2,
                    'mobile'            => 1
                ];
				?>
				<div class="d-flex align-items-center main-footer">
                    <div class="container">
						<div class="row columns"<?php Blogin_Aarambha_Helper::get_data_columns( $main_col_per_row );?>>
							<?php if ( array_key_exists('col-0', $main_elements ) ) :
								// Left Column Content Justify
								$main_row_left_col = get_theme_mod(
									'blogin_aarambha_footer_main_row_left_col_content_justify',
									[
										'desktop'   => 'start',
										'tablet'    => 'start',
										'mobile'    => 'start'
									]
								);
								$main_lef_col_class     = ['column d-flex flex-wrap'];
								$main_lef_col_class[]   = blogin_aarambha_get_classes($main_row_left_col,'justify-content-');
								?>
								<div class="<?php echo esc_attr( implode( ' ', $main_lef_col_class) );?>">
									<?php $this->column_elements( $main_elements['col-0'] ); ?>
								</div>
							<?php endif; ?>
							<?php if ( array_key_exists('col-1', $main_elements ) ) :
								// Center Column Content Justify
								$main_row_center_col = get_theme_mod(
									'blogin_aarambha_footer_main_row_center_col_content_justify',
									[
										'desktop'   => 'center',
										'tablet'    => 'center',
										'mobile'    => 'center'
									]
								);
								$main_center_col_class      = ['column d-flex flex-wrap'];
								$main_center_col_class[]    = blogin_aarambha_get_classes($main_row_center_col,'justify-content-');
								?>
								<div class="<?php echo esc_attr( implode( ' ', $main_center_col_class) );?>">
									<?php $this->column_elements( $main_elements['col-1'] ); ?>
								</div>
							<?php endif; ?>
							<?php if ( array_key_exists('col-2', $main_elements ) ) :
								// Right Column Content Justify
								$main_row_right_col = get_theme_mod(
									'blogin_aarambha_footer_main_row_right_col_content_justify',
									[
										'desktop'   => 'end',
										'tablet'    => 'end',
										'mobile'    => 'end'
									]
								);
								$main_right_col_class   = ['column d-flex flex-wrap'];
								$main_right_col_class[] = blogin_aarambha_get_classes($main_row_right_col,'justify-content-');
								?>
								<div class="<?php echo esc_attr( implode( ' ', $main_right_col_class) );?>">
									<?php $this->column_elements( $main_elements['col-2'] ); ?>
								</div>
							<?php endif; ?>
						</div>
                    </div>
                </div><!-- .main-footer -->
                <?php

            }
            if ( isset( $footer_builder['bottom'] ) ) {
                $bottom_elements       = $footer_builder['bottom'];
                $bottom_elements_count = count( $bottom_elements );
                $bottom_col_per_row    = [
                    'desktop'           => $bottom_elements_count,
                    'tablet'            => $bottom_elements_count <= 2 ? $bottom_elements_count : 2,
                    'mobile'            => 1
                ];
				?>
				<div class="d-flex align-items-center bottom-footer">
                    <div class="container">
                        <div class="row columns"<?php Blogin_Aarambha_Helper::get_data_columns( $bottom_col_per_row );?>>
							<?php if ( array_key_exists('col-0', $bottom_elements ) ) :
								// Left Column Content Justify
								$bottom_row_left_col = get_theme_mod(
									'blogin_aarambha_footer_bottom_row_left_col_content_justify',
									[
										'desktop'   => 'start',
										'tablet'    => 'start',
										'mobile'    => 'center'
									]
								);
								$bottom_lef_col_class     = ['column d-flex flex-wrap'];
								$bottom_lef_col_class[]   = blogin_aarambha_get_classes($bottom_row_left_col,'justify-content-');
								?>
								<div class="<?php echo esc_attr( implode( ' ', $bottom_lef_col_class) );?>">
									<?php $this->column_elements( $bottom_elements['col-0'] ); ?>
								</div>
							<?php endif; ?>
							<?php if ( array_key_exists('col-1', $bottom_elements ) ) :
								// Center Column Content Justify
								$bottom_row_center_col = get_theme_mod(
									'blogin_aarambha_footer_bottom_row_center_col_content_justify',
									[
										'desktop'   => 'center',
										'tablet'    => 'center',
										'mobile'    => 'center'
									]
								);
								$bottom_center_col_class      = ['column d-flex flex-wrap'];
								$bottom_center_col_class[]    = blogin_aarambha_get_classes($bottom_row_center_col,'justify-content-');
								?>
								<div class="<?php echo esc_attr( implode( ' ', $bottom_center_col_class) );?>">
									<?php $this->column_elements( $bottom_elements['col-1'] ); ?>
								</div>
							<?php endif; ?>
							<?php if ( array_key_exists('col-2', $bottom_elements ) ) :
								// Right Column Content Justify
								$bottom_row_right_col = get_theme_mod(
									'blogin_aarambha_footer_bottom_row_right_col_content_justify',
									[
										'desktop'   => 'end',
										'tablet'    => 'end',
										'mobile'    => 'center'
									]
								);
								$bottom_right_col_class   = ['column d-flex flex-wrap'];
								$bottom_right_col_class[] = blogin_aarambha_get_classes($bottom_row_right_col,'justify-content-');
								?>
								<div class="<?php echo esc_attr( implode( ' ', $bottom_right_col_class) );?>">
									<?php $this->column_elements( $bottom_elements['col-2'] ); ?>
								</div>
							<?php endif; ?>
                        </div>
                    </div>
                </div><!-- .bottom-footer -->
                <?php
            }
            ?>
        </div><!-- #desktop-footer -->
        <?php

    }


    /**
     * Footer get_elements only for the sidebar so that we can see sidebar in customizer
     *
     * @param $sidebar_elements array
     * @return void
     */
    public function get_elements( $sidebar_elements ) {

        if ( is_array( $sidebar_elements ) ) {

            $sidebar_array = [
                'sidebar-widgets-footer-sidebar-1',
                'sidebar-widgets-footer-sidebar-2',
                'sidebar-widgets-footer-sidebar-3',
                'sidebar-widgets-footer-sidebar-4',
                'sidebar-widgets-footer-sidebar-5',
                'sidebar-widgets-footer-sidebar-6'
            ];
            $sidebar_elements = array_diff( $sidebar_array, $sidebar_elements);
            echo '<div class="container d-none">';
            foreach ( $sidebar_elements as $key ) {
                if ( file_exists( trailingslashit( get_template_directory() ) . 'template-parts/footer/' . esc_html($key) . '.php' ) ) {
                    get_template_part( 'template-parts/footer/' . esc_html($key) );
                }
            }
            echo '</div><!-- .d-none -->';
        }
    }

}

/**
 * Create Instance for Blogin_Aarambha_Customizer_Footer_Builder
 *
 * @param
 * @return object
 */
if ( ! function_exists( 'blogin_aarambha_customizer_footer_builder' ) ) {

    function blogin_aarambha_customizer_footer_builder() {

        return Blogin_Aarambha_Customizer_Footer_Builder::instance();
    }

    blogin_aarambha_customizer_footer_builder()->run();
}

/**
 * Get footer builder default options
 *
 * @param null
 * @return mixed blogin_aarambha_theme_options
 *
 */
if ( ! function_exists( 'blogin_aarambha_get_footer_builder_options' ) ) :
    function blogin_aarambha_get_footer_builder_options( $key = '' ) {
        if ( ! empty( $key ) ) {
            $footer_default_values  = Blogin_Aarambha_Customizer_Footer_Builder()->footer_defaults();
            $theme_mod_values       = get_theme_mod( $key, isset( $footer_default_values[ $key ] ) ? $footer_default_values[ $key ] : '' );
            return apply_filters( 'blogin_aarambha_' . $key, $theme_mod_values );
        }
        return false;
    }
endif;


