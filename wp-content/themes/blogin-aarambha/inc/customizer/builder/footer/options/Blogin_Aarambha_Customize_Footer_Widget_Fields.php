<?php
/**
 * Blogin Aarambha Theme Customizer Footer Widget settings
 *
 * @package Blogin_Aarambha
 */

class Blogin_Aarambha_Customize_Footer_Widget_Fields extends Blogin_Aarambha_Customize_Base_Field {

    /**
     * Arguments for fields.
     *
     * @return void
     */
    public function init() {
        $this->args = [
			// Widgets Title
			'blogin_aarambha_footer_builder_widget_title_typo' => [
				'type'              => 'typography',
				'default'           => '',
				'sanitize_callback' => ['Blogin_Aarambha_Customizer_Sanitize_Callback', 'sanitize_typography' ],
				'label'             => esc_html__( 'Widget Title', 'blogin-aarambha' ),
				'section'           => 'blogin_aarambha_footer_builder_widget_section',
				'priority'          => 20,
				'inherits'          => [
					'color_1'           => 'var(--color-heading)',
				],
				'fields'			=> ['colors' => true]
			],
			// Widgets Content
			'blogin_aarambha_footer_builder_widget_content_typo' => [
				'type'              => 'typography',
				'default'           => '',
				'sanitize_callback' => ['Blogin_Aarambha_Customizer_Sanitize_Callback', 'sanitize_typography' ],
				'label'             => esc_html__( 'Widget Content', 'blogin-aarambha' ),
				'section'           => 'blogin_aarambha_footer_builder_widget_section',
				'priority'          => 25,
				'colors'            => [
					'color_1'           => esc_html__( 'Normal', 'blogin-aarambha' ),
					'color_2'           => esc_html__( 'Hover', 'blogin-aarambha' ),
				],
				'inherits'          => [
					'color_1'           => 'var(--color-2)',
					'color_2'           => 'var(--color-link-hover)',
				],
				'fields'			=> ['colors' => true]
			],
            // Padding
            'blogin_aarambha_footer_builder_widget_padding' => [
                'type'              => 'dimensions',
                'default'           => '',
                'sanitize_callback' => ['Blogin_Aarambha_Customizer_Sanitize_Callback', 'sanitize_dimensions' ],
                'label'             => esc_html__( 'Padding', 'blogin-aarambha' ),
                'description'       => esc_html__( 'Set each footer widgets wrapper padding.', 'blogin-aarambha' ),
                'section'           => 'blogin_aarambha_footer_builder_widget_section',
                'priority'          => 30,
                'responsive'        => [ 'desktop', 'tablet', 'mobile' ],
            ],
        ];
    }

}
new Blogin_Aarambha_Customize_Footer_Widget_Fields();
