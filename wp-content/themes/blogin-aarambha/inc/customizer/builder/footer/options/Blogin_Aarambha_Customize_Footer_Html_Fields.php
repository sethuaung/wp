<?php
/**
 * Blogin Aarambha Theme Customizer Footer HTML settings
 *
 * @package Blogin_Aarambha
 */

class Blogin_Aarambha_Customize_Footer_Html_Fields extends Blogin_Aarambha_Customize_Base_Field {

    /**
     * Arguments for fields.
     *
     * @return void
     */
    public function init() {
        $this->args = [
            // Grouping Settings
            'blogin_aarambha_footer_html_group_settings' => [
                'type'              => 'group',
                'section'           => 'footer_html',
                'priority'          => 10,
                'choices'           => [
                    'normal'            => array(
                        'tab-title'     => esc_html__( 'General', 'blogin-aarambha' ),
                        'controls'      => array(
                            'custom_logo',
                            'blogin_aarambha_footer_html_text'
                        )
                    ),
                    'hover'         => array(
                        'tab-title'     => esc_html__( 'Style', 'blogin-aarambha' ),
                        'controls'      => array(
                            'blogin_aarambha_footer_html_text_link_color',
                            'blogin_aarambha_footer_html_text_typo',
                            'blogin_aarambha_footer_html_padding',
                            'blogin_aarambha_footer_html_margin'
                        )
                    )
                ]
            ],
            // Textarea
            'blogin_aarambha_footer_html_text' => [
                'type'              => 'editor',
                'default'           => '',
                'sanitize_callback' => 'wp_kses_post',
                'label'             => esc_html__( 'HTML', 'blogin-aarambha' ),
                'description'       => esc_html__( 'Enter Text/Simple HTML Code', 'blogin-aarambha' ),
                'section'           => 'footer_html',
                'priority'          => 15,
            ],
            // Text Typo
            'blogin_aarambha_footer_html_text_typo' => [
                'type'              => 'typography',
                'default'           => '',
                'sanitize_callback' => ['Blogin_Aarambha_Customizer_Sanitize_Callback', 'sanitize_typography' ],
                'label'             => esc_html__( 'Text Color', 'blogin-aarambha' ),
                'section'           => 'footer_html',
                'priority'          => 20,
                'inherits'          => [
                    'color_1'           => 'var(--color-1)'
                ],
                'fields'            => ['colors'=>true]
            ],
            // link color
            'blogin_aarambha_footer_html_text_link_color' => [
                'type'              => 'color',
                'default'           => '',
                'sanitize_callback' => ['Blogin_Aarambha_Customizer_Sanitize_Callback', 'sanitize_color' ],
                'label'             => esc_html__( 'Link Color', 'blogin-aarambha' ),
                'section'           => 'footer_html',
                'colors'            => [
                    'color_1'           => esc_html__( 'Normal', 'blogin-aarambha' ),
                    'color_2'           => esc_html__( 'Hover', 'blogin-aarambha' ),
                ],
                'priority'          => 25,
                'inherits'          => [
                    'color_1'           => 'var(--color-link)',
                    'color_2'           => 'var(--color-link-hover)',
                ]
            ],
            // Padding
            'blogin_aarambha_footer_html_padding' => [
                'type'              => 'dimensions',
                'default'           => [
                    'desktop'           => [
                        'side_1'            => '10px',
                        'side_3'            => '10px',
                        'linked'            => 'off'
                    ]
                ],
                'sanitize_callback' => ['Blogin_Aarambha_Customizer_Sanitize_Callback', 'sanitize_dimensions' ],
                'label'             => esc_html__( 'Padding', 'blogin-aarambha' ),
                'description'       => esc_html__( 'Set HTML container padding.', 'blogin-aarambha' ),
                'section'           => 'footer_html',
                'priority'          => 55,
                'responsive'        => [ 'desktop', 'tablet', 'mobile' ],
            ],
            // Margin
            'blogin_aarambha_footer_html_margin' => [
                'type'              => 'dimensions',
                'default'           => '',
                'sanitize_callback' => ['Blogin_Aarambha_Customizer_Sanitize_Callback', 'sanitize_dimensions' ],
                'label'             => esc_html__( 'Margin', 'blogin-aarambha' ),
                'description'       => esc_html__( 'Set HTML container margin.', 'blogin-aarambha' ),
                'section'           => 'footer_html',
                'priority'          => 60,
                'responsive'        => [ 'desktop', 'tablet', 'mobile' ],
            ]
        ];
    }

}
new Blogin_Aarambha_Customize_Footer_Html_Fields();
