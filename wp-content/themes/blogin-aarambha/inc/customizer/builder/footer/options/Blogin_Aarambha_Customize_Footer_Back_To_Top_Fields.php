<?php
/**
 * Blogin Aarambha Theme Customizer Footer Back to Top Button settings
 *
 * @package Blogin_Aarambha
 */

class Blogin_Aarambha_Customize_Footer_Back_To_Top_Fields extends Blogin_Aarambha_Customize_Base_Field {

    /**
     * Arguments for fields.
     *
     * @return void
     */
    public function init() {
        $this->args = [
            // Enable
            'blogin_aarambha_footer_back_to_top_enable' => [
                'type'              => 'toggle',
                'default'           => ['desktop'=>'true'],
                'priority'          => 5,
                'sanitize_callback' => ['Blogin_Aarambha_Customizer_Sanitize_Callback', 'sanitize_toggle' ],
                'label'             => esc_html__( 'Scroll to Top', 'blogin-aarambha' ),
                'description'       => esc_html__( 'Enable button to scroll to top.', 'blogin-aarambha' ),
                'section'           => 'blogin_aarambha_footer_builder_back_to_top_section',
            ]
        ];
    }

}
new Blogin_Aarambha_Customize_Footer_Back_To_Top_Fields();
