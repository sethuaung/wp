<?php
/**
 * Blogin Aarambha Theme Customizer Footer Menu settings
 *
 * @package Blogin_Aarambha
 */

class Blogin_Aarambha_Customize_Footer_Menu_Fields extends Blogin_Aarambha_Customize_Base_Field {

    /**
     * Arguments for fields.
     *
     * @return void
     */
    public function init() {
        $this->args = [
            // Grouping Settings
            'blogin_aarambha_footer_menu_group_settings' => [
                'type'              => 'group',
                'section'           => 'footer_menu',
                'priority'          => 10,
                'choices'           => [
                    'normal'            => array(
                        'tab-title'     => esc_html__( 'General', 'blogin-aarambha' ),
                        'controls'      => array(
                            'blogin_aarambha_footer_menu_note_one',
                            'blogin_aarambha_footer_menu_spacing',
                        )
                    ),
                    'hover'         => array(
                        'tab-title'     => esc_html__( 'Style', 'blogin-aarambha' ),
                        'controls'      => array(
                            'blogin_aarambha_footer_menu_font_colors',
                            'blogin_aarambha_footer_menu_background_color',
							'blogin_aarambha_footer_menu_note_six',
							'blogin_aarambha_footer_menu_container_padding',
							'blogin_aarambha_footer_menu_container_margin'
                        )
                    )
                ]
            ],
            // Note One
            'blogin_aarambha_footer_menu_note_one' => [
                'type'              => 'heading',
                'description'       => sprintf(__( 'To set menu, go to <a data-type="section" data-id="menu_locations" class="customizer-focus"><strong>Footer Menu</strong></a>', 'blogin-aarambha' )),
                'section'           => 'footer_menu',
                'priority'          => 10,
            ],
            // Items Spacing
            'blogin_aarambha_footer_menu_spacing' => [
                'type'              => 'range',
                'default'           => '',
                'sanitize_callback' => ['Blogin_Aarambha_Customizer_Sanitize_Callback', 'sanitize_range' ],
                'label'             => esc_html__( 'Menu Spacing', 'blogin-aarambha' ),
                'description'       => esc_html__( 'Slide to set Menu Spacing.', 'blogin-aarambha' ),
                'section'           => 'footer_menu',
                'responsive'        => [ 'desktop', 'tablet', 'mobile' ],
                'priority'          => 20
            ],
            // Menu font color
            'blogin_aarambha_footer_menu_font_colors' => [
                'type'              => 'color',
                'default'           => '',
                'sanitize_callback' => ['Blogin_Aarambha_Customizer_Sanitize_Callback', 'sanitize_color' ],
                'label'             => esc_html__( 'Font Colors', 'blogin-aarambha' ),
                'description'       => esc_html__( 'Set menu normal and hover colors.', 'blogin-aarambha' ),
                'section'           => 'footer_menu',
                'colors'            => [
                    'color_1'           => esc_html__( 'Normal', 'blogin-aarambha' ),
                    'color_2'           => esc_html__( 'Hover', 'blogin-aarambha' )
                ],
                'priority'          => 35,
                'inherits'            => [
                    'color_1'           => 'var(--color-link)',
                    'color_2'           => 'var(--color-link-hover)',
                ],
            ],
            // Menu Background
            'blogin_aarambha_footer_menu_background_color' => [
                'type'              => 'color',
                'default'           => '',
                'sanitize_callback' => ['Blogin_Aarambha_Customizer_Sanitize_Callback', 'sanitize_color' ],
                'label'             => esc_html__( 'Background', 'blogin-aarambha' ),
                'description'       => esc_html__( 'Set menu each item background.', 'blogin-aarambha' ),
                'section'           => 'footer_menu',
                'colors'            => [
                    'color_1'           => esc_html__( 'Normal', 'blogin-aarambha' ),
                    'color_2'           => esc_html__( 'Hover', 'blogin-aarambha' )
                ],
                'priority'          => 40,
                'inherits'            => [
                    'color_1'           => 'var(--color-bg-1)',
                    'color_2'           => 'var(--color-bg-1)',
                ],
            ],
			// Heading four
			'blogin_aarambha_footer_menu_note_six' => [
				'type'              => 'heading',
				'label'             => esc_html__( 'CONTAINER', 'blogin-aarambha' ),
				'section'           => 'footer_menu',
				'priority'          => 65,
			],
			// Container Padding
			'blogin_aarambha_footer_menu_container_padding' => [
				'type'              => 'dimensions',
				'default'           => '',
				'sanitize_callback' => ['Blogin_Aarambha_Customizer_Sanitize_Callback', 'sanitize_dimensions' ],
				'label'             => esc_html__( 'Padding', 'blogin-aarambha' ),
				'description'       => esc_html__( 'Set Padding to the Footer Menu.', 'blogin-aarambha' ),
				'section'           => 'footer_menu',
				'responsive'        => [ 'desktop', 'tablet', 'mobile' ],
				'priority'          => 70,
			],
			// Container Margin
			'blogin_aarambha_footer_menu_container_margin' => [
				'type'              => 'dimensions',
				'default'           => '',
				'sanitize_callback' => ['Blogin_Aarambha_Customizer_Sanitize_Callback', 'sanitize_dimensions' ],
				'label'             => esc_html__( 'Margin', 'blogin-aarambha' ),
				'description'       => esc_html__( 'Set Margin to the Footer Menu.', 'blogin-aarambha' ),
				'section'           => 'footer_menu',
				'responsive'        => [ 'desktop', 'tablet', 'mobile' ],
				'priority'          => 75,
			],
        ];
    }

}
new Blogin_Aarambha_Customize_Footer_Menu_Fields();
