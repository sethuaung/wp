<?php
/**
 * Blogin Aarambha Theme Customizer Footer Copyright settings
 *
 * @package Blogin_Aarambha
 */

class Blogin_Aarambha_Customize_Footer_Copyright_Fields extends Blogin_Aarambha_Customize_Base_Field {

    /**
     * Arguments for fields.
     *
     * @return void
     */
    public function init() {
        $this->args = [
            // Grouping Settings
            'blogin_aarambha_footer_copyright_group_settings' => [
                'type'              => 'group',
                'section'           => 'footer_copyright',
                'priority'          => 10,
                'choices'           => [
                    'normal'            => array(
                        'tab-title'     => esc_html__( 'General', 'blogin-aarambha' ),
                        'controls'      => array(
                            'blogin_aarambha_footer_copyright_text',
                            'blogin_aarambha_footer_copyright_link_target'
                        )
                    ),
                    'hover'         => array(
                        'tab-title'     => esc_html__( 'Style', 'blogin-aarambha' ),
                        'controls'      => array(
                            'blogin_aarambha_footer_copyright_text_typo',
                            'blogin_aarambha_footer_copyright_padding',
                            'blogin_aarambha_footer_copyright_margin'
                        )
                    )
                ]
            ],
            // Textarea
            'blogin_aarambha_footer_copyright_text' => [
                'type'              => 'editor',
                'default'           => __( 'Copyright {copyright} {current_year} {site_title}', 'blogin-aarambha' ),
                'sanitize_callback' => 'wp_kses_post',
                'label'             => esc_html__( 'Copyright Text', 'blogin-aarambha' ),
                'description'       => esc_html__( 'You can insert some arbitrary HTML code tags: {current_year} and {site_title}', 'blogin-aarambha' ),
                'section'           => 'footer_copyright',
                'priority'          => 15,
            ],
            // Link Open
            'blogin_aarambha_footer_copyright_link_target' => [
                'type'              => 'toggle',
                'default'           => ['desktop'=>'true'],
                'section'           => 'footer_copyright',
                'priority'          => 20,
                'sanitize_callback' => ['Blogin_Aarambha_Customizer_Sanitize_Callback', 'sanitize_toggle' ],
                'label'             => esc_html__( 'Link Open', 'blogin-aarambha' ),
                'description'       => esc_html__( 'Toggle to enable link open in new window tab.', 'blogin-aarambha' ),
            ],
            // Text Typo
            'blogin_aarambha_footer_copyright_text_typo' => [
                'type'              => 'typography',
                'default'           => [
                    'colors'            => [
                        'color_1'           => 'var(--color-1)',
                        'color_2'           => 'var(--color-2)'
                    ]
                ],
                'sanitize_callback' => ['Blogin_Aarambha_Customizer_Sanitize_Callback', 'sanitize_typography' ],
                'label'             => esc_html__( 'Text', 'blogin-aarambha' ),
                'section'           => 'footer_copyright',
                'priority'          => 45,
                'colors'            => [
                    'color_1'           => esc_html__( 'Normal', 'blogin-aarambha' ),
                    'color_2'           => esc_html__( 'Link Hover', 'blogin-aarambha' )
                ],
                'inherits'          => [
                    'color_1'           => 'var(--color-1)',
                    'color_2'           => 'var(--color-2)'
                ],
                'fields'            => ['colors'=>true],
            ],
            // Padding
            'blogin_aarambha_footer_copyright_padding' => [
                'type'              => 'dimensions',
                'default'           => [
                    'desktop'           => [
                        'side_1'            => '10px',
                        'side_3'            => '10px',
                        'linked'            => 'off'
                    ]
                ],
                'sanitize_callback' => ['Blogin_Aarambha_Customizer_Sanitize_Callback', 'sanitize_dimensions' ],
                'label'             => esc_html__( 'Padding', 'blogin-aarambha' ),
                'section'           => 'footer_copyright',
                'priority'          => 55,
                'responsive'        => [ 'desktop', 'tablet', 'mobile' ],
            ],
            // Margin
            'blogin_aarambha_footer_copyright_margin' => [
                'type'              => 'dimensions',
                'default'           => '',
                'sanitize_callback' => ['Blogin_Aarambha_Customizer_Sanitize_Callback', 'sanitize_dimensions' ],
                'label'             => esc_html__( 'Margin', 'blogin-aarambha' ),
                'section'           => 'footer_copyright',
                'priority'          => 60,
                'responsive'        => [ 'desktop', 'tablet', 'mobile' ],
            ]
        ];
    }

}
new Blogin_Aarambha_Customize_Footer_Copyright_Fields();
