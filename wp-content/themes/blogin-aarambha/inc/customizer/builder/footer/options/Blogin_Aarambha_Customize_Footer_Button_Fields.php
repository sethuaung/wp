<?php
/**
 * Blogin Aarambha Theme Customizer Footer Button settings
 *
 * @package Blogin_Aarambha
 */

class Blogin_Aarambha_Customize_Footer_Button_Fields extends Blogin_Aarambha_Customize_Base_Field {

    /**
     * Arguments for fields.
     *
     * @return void
     */
    public function init() {
        $this->args = [
            // Grouping Settings
            'blogin_aarambha_footer_button_group_settings' => [
                'type'              => 'group',
                'section'           => 'footer_button',
                'priority'          => 10,
                'choices'           => [
                    'normal'            => array(
                        'tab-title'     => esc_html__( 'General', 'blogin-aarambha' ),
                        'controls'      => array(
                            'blogin_aarambha_footer_button_text',
                            'blogin_aarambha_footer_button_url',
                            'blogin_aarambha_footer_button_url_target'
                        )
                    ),
                    'hover'         => array(
                        'tab-title'     => esc_html__( 'Style', 'blogin-aarambha' ),
                        'controls'      => array(
                            'blogin_aarambha_footer_button_color',
                            'blogin_aarambha_footer_button_background',
                            'blogin_aarambha_footer_button_padding',
                            'blogin_aarambha_footer_button_margin'
                        )
                    )
                ]
            ],
            // Text
            'blogin_aarambha_footer_button_text' => [
                'type'              => 'text',
                'default'           => esc_html__( 'Button', 'blogin-aarambha' ),
                'sanitize_callback' => 'sanitize_text_field',
                'label'             => esc_html__( 'Text', 'blogin-aarambha' ),
                'section'           => 'footer_button',
                'priority'          => 20,
            ],
            // URL
            'blogin_aarambha_footer_button_url' => [
                'type'              => 'url',
                'default'           => '#',
                'sanitize_callback' => 'esc_url_raw',
                'label'             => esc_html__( 'URL', 'blogin-aarambha' ),
                'section'           => 'footer_button',
                'priority'          => 25,
            ],
            // Link Open
            'blogin_aarambha_footer_button_url_target' => [
                'type'              => 'toggle',
                'default'           => '',
                'section'           => 'footer_button',
                'priority'          => 50,
                'sanitize_callback' => ['Blogin_Aarambha_Customizer_Sanitize_Callback', 'sanitize_toggle' ],
                'label'             => esc_html__( 'Link Open', 'blogin-aarambha' ),
                'description'       => esc_html__( 'Enable to open the link in the new tab.', 'blogin-aarambha' ),
            ],
            // Button Color
            'blogin_aarambha_footer_button_color' => [
                'type'              => 'color',
                'default'           => [
                    'color_2'           => 'var(--color-link)'
                ],
                'sanitize_callback' => ['Blogin_Aarambha_Customizer_Sanitize_Callback', 'sanitize_color' ],
                'label'             => esc_html__( 'Button', 'blogin-aarambha' ),
                'description'       => esc_html__( 'Set button color.', 'blogin-aarambha' ),
                'section'           => 'footer_button',
                'colors'            => [
                    'color_1'           => esc_html__( 'Normal', 'blogin-aarambha' ),
                    'color_2'           => esc_html__( 'Hover', 'blogin-aarambha' ),
                ],
                'priority'          => 55,
                'inherits'          => [
                    'color_1'           => 'var(--color-link)',
                    'color_2'           => 'var(--color-link)',
                ]
            ],
            // Background
            'blogin_aarambha_footer_button_background' => [
                'type'              => 'color',
                'default'           => [
                    'color_2'           => 'var(--color-bg-3)'
                ],
                'sanitize_callback' => ['Blogin_Aarambha_Customizer_Sanitize_Callback', 'sanitize_color' ],
                'label'             => esc_html__( 'Background', 'blogin-aarambha' ),
                'description'       => esc_html__( 'Set button background.', 'blogin-aarambha' ),
                'section'           => 'footer_button',
                'colors'            => [
                    'color_1'           => esc_html__( 'Normal', 'blogin-aarambha' ),
                    'color_2'           => esc_html__( 'Hover', 'blogin-aarambha' ),
                ],
                'priority'          => 60,
                'inherits'          => [
                    'color_1'           => 'var(--color-bg-1)',
                    'color_2'           => 'var(--color-bg-3)',
                ]
            ],
            // Padding
            'blogin_aarambha_footer_button_padding' => [
                'type'              => 'dimensions',
                'default'           => [
                    'desktop'           => [
                        'side_1'            => '7px',
                        'side_2'            => '15px',
                        'side_3'            => '7px',
                        'side_4'            => '15px',
                        'linked'            => 'off'
                    ]
                ],
                'sanitize_callback' => ['Blogin_Aarambha_Customizer_Sanitize_Callback', 'sanitize_dimensions' ],
                'label'             => esc_html__( 'Padding', 'blogin-aarambha' ),
                'description'       => esc_html__( 'Set button padding.', 'blogin-aarambha' ),
                'section'           => 'footer_button',
                'priority'          => 75,
                'responsive'        => [ 'desktop', 'tablet', 'mobile' ],
            ],
            // Margin
            'blogin_aarambha_footer_button_margin' => [
                'type'              => 'dimensions',
                'default'           => [
                    'desktop'           => [
                        'side_1'            => '5px',
                        'side_2'            => '5px',
                        'side_3'            => '5px',
                        'side_4'            => '5px',
                        'linked'            => 'on'
                    ]
                ],
                'sanitize_callback' => ['Blogin_Aarambha_Customizer_Sanitize_Callback', 'sanitize_dimensions' ],
                'label'             => esc_html__( 'Margin', 'blogin-aarambha' ),
                'description'       => esc_html__( 'Set button margin.', 'blogin-aarambha' ),
                'section'           => 'footer_button',
                'priority'          => 80,
                'responsive'        => [ 'desktop', 'tablet', 'mobile' ],
            ],

        ];
    }

}
new Blogin_Aarambha_Customize_Footer_Button_Fields();
