<?php
/**
 * Blogin Aarambha Theme Customizer Footer Bottom Row settings
 *
 * @package Blogin_Aarambha
 */

class Blogin_Aarambha_Customize_Footer_Bottom_Row_Fields extends Blogin_Aarambha_Customize_Base_Field {

    /**
     * Arguments for fields.
     *
     * @return void
     */
    public function init() {
        $this->args = [
            // Grouping Settings
            'blogin_aarambha_footer_bottom_row_group_settings' => [
                'type'              => 'group',
                'section'           => 'blogin_aarambha_footer_bottom',
                'priority'          => 5,
                'choices'           => [
                    'normal'            => array(
                        'tab-title'     => esc_html__( 'General', 'blogin-aarambha' ),
                        'controls'      => array(
                            'blogin_aarambha_footer_bottom_row_left_col_content_justify',
                            'blogin_aarambha_footer_bottom_row_center_col_content_justify',
                            'blogin_aarambha_footer_bottom_row_right_col_content_justify'
                        )
                    ),
                    'hover'         => array(
                        'tab-title'     => esc_html__( 'Style', 'blogin-aarambha' ),
                        'controls'      => array(
                            'blogin_aarambha_footer_bottom_row_background_overlay',
                            'blogin_aarambha_footer_bottom_row_padding'
                        )
                    )
                ]
            ],
			// Left Column Justify Content
			'blogin_aarambha_footer_bottom_row_left_col_content_justify' => [
				'type'              => 'buttonset',
				'default'           => [
					'desktop'   => 'start',
					'tablet'    => 'start',
					'mobile'    => 'center'
				],
				'sanitize_callback' => ['Blogin_Aarambha_Customizer_Sanitize_Callback', 'sanitize_buttonset' ],
				'label'             => esc_html__( 'Left Column', 'blogin-aarambha' ),
				'description'       => esc_html__( 'Choose position for the content in the Left Column.', 'blogin-aarambha' ),
				'section'           => 'blogin_aarambha_footer_bottom',
				'priority'          => 17,
				'choices'           => [
					'start'     => esc_html__( 'Start', 'blogin-aarambha' ),
					'center'    => esc_html__( 'Center', 'blogin-aarambha' ),
					'end'       => esc_html__( 'End', 'blogin-aarambha' )
				],
				'responsive'        => ['desktop','tablet','mobile'],
			],
			// Center Column Justify Content
			'blogin_aarambha_footer_bottom_row_center_col_content_justify' => [
				'type'              => 'buttonset',
				'default'           => [
					'desktop'   => 'center',
					'tablet'    => 'center',
					'mobile'    => 'center'
				],
				'sanitize_callback' => ['Blogin_Aarambha_Customizer_Sanitize_Callback', 'sanitize_buttonset' ],
				'label'             => esc_html__( 'Center Column', 'blogin-aarambha' ),
				'description'       => esc_html__( 'Choose position for the content in the Center Column.', 'blogin-aarambha' ),
				'section'           => 'blogin_aarambha_footer_bottom',
				'priority'          => 18,
				'choices'           => [
					'start'     => esc_html__( 'Start', 'blogin-aarambha' ),
					'center'    => esc_html__( 'Center', 'blogin-aarambha' ),
					'end'       => esc_html__( 'End', 'blogin-aarambha' )
				],
				'responsive'        => ['desktop','tablet','mobile'],
			],
			// Right Column Justify Content
			'blogin_aarambha_footer_bottom_row_right_col_content_justify' => [
				'type'              => 'buttonset',
				'default'           => [
					'desktop'   => 'end',
					'tablet'    => 'end',
					'mobile'    => 'center'
				],
				'sanitize_callback' => ['Blogin_Aarambha_Customizer_Sanitize_Callback', 'sanitize_buttonset' ],
				'label'             => esc_html__( 'Right Column', 'blogin-aarambha' ),
				'description'       => esc_html__( 'Choose position for the content in the Right Column.', 'blogin-aarambha' ),
				'section'           => 'blogin_aarambha_footer_bottom',
				'priority'          => 19,
				'choices'           => [
					'start'     => esc_html__( 'Start', 'blogin-aarambha' ),
					'center'    => esc_html__( 'Center', 'blogin-aarambha' ),
					'end'       => esc_html__( 'End', 'blogin-aarambha' )
				],
				'responsive'        => ['desktop','tablet','mobile'],
			],
            // Background Overlay
            'blogin_aarambha_footer_bottom_row_background_overlay' => [
                'type'              => 'background',
                'default'           => [
                    'background'        => 'color',
                    'colors'            => [
                        'color_1'           => 'var(--color-bg-3)'
                    ]
                ],
                'sanitize_callback' => ['Blogin_Aarambha_Customizer_Sanitize_Callback', 'sanitize_background' ],
                'label'             => esc_html__( 'Background Overlay', 'blogin-aarambha' ),
                'description'       => esc_html__( 'Set Background overlay color for bottom row container.', 'blogin-aarambha' ),
                'section'           => 'blogin_aarambha_footer_bottom',
                'priority'          => 20,
                'inherits'          => [
                    'color_1'           => 'var(--color-bg-3)'
                ],
                'fields'            => ['colors' => true],
            ],
            // Padding
            'blogin_aarambha_footer_bottom_row_padding' => [
                'type'              => 'dimensions',
                'default'           => '',
                'sanitize_callback' => ['Blogin_Aarambha_Customizer_Sanitize_Callback', 'sanitize_dimensions' ],
                'label'             => esc_html__( 'Padding', 'blogin-aarambha' ),
                'section'           => 'blogin_aarambha_footer_bottom',
                'priority'          => 35,
                'description'       => esc_html__( 'Set footer bottom row padding.', 'blogin-aarambha' ),
                'responsive'        => [ 'desktop', 'tablet', 'mobile' ]
            ]
        ];
    }

}
new Blogin_Aarambha_Customize_Footer_Bottom_Row_Fields();
