<?php
/**
 * Blogin Aarambha Theme Customizer Selective Refresh
 *
 * @package Blogin_Aarambha
 */


if ( ! defined( 'ABSPATH' ) ) {
    exit; // Exit if accessed directly.
}

if ( isset( $wp_customize->selective_refresh ) ) {

    $wp_customize->selective_refresh->add_partial(
        'blogname',
        array(
            'selector'        => '.site-title a',
            'render_callback' => [ $this, 'blogin_aarambha_customize_partial_blogname' ],
        )
    );
    $wp_customize->selective_refresh->add_partial(
        'blogdescription',
        array(
            'selector'        => '.site-description',
            'render_callback' => [ $this, 'blogin_aarambha_customize_partial_blogdescription' ],
        )
    );

    // Header Builder
    $wp_customize->selective_refresh->add_partial(
        'blogin_aarambha_header',
        array(
            'selector'          => '.site-header',
            'settings'          => array(
                'blogin_aarambha_header_builder_controller_section'
            ),
            'render_callback'   => function() {
                Blogin_Aarambha_Customizer_Header_Builder()->blogin_aarambha_header_display();
            }
        )
    );

    // Footer Builder
    $wp_customize->selective_refresh->add_partial(
        'blogin_aarambha_footer',
        array(
            'selector'          => '.site-footer',
            'settings'          => array(
                'blogin_aarambha_footer_builder_controller_section'
            ),
            'render_callback'   => function() {
                Blogin_Aarambha_Customizer_Footer_Builder()->blogin_aarambha_footer_display();
            }
        )
    );
}
