<?php
/**
 * Customizer Control: blogin_aarambha_heading
 *
 * @package Blogin_Aarambha
 */

/**
 * Blogin_Aarambha_Customize_Heading_Control class
 */
class Blogin_Aarambha_Customize_Heading_Control extends Blogin_Aarambha_Customize_Base_Control {

    /**
     * The type of customize control being rendered.
     *
     * @access public
     * @var    string
     */
    public $type = 'blogin_aarambha_heading';

    /**
     * Underscore JS template to handle the control's output.
     *
     * @access public
     * @return void
     */
    public function content_template() { ?>

        <div class="control-wrap heading-control">

            <# if ( data.label ) { #>
            <span class="customize-control-title">{{{ data.label }}}</span>
            <# } #>

            <# if ( data.description ) { #>
            <span class="description customize-control-description">{{{ data.description }}}</span>
            <# } #>

        </div>

        <?php
    }
}

// Register JS-rendered control types.
$wp_customize->register_control_type( 'Blogin_Aarambha_Customize_Heading_Control' );