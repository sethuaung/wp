<?php
/**
 * Functions which enhance the theme by hooking into WordPress
 *
 * @package Blogin_Aarambha
 */

/**
 * Adds custom classes to the array of body classes.
 *
 * @param array $classes Classes for the body element.
 * @return array
 */
function blogin_aarambha_body_classes( $classes ) {

    // Adds a class of hfeed to non-singular pages.
    if ( ! is_singular() ) {
        $classes[] = 'hfeed';
    }

    // Adds a class of no-sidebar when there is no sidebar present.
    if ( ! is_active_sidebar( 'sidebar-1' ) || ! Blogin_Aarambha_Helper::get_sidebar_layout() ) {
        $classes[] = 'no-sidebar';
    }

    if ( is_front_page() && is_home() || is_home() || is_search() || is_archive() ) {
        $classes[] = 'blogin-aarambha-blog';
    }

    // Is Sticky Sidebar
    $is_sticky_sidebar = get_theme_mod(
        'blogin_aarambha_sidebar_sticky',
        ''
    );
    if ( $is_sticky_sidebar ) {
        $classes[]  = 'has-sticky-sidebar';
    }

    // Placeholder or Thumbnail
    if ( is_singular() ) {
        if ( has_post_thumbnail() ) {
            $classes[]  = 'has-thumbnail';
        }
    }
    else {
        $classes[]  = 'has-blog-thumbnail';
    }

    return array_unique($classes);
}
add_filter( 'body_class', 'blogin_aarambha_body_classes' );

/**
 * Adds custom classes to the array of post classes.
 *
 * @param array $classes Classes for the post element.
 * @return array
 */
function blogin_aarambha_post_classes( $classes ) {

    global $post;

    // Featured Image Condition
    $classes[] = 'post';
    if ( has_post_thumbnail() || ( is_archive() || is_front_page() && is_home() ) || is_search() ) {
        $classes[] = 'has-featured-image';
    }
    else {
        $classes[] = 'no-featured-image';
    }

    if ( ! is_page() ) {
        //check if some meta field is set 
        $content_layout = get_post_meta( $post->ID, 'blogin_aarambha_content_layout', true );
        $classes[]      = ( $content_layout ) ? 'has-'. esc_attr($content_layout) . '-layout' : 'has-portrait-layout';
    }

    return array_unique($classes);
}
add_filter( 'post_class', 'blogin_aarambha_post_classes' );

/**
 * Add a pingback url auto-discovery header for single posts, pages, or attachments.
 */
function blogin_aarambha_pingback_header() {
    if ( is_singular() && pings_open() ) {
        printf( '<link rel="pingback" href="%s">', esc_url( get_bloginfo( 'pingback_url' ) ) );
    }
}
add_action( 'wp_head', 'blogin_aarambha_pingback_header' );

/**
 * Return an array of all icons.
 */
function blogin_aarambha_get_fontawesome() {
        // Bail if the nonce doesn't check out
        if ( ! isset( $_POST['blogin_aarambha_customize_nonce'] ) || ! wp_verify_nonce( sanitize_key( $_POST['blogin_aarambha_customize_nonce'] ), 'blogin_aarambha_customize_nonce' ) ) {
            wp_die();
        }

        // Do another nonce check
        check_ajax_referer( 'blogin_aarambha_customize_nonce', 'blogin_aarambha_customize_nonce' );

        // Bail if user can't edit theme options
        if ( ! current_user_can( 'edit_theme_options' ) ) {
            wp_die();
        }

        // Get all of our fonts
        $fonts = Blogin_Aarambha_Font_Awesome_Icons::$icons;

        ob_start();
        if( $fonts ){ ?>
            <ul class="font-group">
                <?php
                foreach( $fonts as $font ){
                    echo '<li data-font="' . esc_attr( $font ) . '"><i class="' . esc_attr( $font ) . '"></i></li>';
                }
                ?>
            </ul>
            <?php
        }
        $output = ob_get_clean();
        echo apply_filters( 'blogin_aarambha_get_fontawesome', $output ); // phpcs:ignore WordPress.Security.EscapeOutput.OutputNotEscaped

        // Exit
        wp_die();
    }
add_action( 'wp_ajax_blogin_aarambha_get_fontawesome', 'blogin_aarambha_get_fontawesome' );

if ( ! function_exists( 'blogin_aarambha_get_template_part' ) ) {

	/**
	 * blogin_aarambha_get_template_part
	 *
	 * @param      $id
	 * @param      $slug
	 * @param null $name
	 */
	function blogin_aarambha_get_template_part( $id, $slug, $name = null ) {

		$templates = array();
		$name      = (string) $name;
		if ( '' !== $name ) {
			$templates[] = "{$slug}-{$name}.php";
		}

		$templates[] = "{$slug}.php";
		$template    = locate_template( $templates );

		// Allow 3rd party plugins to filter template file from their plugin.
		$template = apply_filters( 'blogin_aarambha_get_template_part', $template, $id, $slug, $name );
		if ( $template ) {
			load_template( $template, false );
		}
	}
}