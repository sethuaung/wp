<?php
/**
 * Blogin Aarambha hooks
 *
 * @package Blogin_Aarambha
 */

/* ------------------------------ HEADER ------------------------------ */
/**
 * Meta head.
 *
 * @see blogin_aarambha_head_meta()
 */
add_action( 'blogin_aarambha_head', 'blogin_aarambha_head_meta', 10 );


/**
 * Header Bottom Content
 *
 * @see blogin_aarambha_header_page_top_content()
 */
add_action( 'blogin_aarambha_header_bottom', 'blogin_aarambha_header_page_top_content', 10 );



/* ------------------------------ BEFORE CONTENT ------------------------------ */
/**
 * Before Content of page
 *
 * @see blogin_aarambha_site_content_start()
 * @see blogin_aarambha_page_section_start()
 * @see blogin_aarambha_page_container_start()
 */
add_action( 'blogin_aarambha_content_before', 'blogin_aarambha_site_content_start', 5 );
add_action( 'blogin_aarambha_content_before', 'blogin_aarambha_page_section_start', 10 );
add_action( 'blogin_aarambha_content_before', 'blogin_aarambha_page_container_start', 15 );

/* ------------------------------ AFTER CONTENT ------------------------------ */
/**
 * After Content of page
 *
 * @see blogin_aarambha_page_container_end()
 * @see blogin_aarambha_page_section_end()
 * @see blogin_aarambha_site_content_end()
 */
add_action( 'blogin_aarambha_content_after', 'blogin_aarambha_page_container_end', 5 );
add_action( 'blogin_aarambha_content_after', 'blogin_aarambha_page_section_end', 10 );
add_action( 'blogin_aarambha_content_after', 'blogin_aarambha_site_content_end', 15 );

/* ------------------------------ BLOG/ARCHIVE PAGE ------------------------------ */
/**
 * Posts Content
 *
 * @see blogin_aarambha_blog_header_wrapper()
 * @see blogin_aarambha_blog_entry_content()
 */
add_action( 'blogin_aarambha_posts_content', 'blogin_aarambha_blog_posts_wrapper', 10 );
add_action( 'blogin_aarambha_posts_content_top', 'blogin_aarambha_blog_entry_header', 10 );


/**
 * After content loop
 *
 * @see blogin_aarambha_posts_navigation()
 */
add_action( 'blogin_aarambha_content_loop_after', 'blogin_aarambha_posts_navigation', 10 );

/* ------------------------------ SEARCH PAGE ------------------------------ */

/**
 * Entry Header
 *
 * @see blogin_aarambha_search_posts_header()
 */
add_action( 'blogin_aarambha_search_posts_entry_header', 'blogin_aarambha_search_posts_header', 10 );

/**
 * Entry Content
 *
 * @see blogin_aarambha_search_posts_content()
 */
add_action( 'blogin_aarambha_search_posts_entry_content', 'blogin_aarambha_search_posts_content', 10 );

/**
 * Entry Footer
 *
 * @see blogin_aarambha_search_posts_footer()
 */
add_action( 'blogin_aarambha_search_posts_entry_footer', 'blogin_aarambha_search_posts_footer', 10 );

/* ------------------------------ SINGLE POST ------------------------------ */
/**
 * Single Post Content
 *
 * @see blogin_aarambha_post_header_wrapper()
 * @see blogin_aarambha_post_entry_content()
 */
add_action( 'blogin_aarambha_post_content', 'blogin_aarambha_post_header_wrapper', 10 );
add_action( 'blogin_aarambha_post_content', 'blogin_aarambha_post_entry_content', 15 );

/**
 * Single Post Content Bottom
 *
 * @see blogin_aarambha_edit_post_link()
 */
add_action( 'blogin_aarambha_post_content_bottom', 'blogin_aarambha_edit_post_link', 10 );

/**
 * Single Post Content After
 *
 * @see blogin_aarambha_post_after_content()
 */
add_action( 'blogin_aarambha_post_content_after', 'blogin_aarambha_post_after_content', 10 );

/* ------------------------------ SINGLE PAGE ------------------------------ */

/**
 * Single Page Content
 *
 * @see blogin_aarambha_page_header_wrapper()
 * @see blogin_aarambha_page_entry_content()
 */
add_action( 'blogin_aarambha_page_content', 'blogin_aarambha_page_header_wrapper', 10 );
add_action( 'blogin_aarambha_page_content', 'blogin_aarambha_page_entry_content', 15 );


/**
 * Single Page Content Bottom
 *
 * @see blogin_aarambha_edit_post_link()
 */
add_action( 'blogin_aarambha_page_content_bottom', 'blogin_aarambha_edit_post_link', 10 );

/**
 * Single Page Content After
 *
 * @see blogin_aarambha_page_after_content()
 */
add_action( 'blogin_aarambha_page_content_after', 'blogin_aarambha_page_after_content', 10 );

/* ------------------------------ COMMENTS ------------------------------ */
/**
 * Comments
 *
 * @see blogin_aarambha_comment_list()
 * @see blogin_aarambha_comment_form()
 */
add_action( 'blogin_aarambha_comments', 'blogin_aarambha_comment_list', 10 );
add_action( 'blogin_aarambha_comments', 'blogin_aarambha_comment_form', 15 );

/* ------------------------------ FOOTER ------------------------------ */
/**
 * Footer back to top
 *
 * @see blogin_aarambha_footer_back_to_top()
 */
add_action( 'blogin_aarambha_footer_after', 'blogin_aarambha_footer_back_to_top', 10 );
