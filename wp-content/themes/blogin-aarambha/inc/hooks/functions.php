<?php
/**
 * Blogin Aarambha functions to be hooked
 *
 * @package Blogin_Aarambha
 */


/* ------------------------------ HEADER ------------------------------ */

if ( ! function_exists( 'blogin_aarambha_head_meta' ) ) :
    /**
     * Meta head
     */
    function blogin_aarambha_head_meta() {
        ?>
        <meta charset="<?php bloginfo( 'charset' ); ?>">
        <meta name="viewport" content="width=device-width, initial-scale=1">
        <link rel="profile" href="https://gmpg.org/xfn/11">
        <?php
    }
endif;

if ( ! function_exists( 'blogin_aarambha_header_page_top_content' ) ) :
    /**
     * Featured page content page header
     */
    function blogin_aarambha_header_page_top_content() {

        if ( is_front_page() && is_home() ) {
            return;
        }
        // Check meta first to override and return (prevents filters from overriding meta)
        $page_header_enable = get_post_meta( Blogin_Aarambha_Helper::get_post_id(), 'blogin_aarambha_page_header_enable', true );
        if ( $page_header_enable && $page_header_enable != 'default' ) {
            return;
        }
        // Blog
        $elements = get_theme_mod(
            'blogin_aarambha_blog_banner_elements',
            ['post-title']
        );
        if ( is_single() ) {
        	$elements = get_theme_mod(
                'blogin_aarambha_post_banner_elements',
                ['breadcrumb']
            );
        }

        // Is Single Page
        elseif ( is_page() ) {
            $elements = get_theme_mod(
                'blogin_aarambha_page_header_wrapper_elements',
                ['breadcrumb']
            );
        }
        // Is 404 Page
        elseif ( is_404() ) {
            $elements = get_theme_mod(
                'blogin_aarambha_error_banner_elements',
                ['breadcrumb']
            );
        }
        if ( ! empty( $elements ) || !empty( $bottom_elements ) ) : ?>

            <div class="page-title-wrap">

                <?php if ( !empty($elements) ) : ?>
                    <div class="container d-flex flex-column align-items-left text-left">
                        <?php
                        foreach ( $elements as $element ) :

                            switch ( $element ) :

                                case 'post-title' :
                                    Blogin_Aarambha_Helper::archive_title( '<h2 class="page-title">', '</h2>');
                                    break;

                                case 'breadcrumb' :
                                    Blogin_Aarambha_Breadcrumb::get_breadcrumb();
                                    break;

                                case 'excerpt' :
                                    echo '<div class="entry-content">';
                                    Blogin_Aarambha_Helper::post_excerpt();
                                    echo '</div><!-- .entry-content -->';
                                    break;

                                case 'post-meta' :
                                    echo '<div class="post-meta-wrapper header-post-meta">';
                                    Blogin_Aarambha_Helper::post_meta( get_the_ID() );
                                    echo '</div><!-- .header-post-meta -->';
                                    break;

                                case 'post-desc' :
                                    if ( ! is_404() ) {
                                        the_archive_description( '<div class="archive-description">', '</div>' );
                                    }
                                    break;

                            endswitch;

                        endforeach;
                        ?>
                    </div><!-- .page-top-main-content -->
                <?php endif; ?>

            </div>

        <?php endif;
    }
endif;

/* ------------------------------ BEFORE CONTENT ------------------------------ */

if ( ! function_exists( 'blogin_aarambha_site_content_start' ) ) :
    /**
     * Add custom wrapper div of site content wrapper
     */
    function blogin_aarambha_site_content_start() {
        ?>
        <div id="content" <?php Blogin_Aarambha_Helper::site_content_class(); ?>>
        <?php
    }
endif;

if ( ! function_exists( 'blogin_aarambha_page_section_start' ) ) :
    /**
     * Add custom wrapper for page section
     */
    function blogin_aarambha_page_section_start() {
        if ( is_404() ) {
            return;
        }
        $section_class = is_single() ? 'single-page-wrapper single-post-wrapper' : 'single-page-wrapper';
        ?>
        <section class="<?php echo esc_attr( $section_class ); ?>">
        <?php
    }
endif;

if ( ! function_exists( 'blogin_aarambha_page_container_start' ) ) :
    /**
     * Add custom wrapper for page container start
     */
    function blogin_aarambha_page_container_start() {
        if ( is_404() ) {
            return;
        }
        ?>
        <div class="container d-flex flex-wrap">
        <?php
    }
endif;

/* ------------------------------ AFTER CONTENT ------------------------------ */

if ( ! function_exists( 'blogin_aarambha_page_container_end' ) ) :
    /**
     * Close custom wrapper div after content
     */
    function blogin_aarambha_page_container_end() {
        if ( is_404() ) {
            return;
        }
        get_sidebar();
        echo '</div><! -- .container -->';
    }
endif;

if ( ! function_exists( 'blogin_aarambha_page_section_end' ) ) :
/**
 * Close custom wrapper div after content
 */
    function blogin_aarambha_page_section_end() {
        if ( is_404() ) {
            return;
        }
        echo '</section><! -- .single-page-wrapper -->';
    }
endif;

if ( ! function_exists( 'blogin_aarambha_site_content_end' ) ) :
    /**
     * Close custom wrapper div after content
     */
    function blogin_aarambha_site_content_end() {
        echo '</div><! -- #content -->';
    }
endif;

/* ------------------------------ BLOG CONTENT ------------------------------ */

if ( ! function_exists( 'blogin_aarambha_blog_posts_wrapper' ) ) :
    /**
     * Blog Posts Wrapper
     */
    function blogin_aarambha_blog_posts_wrapper() {
        global $post;
        $content_layout = get_post_meta( $post->ID, 'blogin_aarambha_content_layout', true );
        $classes        = ['post-detail-wrap d-flex'];
        if( $content_layout && $content_layout === 'landscape' ) {
            $classes[]  = 'flex-column';
        } else {
            $classes[]  = 'align-items-center flex-row'; 
        }
        ob_start(); ?>

        <div class="<?php echo esc_attr( implode( ' ', $classes ) ); ?>">
            <?php blogin_aarambha_get_post_thumbnail(); ?>
            <?php blogin_aarambha_blog_entry_content(); ?>
        </div><!-- .post-detail-wrap -->

        <?php
        $output = ob_get_clean();
        echo apply_filters( 'blogin_aarambha_blog_posts_wrapper', $output ); // phpcs:ignore WordPress.Security.EscapeOutput.OutputNotEscaped
    }
endif;


if ( ! function_exists( 'blogin_aarambha_blog_entry_header' ) ) :
    /**
     * Blog Posts entry header
     */
    function blogin_aarambha_blog_entry_header() {

        $posts_elements = get_theme_mod(
            'blogin_aarambha_blog_header_elements',
            ['post-title','post-meta']
        );
        if ( ! empty( $posts_elements ) ) :

            echo '<div class="entry-header">';

            foreach ( $posts_elements as $post_element ) :

                switch ( $post_element ) :

                    case 'post-title' :
                        Blogin_Aarambha_Helper::post_title();
                        break;

                    case 'post-meta' :
                        Blogin_Aarambha_Helper::post_meta( get_the_ID() );
                        break;

                    case 'categories' :
                        Blogin_Aarambha_Helper::post_meta( get_the_ID(), ['categories'] );
                        break;

                endswitch;

            endforeach;

            echo '</div><!-- .entry-header -->';

        endif;
    }
endif;

if ( ! function_exists( 'blogin_aarambha_blog_entry_content' ) ) :
    /**
     * Entry Blog Post Content
     */
    function blogin_aarambha_blog_entry_content() {

        $content_elements = get_theme_mod('blogin_aarambha_blog_content_elements',['post-excerpt','read-more']);

        ?>
        <div class="entry-content-wrap">

            <?php
            /**
             * Functions hooked in to blogin_aarambha_posts_content_top action.
             *
             */
            do_action( 'blogin_aarambha_posts_content_top' );
            ?>

            <?php
            if ( ! empty( $content_elements ) ) :

                foreach ( $content_elements as $content_element ) :
    
                    switch ( $content_element ) :

                        case 'post-excerpt' :
                            Blogin_Aarambha_Helper::post_content();
                            break;
    
                        case 'read-more' :
                            Blogin_Aarambha_Helper::read_more();
                            break;

                        case 'cats-tags' :

                            echo '<footer class="entry-footer">';

                            if ( 'post' === get_post_type() ) {
                                $categories_list    = get_the_category_list( ' ' );
                                $tags_list 			= get_the_tag_list( '', ' ' );

                                echo '<div class="d-flex flex-wrap">';
                                if ( $categories_list ) {
                                    printf( '<span class="cat-links">' . esc_html__( 'Posted in %1$s', 'blogin-aarambha' ) . '</span>', $categories_list ); // WPCS: XSS OK.
                                }
                                if ( $tags_list ) {
                                    printf( '<span class="tags-links"><span>&nbsp;' . esc_html__( 'and', 'blogin-aarambha' ) . '&nbsp;</span><span>' . esc_html__( 'Tagged %1$s', 'blogin-aarambha' ) . '</span></span>', $tags_list ); // WPCS: XSS OK.
                                }
                                echo '</div><!-- .d-flex -->';
                            }

                            echo '</footer>';
                            
                            break;
    
                    endswitch;
    
                endforeach;

            endif;
            ?>

            <?php
            /**
             * Functions hooked in to blogin_aarambha_posts_content_bottom action.
             *
             */

            do_action( 'blogin_aarambha_posts_content_bottom' );
            ?>

        </div><!-- .entry-content-wrap -->

        <?php
    }
endif;

if ( ! function_exists( 'blogin_aarambha_posts_navigation' ) ) :
    /**
     * Blog Posts navigation
     */
    function blogin_aarambha_posts_navigation() {

        Blogin_Aarambha_Helper::post_pagination();
    }
endif;

/* ------------------------------ POST CONTENT ------------------------------ */

if ( ! function_exists( 'blogin_aarambha_post_header_wrapper' ) ) :
    /**
     * Post Header Wrapper
     */
    function blogin_aarambha_post_header_wrapper() {
        global $post;
        $content_layout = get_post_meta( $post->ID, 'blogin_aarambha_content_layout', true );
        $classes        = ['entry-header-wrap d-flex'];
        if( $content_layout && $content_layout === 'landscape' ) {
            $classes[]  = 'flex-column';
        } else {
            $classes[]  = 'flex-row align-items-center'; 
        }
        echo '<div class="'. esc_attr( implode( ' ', $classes ) ) .'">';
        blogin_aarambha_get_post_thumbnail();
        blogin_aarambha_post_entry_header();
        echo '</div><!-- .entry-header-wrap -->';
    }
endif;

if ( ! function_exists( 'blogin_aarambha_get_post_thumbnail' ) ) :
    /**
     * Post Thumbnail
     */
    function blogin_aarambha_get_post_thumbnail() {
        global $post;
        $content_layout = get_post_meta( $post->ID, 'blogin_aarambha_content_layout', true );
        $img_ratio      = ( Blogin_Aarambha_Helper::get_sidebar_layout() != 'none' ) ? '4x3' : '21x11';
        if( $content_layout && $content_layout === 'landscape' || is_page() ) {
            $img_ratio  = '16x9';
        }
        // Is Singular
        if ( is_singular() ) {
            $img_size = is_single() ? get_theme_mod('blogin_aarambha_post_image_size',['desktop' => 'medium_large'] ) : get_theme_mod( 'blogin_aarambha_page_image_size', ['desktop' => 'medium_large'] );
            blogin_aarambha_singular_post_thumbnail( $img_size['desktop'],$img_ratio );
        }
        else {
            $img_size = get_theme_mod( 'blogin_aarambha_blog_image_size', ['desktop' => 'medium_large'] );
            blogin_aarambha_post_thumbnail( $img_size['desktop'],$img_ratio );
        }
    }
endif;

if ( ! function_exists( 'blogin_aarambha_post_entry_header' ) ) :
    /**
     * Single post entry header
     */
    function blogin_aarambha_post_entry_header() {

        $posts_elements = get_theme_mod(
            'blogin_aarambha_post_header_elements',
            ['post-title','post-meta']
        );
        if ( ! empty( $posts_elements ) ) :

            echo '<div class="entry-header d-flex flex-column text-left">';

            foreach ( $posts_elements as $post_element ) :

                switch ( $post_element ) :

                    case 'post-title' :
                        Blogin_Aarambha_Helper::post_title();
                        break;

                    case 'post-meta' :
                        Blogin_Aarambha_Helper::post_meta( get_the_ID() );
                        break;

                    case 'categories' :
                        Blogin_Aarambha_Helper::post_meta( get_the_ID(), ['categories'] );
                        break;

                endswitch;

            endforeach;

            echo '</div><!-- .entry-header -->';

        endif;
    }
endif;

if ( ! function_exists( 'blogin_aarambha_post_entry_content' ) ) :
    /**
     * Entry Post Content
     */
    function blogin_aarambha_post_entry_content() {

        $content_elements = get_theme_mod('blogin_aarambha_post_content_elements',['post-content','cats-tags']);

        ?>
        <div class="entry-content-wrap">

            <?php
            /**
             * Functions hooked in to blogin_aarambha_post_content_top action.
             *
             */
            do_action( 'blogin_aarambha_post_content_top' );
            ?>

            <?php
            if ( ! empty( $content_elements ) ) :

                foreach ( $content_elements as $content_element ) :
    
                    switch ( $content_element ) :
    
                        case 'post-content' :

                            Blogin_Aarambha_Helper::post_content();
                           
                            break;
    
                        case 'cats-tags' :

                            echo '<footer class="entry-footer">';

                            if ( 'post' === get_post_type() ) {
                                $categories_list    = get_the_category_list( ' ' );
                                $tags_list 			= get_the_tag_list( '', ' ' );

                                echo '<div class="d-flex flex-wrap">';
                                if ( $categories_list ) {
                                    printf( '<span class="cat-links">' . esc_html__( 'Posted in %1$s', 'blogin-aarambha' ) . '</span>', $categories_list ); // WPCS: XSS OK.
                                }
                                if ( $tags_list ) {
                                    printf( '<span class="tags-links"><span>&nbsp;' . esc_html__( 'and', 'blogin-aarambha' ) . '&nbsp;</span><span>' . esc_html__( 'Tagged %1$s', 'blogin-aarambha' ) . '</span></span>', $tags_list ); // WPCS: XSS OK.
                                }
                                echo '</div><!-- .d-flex -->';
                            }

                            echo '</footer><!-- .entry-footer -->';
                            
                            break;
    
                    endswitch;
    
                endforeach;

            endif;
            ?>

            <?php
            /**
             * Functions hooked in to blogin_aarambha_post_content_bottom action.
             *
             */

            do_action( 'blogin_aarambha_post_content_bottom' );
            ?>

        </div><!-- .entry-content-wrap -->

        <?php
    }
endif;

if ( ! function_exists( 'blogin_aarambha_post_after_content' ) ) :
    /**
     * Post Footer for extra elements
     */
    function blogin_aarambha_post_after_content() {

        /**
         * Functions hooked in to blogin_aarambha_single_post_content_after_top action.
         *
         */
        do_action( 'blogin_aarambha_single_post_content_after_top' );
        ?>
        <?php
        $elements = get_theme_mod(
            'blogin_aarambha_post_after_content',
            ['post-comments','post-navigation']
        );

        if ( ! empty( $elements ) ) :

            foreach ( $elements as $element ) :

                switch ( $element ) :

                    case 'post-comments' :
                        Blogin_Aarambha_Helper::post_comment();
                        break;

                    case 'post-navigation' :
                        Blogin_Aarambha_Helper::post_navigation();
                        break;

                    case 'author-box' :
                        Blogin_Aarambha_Helper::author_box();
                        break;

                    case 'related-posts' :
                        Blogin_Aarambha_Helper::related_posts();
                        break;

                endswitch;

            endforeach;
        endif;

        ?>

        <?php
        /**
         * Functions hooked in to blogin_aarambha_single_post_content_after_bottom action.
         *
         */
        do_action( 'blogin_aarambha_single_post_content_after_bottom' );

    }
endif;

/* ------------------------------ PAGE CONTENT ------------------------------ */

if ( ! function_exists( 'blogin_aarambha_page_header_wrapper' ) ) :
    /**
     * Page Header Wrapper
     */
    function blogin_aarambha_page_header_wrapper() {
        $elements = get_theme_mod(
            'blogin_aarambha_page_header_elements',
            ['post-image','post-title']
        );

        if ( ! empty( $elements ) ) :

            echo '<div class="entry-header-wrap d-flex flex-column">';

            foreach ( $elements as $element ) :

                switch ( $element ) :

                    case 'post-image' :
                        blogin_aarambha_get_post_thumbnail();
                        break;

                    case 'post-title' :
                        blogin_aarambha_page_entry_header();
                        break;

                endswitch;

            endforeach;

            echo '</div><!-- .entry-header-wrap -->';
            
        endif;
    }
endif;

if ( ! function_exists( 'blogin_aarambha_page_entry_header' ) ) :
    /**
     * Single Page entry header
     */
    function blogin_aarambha_page_entry_header() {

        $posts_elements = get_theme_mod(
            'blogin_aarambha_page_entry_header_elements',
            ['post-title']
        );
        if ( ! empty( $posts_elements ) ) :

            echo '<div class="entry-header">';

            foreach ( $posts_elements as $post_element ) :

                switch ( $post_element ) :

                    case 'post-title' :
                        Blogin_Aarambha_Helper::post_title();
                        break;

                endswitch;

            endforeach;

            echo '</div><!-- .entry-header -->';

        endif;
    }
endif;

if ( ! function_exists( 'blogin_aarambha_page_entry_content' ) ) :
    /**
     * Entry Page Content
     */
    function blogin_aarambha_page_entry_content() {

        $content_elements = get_theme_mod('blogin_aarambha_page_content_elements',['page-content']);

        ?>
        <div class="entry-content-wrap">

            <?php
            /**
             * Functions hooked in to blogin_aarambha_page_content_top action.
             *
             */
            do_action( 'blogin_aarambha_page_content_top' );
            ?>

            <?php
            if ( ! empty( $content_elements ) ) :

                foreach ( $content_elements as $content_element ) :
    
                    switch ( $content_element ) :
    
                        case 'page-content' :

                            Blogin_Aarambha_Helper::post_content();
                           
                            break;
    
                    endswitch;
    
                endforeach;

            endif;
            ?>

            <?php
            /**
             * Functions hooked in to blogin_aarambha_page_content_bottom action.
             *
             */

            do_action( 'blogin_aarambha_page_content_bottom' );
            ?>

        </div><!-- .entry-content-wrap -->

        <?php
    }
endif;

if ( ! function_exists( 'blogin_aarambha_page_after_content' ) ) :
    /**
     * Page after content extra elements
     */
    function blogin_aarambha_page_after_content() {

        /**
         * Functions hooked in to blogin_aarambha_single_page_content_after_top action.
         *
         */
        do_action( 'blogin_aarambha_single_page_content_after_top' );
        ?>
        <?php
        $elements = get_theme_mod(
            'blogin_aarambha_page_after_content',
            ['post-comments']
        );

        if ( ! empty( $elements ) ) :

            foreach ( $elements as $element ) :

                switch ( $element ) :

                    case 'post-comments' :
                        Blogin_Aarambha_Helper::post_comment();
                        break;

                endswitch;

            endforeach;
        endif;

        ?>

        <?php
       /**
         * Functions hooked in to blogin_aarambha_single_page_content_after_bottom action.
         *
         */
        do_action( 'blogin_aarambha_single_page_content_after_bottom' );

    }
endif;

/* ------------------------------ COMMENTS ------------------------------ */

if ( ! function_exists( 'blogin_aarambha_comment_list' ) ) :
    /**
     * comment list
     */
    function blogin_aarambha_comment_list() {

        if ( have_comments() ) {

            /**
             * Functions hooked into blogin_aarambha_comments_list_before action
             *
             */
            do_action( 'blogin_aarambha_comments_list_before' );

            $title_output = '<h2 class="comments-title">';
            $comment_count = (int) get_comments_number();
            if ( 1 === $comment_count ) {
                $title_output .= esc_html__( 'One Comment', 'blogin-aarambha' );
            } else {
                $title_output .= sprintf(
                /* translators: 1: comment count number */
                esc_html( _nx( '%1$s Comment', '%1$s Comments', $comment_count, 'comments title', 'blogin-aarambha' ) ),
                // phpcs:ignore WordPress.Security.EscapeOutput.OutputNotEscaped
                number_format_i18n( $comment_count )
                );
            }
            $title_output .= '</h2><!-- .comments-title -->';
            echo wp_kses_post( apply_filters( 'blogin_aarambha_post_comments_title', $title_output ) );

            the_comments_navigation();
            echo '<ol class="comment-list">';
            wp_list_comments(
				array(
                    'style'       => 'ol',
                    'short_ping'  => true,
                    'avatar_size' => 60,
				)
			);
            echo '<ol><!-- .comment-list -->';
            the_comments_navigation();

            /**
             * Functions hooked into blogin_aarambha_comments_list_after action
             *
             */
            do_action( 'blogin_aarambha_comments_list_after' );
        }
    }
endif;

if ( ! function_exists( 'blogin_aarambha_comment_form' ) ) :
    /**
     * comment form
     */
    function blogin_aarambha_comment_form() {
        // You can start editing here -- including this comment!
        if ( have_comments() ) :

            // If comments are closed and there are comments, let's leave a little note, shall we?
            if ( ! comments_open() ) :
                ?>
                <p class="no-comments"><?php esc_html_e( 'Comments are closed.', 'blogin-aarambha' ); ?></p>
                <?php
            endif;

        endif; // Check for have_comments().

        comment_form();
    }
endif;

/* ------------------------------ FOOTER ------------------------------ */

if ( ! function_exists( 'blogin_aarambha_footer_back_to_top' ) ) :
    /**
     * Footer Back to Top
     */
    function blogin_aarambha_footer_back_to_top() {
        $back_to_top = get_theme_mod(
                'blogin_aarambha_footer_back_to_top_enable',
                ['desktop'=>'true']
        );
        if ( $back_to_top && array_key_exists( 'desktop', $back_to_top ) ) :
        ?>
        <div class="back-to-top">
            <button href="#masthead" title="<?php esc_attr_e('Go to Top','blogin-aarambha'); ?>"><i class="fa fa-angle-up" aria-hidden="true"></i></button>
        </div><!-- .back-to-top -->
        <?php
        endif;
    }
endif;

/* ------------------------------ Header Menu ------------------------------ */

if ( ! function_exists( 'blogin_aarambha_desktop_menu_fallback' ) ) :

	/**
	 * Menu fallback for primary menu.
	 *
	 * Contains wp_list_pages to display pages created,
	 */
	function blogin_aarambha_desktop_menu_fallback() {
		$output  = '';
		$output .= '<div class="menu-top-menu-container">';
		$output .= '<ul id="primary-menu-list" class="menu-wrapper d-flex flex-wrap">';

		$output .= wp_list_pages(
			array(
				'echo'     => false,
				'title_li' => false,
			)
		);

		$output .= '</ul>';
		$output .= '</div>';

		// @codingStandardsIgnoreStart
		echo $output;
		// @codingStandardsIgnoreEnd
	}

endif;

if ( ! function_exists( 'blogin_aarambha_mobile_menu_fallback' ) ) :

    /**
     * Menu fallback for mobile menu.
     *
     * Contains wp_list_pages to display pages created,
     */
    function blogin_aarambha_mobile_menu_fallback() {
        $output  = '';
        $output .= '<div class="menu-top-menu-container">';
        $output .= '<ul id="mobile-menu-list" class="menu-wrapper d-flex flex-wrap d-none">';

        $output .= wp_list_pages(
            array(
                'echo'     => false,
                'title_li' => false,
            )
        );

        $output .= '</ul>';
        $output .= '</div>';

        // @codingStandardsIgnoreStart
        echo $output;
        // @codingStandardsIgnoreEnd
    }

endif;
