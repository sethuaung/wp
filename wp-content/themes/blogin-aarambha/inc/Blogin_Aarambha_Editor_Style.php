<?php
/**
 * Blogin Aarambha Editor Style
 *
 * @package Blogin_Aarambha
 */

if ( ! defined( 'ABSPATH' ) ) {
    exit;
}

/**
 * class for editor style
 *
 * @access public
 */
class Blogin_Aarambha_Editor_Style {

    /**
     * Main Instance
     *
     * Insures that only one instance of Blogin_Aarambha_Editor_Style exists in memory at any one
     * time. Also prevents needing to define globals all over the place.
     *
     * @since    1.0.0
     * @access   public
     *
     * @return object
     */
    public static function instance() {

        // Store the instance locally to avoid private static replication
        static $instance = null;

        // Only run these methods if they haven't been ran previously
        if ( null === $instance ) {
            $instance = new Blogin_Aarambha_Editor_Style;
        }

        // Always return the instance
        return $instance;
    }

    /**
     *  Run functionality with hooks
     *
     * @since    1.0.0
     * @access   public
     *
     * @return void
     */
    public function run() {

        add_action( 'enqueue_block_editor_assets', array( $this, 'enqueue_gutenberg_assets' ) );
    }

    /**
     *  Call back function for enqueue_gutenberg_assets
     *  Change Post/Page builder layout
     *
     * @since    1.0.0
     * @access   public
     *
     * @return void
     */
    public function enqueue_gutenberg_assets() {

        if ( blogin_aarambha_google_fonts()->google_font_url() ) {
            wp_enqueue_style( 'blogin-aarambha-google-fonts', blogin_aarambha_google_fonts()->google_font_url(), array(), BLOGIN_AARAMBHA_VERSION );
        }

        wp_enqueue_style( 'blogin-aarambha-editor-styles', BLOGIN_AARAMBHA_URI . 'assets/css/editor' . BLOGIN_AARAMBHA_SCRIPT_PREFIX . '.css', null, BLOGIN_AARAMBHA_VERSION, 'all' );

        // Editor inline style
        wp_add_inline_style( 'blogin-aarambha-editor-styles', Blogin_Aarambha_Customizer_Inline_Style::css_output( 'editor' ) );
    }
}

/**
 * Create Instance for blogin_aarambha_editor_style
 *
 * @since    1.0.0
 * @access   public
 *
 * @param
 *
 * @return object
 */
if ( ! function_exists( 'blogin_aarambha_editor_style' ) ) {

    function blogin_aarambha_editor_style() {

        return Blogin_Aarambha_Editor_Style::instance();
    }

    blogin_aarambha_editor_style()->run();
}
