<?php
/**
 * Plugin recommendation
 *
 * @package Blogin_Aarambha
 */

// Load TGM library.
require BLOGIN_AARAMBHA_DIR . 'inc/tgm/class-tgm-plugin-activation.php';

if ( ! function_exists( 'blogin_aarambha_recommended_plugins' ) ) :

	/**
	 * Register recommended plugins.
	 *
	 * @since 1.0.0
	 */
	function blogin_aarambha_recommended_plugins() {
		$plugins = array(
			array(
				'name'     => esc_html__( 'Aarambha Demo Sites', 'blogin-aarambha' ),
				'slug'     => 'aarambha-demo-sites',
				'required' => false,
			),
			array(
				'name'     => esc_html__( 'Elementor Page Builder', 'blogin-aarambha' ),
				'slug'     => 'elementor',
				'required' => false,
			),
			array(
				'name'     => esc_html__( 'Aarambha Kits for Elementor', 'blogin-aarambha' ),
				'slug'     => 'aarambha-kits-for-elementor',
				'required' => false,
			),
		);

		$config = array();

		tgmpa( $plugins, $config );
	}

endif;

add_action( 'tgmpa_register', 'blogin_aarambha_recommended_plugins' );
