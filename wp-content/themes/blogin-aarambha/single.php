<?php
/**
 * The template for displaying all single posts
 *
 * @link https://developer.wordpress.org/themes/basics/template-hierarchy/#single-post
 *
 * @package Blogin_Aarambha
 */

get_header();
?>

<div id="primary" <?php Blogin_Aarambha_Helper::primary_class();?>>
    <main id="main" class="site-main">

        <?php
        /**
         * Functions hooked into blogin_aarambha_post_content_loop_before action
         *
         */
        do_action('blogin_aarambha_post_content_loop_before');

        while ( have_posts() ) : the_post();

            /**
             * Functions hooked into blogin_aarambha_post_content_before action
             *
             */
            do_action('blogin_aarambha_post_content_before');

            get_template_part( 'template-parts/content', 'single' );

            /**
             * Functions hooked into blogin_aarambha_post_content_after action
             *
             * @hooked blogin_aarambha_post_after_content - 10
             */
            do_action('blogin_aarambha_post_content_after');

        endwhile; // End of the loop.

        /**
         * Functions hooked into blogin_aarambha_post_content_loop_after action
         *
         */
        do_action('blogin_aarambha_post_content_loop_after');

        ?>

    </main><!-- #main -->
</div><!-- #primary -->

<?php get_footer(); ?>