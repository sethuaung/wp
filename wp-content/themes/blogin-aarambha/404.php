<?php
/**
 * The template for displaying 404 pages (not found)
 *
 * @link https://codex.wordpress.org/Creating_an_Error_404_Page
 *
 * @package Blogin_Aarambha
 */

get_header();
?>

<section class="error-404 not-found">
    <?php
    $content_elements = get_theme_mod(
        'blogin_aarambha_error_content_elements',
        ['title','subtitle','button']
    );
    if ( ! empty( $content_elements ) ) :

        $error_image = get_theme_mod(
            'blogin_aarambha_error_content_image',
            ''
        );
        ?>
        <div class="error-page-content">

            <?php foreach ($content_elements as $key => $value) {
                // image
                if ($value === 'image' && $error_image !== '' ) {
                    ?>
                    <figure>
                        <img src="<?php echo esc_url( $error_image ); ?>" alt="<?php esc_attr_e( '404 Error Image', 'blogin-aarambha' ); ?>">
                    </figure>
                    <?php
                }
                // title
                if ($value === 'title') {
                    ?>
                    <h2><?php esc_html_e( '404', 'blogin-aarambha' ); ?></h2>
                    <?php
                }
                // subtitle
                if ($value === 'subtitle') {
                    ?>
                    <h4><?php esc_html_e( 'It looks like nothing was found at this location. Maybe try one of the links below or a search?', 'blogin-aarambha'); ?></h4>
                    <?php
                }
                // button
                if ($value === 'button') {
                    ?>
                    <div class="d-flex justify-content-center read-more-wrap">
                        <a href="<?php echo esc_url( home_url( '/' ) ); ?>" class="home-button read-more-button d-flex align-items-center" rel="home">
                            <?php esc_html_e( 'Return Home', 'blogin-aarambha' ); ?>
                        </a>
                    </div><!-- read-more-wrap -->
                    <?php
                }
                // search
                if ($value === 'search') {
                    get_search_form();
                }
            } ?>
            
        </div><!-- .error-page-content -->
    <?php
    endif;
    ?>
</section><!-- .error-404 -->

<?php get_footer(); ?>
