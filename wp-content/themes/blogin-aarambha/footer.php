<?php
/**
 * The template for displaying the footer
 *
 * Contains the closing of the #content div and all content after.
 *
 * @link https://developer.wordpress.org/themes/basics/template-files/#template-partials
 *
 * @package Blogin_Aarambha
 */

?>

<?php
/**
 * Functions hooked into blogin_aarambha_content_after action
 *
 * @hooked blogin_aarambha_page_container_end     - 5
 * @hooked blogin_aarambha_page_section_end       - 10
 * @hooked blogin_aarambha_site_content_end       - 15
 */
do_action( 'blogin_aarambha_content_after' );
?>


<?php
/**
 * Functions hooked into blogin_aarambha_footer_before action
 *
 */
do_action( 'blogin_aarambha_footer_before' );
?>

<footer id="colophon" class="site-footer">

    <?php
    /**
     * Functions hooked into blogin_aarambha_footer_before action
     *
     */
    do_action( 'blogin_aarambha_footer_top' );
    ?>

    <?php
    /**
     * Functions hooked into blogin_aarambha_footer action
     *
     * @hooked blogin_aarambha_footer_site_info - 10
     */
    do_action( 'blogin_aarambha_footer' );
    ?>

    <?php
    /**
     * Functions hooked into blogin_aarambha_footer_before action
     *
     */
    do_action( 'blogin_aarambha_footer_bottom' );
    ?>

</footer><!-- #colophon -->

<?php
/**
 * Functions hooked into blogin_aarambha_footer_after action
 *
 * @hooked blogin_aarambha_footer_back_to_top - 10
 */
do_action( 'blogin_aarambha_footer_after' );
?>

</div><!-- #page -->


<?php
/**
 * Functions hooked into blogin_aarambha_body_bottom action
 *
 */
do_action( 'blogin_aarambha_body_bottom' );
?>

<?php wp_footer(); ?>

</body>
</html>
