<?php
/**
 * The header for our theme
 *
 * This is the template that displays all of the <head> section and everything up until <div id="content">
 *
 * @link https://developer.wordpress.org/themes/basics/template-files/#template-partials
 *
 * @package Blogin_Aarambha
 */

?>

<!doctype html>

<?php
/**
 * Functions hooked into blogin_aarambha_html_before action
 */
do_action( 'blogin_aarambha_html_before' );
?>

<html <?php language_attributes(); ?>>

<head>

	<?php
	/**
	 * Functions hooked into blogin_aarambha_head_top action
	 */
	do_action( 'blogin_aarambha_head_top' );
	?>

	<?php
	/**
	 * Functions hooked into blogin_aarambha_head action
	 *
	 * @hooked blogin_aarambha_head_meta - 10
	 */
	do_action( 'blogin_aarambha_head' );
	?>

	<?php
	/**
	 * Functions hooked into blogin_aarambha_head_bottom action
	 */
	do_action( 'blogin_aarambha_head_bottom' );
	?>

	<?php wp_head(); ?>

</head>

<body <?php body_class(); ?>>

<?php
/**
 * Functions hooked into blogin_aarambha_body_top action
 */
do_action( 'blogin_aarambha_body_top' );
?>

<?php wp_body_open(); ?>

<div id="page" <?php Blogin_Aarambha_Helper::site_class(); ?>>

	<a class="skip-link screen-reader-text" href="#primary"><?php esc_html_e( 'Skip to content', 'blogin-aarambha' ); ?></a>

	<?php
	/**
	 * Functions hooked into blogin_aarambha_header_before action
	 */
	do_action( 'blogin_aarambha_header_before' );
	?>

	<header id="masthead" class="site-header">

		<?php
		/**
		 * Functions hooked into blogin_aarambha_header_top action
		 */
		do_action( 'blogin_aarambha_header_top' );
		?>

		<?php
		/**
		 * Functions hooked into blogin_aarambha_header action
		 *
		 * @hooked blogin_aarambha_header_main - 10
		 */
		do_action( 'blogin_aarambha_header' );

		?>

		<?php
		/**
		 * Functions hooked into blogin_aarambha_header_bottom action
		 *
		 * @hooked blogin_aarambha_header_page_top_content - 10
		 */
		do_action( 'blogin_aarambha_header_bottom' );

		?>

	</header><!-- #masthead -->

	<?php
	/**
	 * Functions hooked into blogin_aarambha_header_after action
	 */
	do_action( 'blogin_aarambha_header_after' );
	?>

	<?php
	/**
	 * Functions hooked into blogin_aarambha_content_before action
	 *
	 * @hooked blogin_aarambha_site_content_start     - 5
	 * @hooked blogin_aarambha_page_section_start     - 10
	 * @hooked blogin_aarambha_page_container_start   - 15
	 */
	do_action( 'blogin_aarambha_content_before' );
	?>
