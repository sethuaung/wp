<?php
/**
 * The template for displaying comments
 *
 * This is the template that displays the area of the page that contains both the current comments
 * and the comment form.
 *
 * @link https://developer.wordpress.org/themes/basics/template-hierarchy/
 *
 * @package Blogin_Aarambha
 */

/*
 * If the current post is protected by a password and
 * the visitor has not yet entered the password we will
 * return early without loading the comments.
 */
if ( post_password_required() ) {
	return;
}
?>

<div id="comments" class="comments-area">

    <?php
    /**
     * Functions hooked into blogin_aarambha_comments_before action
     *
     */
    do_action( 'blogin_aarambha_comments_before' );
    ?>

    <?php
    /**
     * Functions hooked into blogin_aarambha_comments action
     *
     * @hooked blogin_aarambha_comment_list - 10
     * @hooked blogin_aarambha_comment_form - 15
     */
    do_action( 'blogin_aarambha_comments' );
    ?>

    <?php
    /**
     * Functions hooked into blogin_aarambha_comments_after action
     *
     */
    do_action( 'blogin_aarambha_comments_after' );
    ?>

</div><!-- #comments -->
