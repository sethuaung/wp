<?php
/**
 *
 * Template Name: Page Builder
 *
 * The template for displaying content from page builder.
 *
 * This is the template that displays all pages by default.
 * Please note that this is the WordPress construct of pages
 * and that other 'pages' on your WordPress site may use a
 * different template.
 *
 * @link    https://developer.wordpress.org/themes/basics/template-hierarchy/
 *
 * @package Blogin_Aarambha
 */
get_header();
?>

<div id="primary" <?php Blogin_Aarambha_Helper::primary_class();?>>
	<main id="main" class="site-main">
		<?php
		while ( have_posts() ) : the_post();
			the_content();
		endwhile; // End of the loop.
		?>
	</main>
</div><!-- #primary -->

<?php get_footer(); ?>
