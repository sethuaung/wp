=== Blogin Aarambha ===

Contributors: aarambhathemes
Tags: translation-ready, blog, one-column, two-columns, right-sidebar, left-sidebar, grid-layout, custom-colors, custom-logo, custom-menu, editor-style, featured-images, footer-widgets, theme-options, threaded-comments, e-commerce, news
Requires at least: 5.6
Requires PHP: 7.0
Tested up to: 6.1
Stable tag: 1.0.0
License: GPLv3 or later
License URI: https://www.gnu.org/licenses/gpl-3.0.en.html

Blogin Aarambha is a multipurpose, and Elementor compatible business theme having responsive designs with lots of powerful features. It facilitates you to create mobile friendly sites so the website will perform all functionalities flawlessly also in the mobile devices. Another interestingness about this theme is, you can try out any alluring demo and get them into your site just in clicks.

== Description ==

Blogin Aarambha consists of the most essential features that your eyes will search to create professional corporate sites. We have come up with the complete documentations as well as video tutorials. So, you don’t need to worry about implementing theme features on your site development. Moreover, developers are ready whenever you want to provide the necessary support for implementation, bug solving etc.

== Frequently Asked Questions ==

= Does this theme support any plugins? =

Real Home includes support for Aarambha Kits for Elementor and for Infinite Scroll in Jetpack.

== Copyright ==

Blogin Aarambha WordPress Theme, Copyright (c) 2022, Aarambha Themes
Blogin Aarambha is distributed under the terms of the GNU GPL

Blogin Aarambha Theme bundles the following third-party resources:

Underscores
Copyright (C) 2012-2020 Automattic, Inc.
License: GPLv2 or later - https://www.gnu.org/licenses/gpl-2.0.html
Source: https://underscores.me/

Font Awesome
Copyright Dave Gandy
License: SIL OFL 1.1 - https://scripts.sil.org/OFL, MIT License - http://opensource.org/licenses/MIT
Source: https://fontawesome.com/

MeanMenu
Copyright (C) 2012-2013 Chris Wharton (themes@meanthemes.com)
License: GNU General Public License - http://www.gnu.org/licenses/
Source: https://github.com/meanthemes/meanMenu

Select2
Copyright (c) 2012-2017 Kevin Brown, Igor Vaynberg, and Select2 contributors
License: MIT - https://opensource.org/licenses/MIT
Source: https://select2.org/

Slick
Copyright (c) Ken Wheeler
License: MIT - https://opensource.org/licenses/MIT
Source: https://github.com/kenwheeler/slick/

Theia Sticky Sidebar
Copyright (c) 2014 Liviu Cristian Mirea Ghiban
License: MIT - https://opensource.org/licenses/MIT
Source: https://github.com/WeCodePixels/theia-sticky-sidebar

Wp Color Picker Alpha
Copyright Sergio kallookoo
License: GPLv2 or later - https://www.gnu.org/licenses/gpl-2.0.html
Source: https://github.com/kallookoo/wp-color-picker-alpha

BreadcrumbTrail
Copyright (c) 2008-2015 Justin Tadlock
License: GNU General Public License v2.0 - https://github.com/justintadlock/breadcrumb-trail/blob/master/license.md
Source: https://github.com/justintadlock/breadcrumb-trail

Webfonts Loader
Copyright (c) 2020 WPTT
License: MIT - https://github.com/WPTT/webfont-loader/blob/master/LICENSE
Source: https://github.com/WPTT/webfont-loader

TGM Plugin Activation
Copyright (c) 2011, Thomas Griffin
License: GNU General Public License v2.0 - https://github.com/TGMPA/TGM-Plugin-Activation/blob/develop/LICENSE.md
Source: https://github.com/TGMPA/TGM-Plugin-Activation

Customizer Header/Footer Builder reference from CosmosWP
License: GPLv2 or later - https://www.gnu.org/licenses/gpl-2.0.html
Source: https://wordpress.org/themes/cosmoswp/


Images for theme screenshot:

Logo,
License: CC0 license - https://creativecommons.org/about/cc0

== Image Used ==

License: pxhere provides images under CC0 1.0 Universal (CC0 1.0) 
license: https://creativecommons.org/publicdomain/zero/1.0/
Source:  https://pxhere.com/en/photo/1338804
         https://pxhere.com/en/photo/1126300
         https://pxhere.com/en/photo/1391136


All images from assets/images and inc/meta-boxes/assets/images
left.svg:
none.svg:
right.svg:
search-icon.png:
default-sidebar.svg:
left-sidebar.svg:
no-sidebar.svg:
right-sidebar.svg:
License: Creative Commons Zero (CC0) license - https://creativecommons.org/about/cc0
Source:	Self Created
