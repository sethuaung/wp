=== Webinar and Video Conference with Jitsi Meet ===
Contributors: wppool, azizultex, foysalremon
Tags: jitsi meeting, live video chat, video chat, wordpress meeting plugin, wordpress video conference, plugin, wordpress webinar, best video conferencing plugin for wordpress, buddypress, buddypress video conference, jitsi meet, video conferencing plugin for wordpress, video meeting wordpress plugin, webinar and video conference with jitsi meet, wordpress plugin for video conferencing
Requires at least: 5.0
Tested up to: 6.0.2
Requires PHP: 5.4
Stable Tag: 1.2.5
License: GPL-2.0+
Lisecse URL: http://www.gnu.org/licenses/gpl-2.0.txt

Experience the best WordPress video conference plugin and live video chat solution. Get shortcode support with WooCommerce and BuddyPress integration.

== Description ==
Looking for a branded WordPress meeting experience that’s a simple, inexpensive, and no-technical choice? Introducing Jitsi Meet - the easiest solution to host virtual branded live webinars, meetings, conferences, online classes, and video calls directly on your WordPress website. Experiencing live video chat with a WordPress meeting plugin has never been so easy!

[youtube https://youtu.be/eANgixqxe84]

👁️ [View Demo](https://go.wppool.dev/O0Z) | 🚀 [Get The Ultimate Version](https://go.wppool.dev/ruO4) | 👩🏼‍💻 [Get Support](https://wppool.dev/contact/)

Built-in meeting post type, shortcodes, Elementor widgets, and Gutenberg blocks let you create WordPress meetings without any technical or coding knowledge. Fully integrated with WooCommerce, LMS, Multivendors, email marketing, and booking plugins. This is the most powerful and convenient WordPress video conference plugin with zero coding.

You can turn your video meeting created as an individual post on your website. Not only that, but you can also embed the meeting on your website. You deserve the best in the best live video chat plugin for WordPress.

Jitsi Meet Pro provides the JaaS integration for branded WordPress meeting plugin experience with a custom domain. Create a unique branded name on the URL which identifies your website and business.

Seamless integration with WooCommerce, LearnPress, and Tutor LMS allows you to create and embed meetings on these platforms with unlimited attendees, livestream, transcript, and recording options too. Jitsi Meet also has Google, Outlook, and Apple calendar integration.

You can install the plugin on your WordPress site and start meeting right from your website without any email, password, or verification hassles. Configure your video frame height and width that suits your needs. Creating WordPress webinars is not an arduous task anymore.

Maintain productivity with HD video, screen sharing, and tons of collaboration features from any device for businesses of all sizes with the best WordPress video conference plugin - Jitsi Meet.

==🎁Free Features==

**Meeting Gutenberg block** - Jitsi Meet WP plugin has built-in Gutenberg block that allows you to create as many conferences and video calls as you want with the easiest way.

**Meeting Elementor widget** - You can create meetings from Elementor using shortcode very easily. Experience a hassle-free live video chat experience.

**Meeting Shortcode** - Shortcode available for various purposes in Jitsi Meet such ase - create and embed video meeting on website, create meeting from Elementor widget and more.

**Mute meeting** - You can mute the video conference or webinar as per wish while hosting vis Jitsi Meet.

**Screen sharing** - The host and participants can share their screens for better communication in virtual meetings of Jitsi Meet. This is the smartest WordPress meeting plugin on the market.

**Unlimited meetings** - You can host as many live webinars and video conferences as you want using the Jitsi Meet plugin from your WordPress website.

👁️[View Demo](https://go.wppool.dev/O0Z) | 🚀 [Get The Ultimate Version](https://go.wppool.dev/ruO4) | 👩🏼‍💻 [Get Support](https://wppool.dev/contact/)

==🔥PRO Features==

**Custom domain** - Create a unique branded name on the URL which identifies your website.

**Meeting post type** - You can embed the created meeting as a post on your website.

**Livestream, recording, and transcript** - Get an in-build recording option with automatically transcript setting.

**Calendar integration** - Schedule meetings from Google, Outlook, and Apple calendar and send invitations to participants as well.

**Meeting registration** - Allow only registered participants and keep your meeting secured.

**Recurring meeting** - Set a recurring meeting with a unique ID to be used at any time.

==💪Ultimate Features==

**WooCommerce integration** - Automate your meetings directly from the WordPress dashboard by linking them to your WooCommerce products automatically.

**BuddyPress Integration** - BuddyPress group members can now join meeting rooms anytime they want. Activate Jitsi Meet Ultimate to find a new meeting room within the BuddyPress groups. A super easy solution to organize BuddyPress video conferences.

**LMS integration** - Live interaction is significant for online lessons to ensure student satisfaction. Activate Jitsi Meet Ultimate and add a video meeting facility for your learners. This feature is available for both LearnPress and Tutor LMS.

**Integration with booking plugin** - Automate bookings online 24/7, turn those into virtual appointments, and send video links for seamless business communications.

**Integration with Multi Vendor Marketplace (upcoming)** - Create branded video meetings and schedule them from your Multi-Vendor Marketplace like Dokan and provide prompt customer support.

**Integration with email marketing tools (upcoming)** - Integration with email marketing tools will automatically add the registrants as new subscribers directly to your Zapier, MailChimp, etc. accounts.

👁️[View Demo](https://go.wppool.dev/O0Z) | 🚀 [Get The Ultimate Version](https://go.wppool.dev/ruO4) | 👩🏼‍💻 [Get Support](https://wppool.dev/contact/)

== Installation ==

1. Go to your Dashboard
2. Opt-in to Plugins > Add New 
3. Search for Webinar and Video Conference with Jitsi Meet
4. Click on Install and Activate

== How to use as Gutenberg block? ==

1. While you are on the post/page edit screen click on Gutenberg plus icon to add a new Gutenberg block
2. Add "Jitsi Meet" from the "Embeds" category
3. Set width and height and you are done

== How to use as shortcode? ==

Write down the shortcode '[jitsi-meet-wp/]' and set width, height and name of conference with attribute. Ex. `[jitsi-meet-wp name="YourWebinerName" width="700" height="640"/]`

== 🔥 More Awesome Plugins ==
If you like Webinar and Video Conference with Jitsi Meet, then consider checking out our other awesome projects:

🌓 **[WP Dark Mode](https://wordpress.org/plugins/wp-dark-mode/)**  - Use WP Dark Mode plugin to create a stunning dark version for your WordPress website. WP Dark Mode works automatically without going into any complicated settings.

🔄 **[Sheets To WP Table Live Sync](https://wordpress.org/plugins/sheets-to-wp-table-live-sync/)**  - Quick. Easy. Simple. Keep your Google Spreadsheet data always synced LIVE with the WordPress table. Responsive data tables with as many data you want to display - Sheets to WP Table Live Sync plugin got it all!

🎥 **[Easy Video Reviews](https://wordpress.org/plugins/easy-video-reviews/)**  - Easy Video Reviews is the best and easiest video review plugin for WordPress. Your customers can record and send video testimonials right from their browser, and you can manage and showcase anywhere on your WordPress website.

💬 **[Chat Widgets for Multivendor Marketplaces](https://wordpress.org/plugins/chat-widgets-for-multivendor-marketplaces/)**  - This chat widget plugin makes it very easy for you to connect with your potential customers via their favorite social media channels like Facebook, Messenger, WhatsApp, Viber, Telegram, Slack, TikTok, and more.

== Frequently Asked Questions ==

= How quickly can I get started? =
Just activate the plugin on your website and get started within minutes. Watch this [Video](https://www.youtube.com/watch?v=MckVL389lpc).

= Does the plugin support Gutenberg and Elementor? =
Yes, you can create meetings as Gutenberg block and Elementor widget using the Jitsi Meet plugin.

= Is there any shortcode? =
Yes, with the shortcode [jitsi-meet-wp/], you can easily add the Jitsi Meet conference room where there is no Gutenberg support.

= Can I create a custom domain? = 
You can create branded meeting URL for your website with the custom domain feature of Jitsi Meet.

= Is Jitsi Meet safe? =
Yes, it's absolutely secure. We're committed to protecting your privacy and your data while you conduct online meetings.

= I want a refund for my purchase of the plugin. What is the procedure? = 
We have a 14 days refund policy. Please email us at support@wppool.dev explaining why you would like to get a refund.

= Whom do I contact for any kind of support? = 
For instant support, please send us a message on our Facebook page, or you can also contact us through [Support](https://wppool.dev/contact/).

= Can I use self-hosted Jitsi Meet with this plugin? = 
Yes, You can use Jitsi Meet free - [https://meet.jit.si](https://meet.jit.si), JaaS - [https://jaas.8x8.vc](https://jaas.8x8.vc) or self-hosted Jitsi Meet with our plugin as you need as per your usage requirements.

= Can I use my branded logo and domain? =
Yes, You can use your own branded logo and domain when using JaaS - [https://jaas.8x8.vc](https://jaas.8x8.vc) or self-hosted Jitsi Meet.

= Can I record the meeting? = 
Yes, you can record your meetings. However, if you want, you can turn off the recording meeting in the premium version.

= How many Active users do we have? =
On the free version, you have unlimited active users, But the participants are limited to 100/per meeting.

When you have a JaaS subscription then 25 monthly active users are free, 300 on basic, 1500 on JaaS Standard and 3000 on Business package.

Participants are limited to JaaS 500/meetings, to increase the limit you will need to change your plan according to your need.

You can have as many meetings as you want for self-hosting. It depends on where you host the Jitsi Meet server and how much resources you allocate.

Although Jitsi suggested 75 participants can attend for flawless meetings. But you can manage thousands of meetings based on the resources allocated to your server.

= Where will my meeting recordings be stored? =
On the free version users have only Dropbox to store the recording But JASS has Dropbox with Google cloud.

= Can I do Live Streaming? = 
Yes, You can do live streaming on YouTube from the Meeting.

== ScreenShots ==

1. Branding Settings
2. API Settings
3. Audio Settings
4. Configurations
5. Video Settings
6. Create Meeting
7. Meeting Registration 
8. LMS Integration 
9. WooCommerce Integration 1
10. WooCommerce Integration 2
11. Self Hosted

== Changelog ==

= 1.2.5 - 3 OCT 2022 =
- Regular user experience checked and improved 

= 1.2.4 - 6 SEP 2022 =
- Checked and fixed compatibility with WordPress 6.0.2

= 1.2.3 - 19 AUG 2022 =
- Regular user experience checked and improved 

= 1.2.2 - 3 AUG 2022 =
- Checked and fixed compatibility with WordPress 6.0.1
- Updated and optimize user friendly and organized readme

= 1.2.1 - 29 MAY 2022 =
- Checked and fixed compatibility with WordPress 6.0

= 1.2.0 - 30 MAR 2022 =
- Checked and fixed regular compatibility checking
- Fixed contact subscription issue

= 1.1.10 - 13 MAR 2022 =
- Fix: JavaScript conflict error with Classic Editor and All in One Seo plugins
- Improved: Tested and made compatible with WordPress 5.9.2

= 1.1.9 - 08 MAR 2022 =
- Improved: Tested and made compatible with WordPress 5.9.1
- Fixed: Logged in user not automatically added to room

= 1.1.8 - 15 FEB 2022 =
- Improved: Elementor 3.5.5 compatibility
- Improved: New icon on gutenberg block 

= 1.1.7 - 26 JAN 2022 = 
- Improved: Tested and make compitable with WordPress 5.9

= 1.1.6 - 02 JAN 2022 = 
- Added: Enabling/Disabling inviting
- Improved: Tested and make compitable with PHP 8.1

= 1.1.5 - 23 DEC 2021 =
- Updated: Frequently asked questions
- Modified: Change Headway position

= 1.1.4 - 19 DEC 2021 =
- Fixed: Force welcome screen issue
- Added: Skip button on welcome screen
- Updated: Logo and assets

= 1.1.3 - 06 DEC 2021 =
- Added: Enable using your own hosted jitsi 
- Added: Welcome screen on activation to setup jitsi api source

= 1.1.2 - 15 NOV 2021 =
- Improved: Tested and make compitable with WordPress 5.8.2
- Added: Review & Affiliate banner on admin 

= 1.1.1 - 25 OCT 2021 =
- Updated: Description on plugin page
- Updated: Screenshots on plugin page 
- Added: Screenshots on plugin page 

= 1.1.0 - 29 SEP 2021 =
- Modified: Integration with Jitsi Pro and Jitsi Ultimate

= 1.0.9 - 23 SEP 2021 =
- Modified: Link to pro version 
- Added: Headway changelog viewer on admin menu 

= 1.0.8 - 15 SEP 2021 =
- Improvement: Randomize the meeting name on shortcode

= 1.0.7 - 14 SEP 2021 =
- Improvement: Improved compatibility for WordPress 5.8.1
- Improvement: Changelog Reorganized

= 1.0.6 - 06 SEP 2021 =
- Updated: Setting page
- Added: Elementor Widget

= 1.0.5 - 12 APR 2021 =
- Updated: Appsero client
- Added: Deactivation reasons

= 1.0.4 - 08 NOV 2020 =
- Added: Start with audio muted option
- Added: Start with video muted option
- Added: Start with screen sharing option

= 1.0.3 - 17 AUG 2020 =
- Mobile responsive fixed with app download option

= 1.0.2 - 14 AUG 2020 =
- Enabled name edit on gutenberg block
- Optimized gutenberg block

= 1.0.1 - 12 AUG 2020 =
- Shortcode added

= 1.0.0 - 22 JUL 2020 =
- Initial stable realese

== Privacy Policy  ==
Webinar and Video Conference with Jitsi Meet uses [Appsero](https://appsero.com) SDK to collect some telemetry data upon user's confirmation. This helps us to troubleshoot problems faster & make product improvements.

Appsero SDK **does not gather any data by default.** The SDK only starts gathering basic telemetry data **when a user allows it via the admin notice**. We collect the data to ensure a great user experience for all our users. 

Integrating Appsero SDK **DOES NOT IMMEDIATELY** start gathering data, **without confirmation from users in any case.**

Learn more about how [Appsero collects and uses this data](https://appsero.com/privacy-policy/).