<?php

/**
 *
 * @package    Video_Conference
 * @subpackage Video_Conference/admin
 * @author     Helmi <pm2monit@gmail.com>
 */
class Video_Conference_Admin {

	private $plugin_name;
	private $version;

	public function __construct( $plugin_name, $version ) {
		$this->plugin_name = $plugin_name;
		$this->version = $version;
	}

	public function enqueue_styles() {
		wp_enqueue_style( $this->plugin_name, plugin_dir_url( __FILE__ ) . 'css/video-conference-admin.css', array(), $this->version, 'all' );
	}

	public function enqueue_scripts() {
		wp_enqueue_script( $this->plugin_name, plugin_dir_url( __FILE__ ) . 'js/video-conference-admin.js', array( 'jquery' ), $this->version, false );
	}

	// Register Custom Post Type
	public function vicon_post_type() {

		$labels = array(
			'name'                  => _x( 'Video Conference', 'Post Type General Name', 'text_domain' ),
			'singular_name'         => _x( 'Video Conference', 'Post Type Singular Name', 'text_domain' ),
			'menu_name'             => __( 'Vicon', 'text_domain' ),
			'name_admin_bar'        => __( 'Video Conference', 'text_domain' ),
			'archives'              => __( 'Meeting Archives', 'text_domain' ),
			'attributes'            => __( 'Meeting Attributes', 'text_domain' ),
			'parent_item_colon'     => __( 'Parent Meeting:', 'text_domain' ),
			'all_items'             => __( 'All Meetings', 'text_domain' ),
			'add_new_item'          => __( 'Add New MeetingID', 'text_domain' ),
			'add_new'               => __( 'Add New MeetingID', 'text_domain' ),
			'new_item'              => __( 'New MeetingID', 'text_domain' ),
			'edit_item'             => __( 'Edit MeetingID', 'text_domain' ),
			'update_item'           => __( 'Update MeetingId', 'text_domain' ),
			'view_item'             => __( 'View MeetingID', 'text_domain' ),
			'view_items'            => __( 'View MeetingIDs', 'text_domain' ),
			'search_items'          => __( 'Search MeetingID', 'text_domain' ),
			'not_found'             => __( 'Not found', 'text_domain' ),
			'not_found_in_trash'    => __( 'Not found in Trash', 'text_domain' ),
			'featured_image'        => __( 'Featured Image', 'text_domain' ),
			'set_featured_image'    => __( 'Set featured image', 'text_domain' ),
			'remove_featured_image' => __( 'Remove featured image', 'text_domain' ),
			'use_featured_image'    => __( 'Use as featured image', 'text_domain' ),
			'insert_into_item'      => __( 'Insert into MeetingID', 'text_domain' ),
			'uploaded_to_this_item' => __( 'Uploaded to this item', 'text_domain' ),
			'items_list'            => __( 'MeetingIDs list', 'text_domain' ),
			'items_list_navigation' => __( 'MeetingIDs list navigation', 'text_domain' ),
			'filter_items_list'     => __( 'Filter MeetingIDs list', 'text_domain' ),
		);
		$args = array(
			'label'                 => __( 'Video Conference', 'text_domain' ),
			'description'           => __( 'Video Conferece Plugin With Jitsi', 'text_domain' ),
			'labels'                => $labels,
			'supports'              => array( 'title', 'excerpt'),
			// 'taxonomies'            => array( 'category'),
			'hierarchical'          => false,
			'public'                => true,
			'show_ui'               => true,
			'show_in_menu'          => true,
			'menu_position'         => 5,
			'menu_icon'             => 'dashicons-video-alt2',
			'show_in_admin_bar'     => true,
			'show_in_nav_menus'     => true,
			'can_export'            => true,
			'has_archive' 			=> false,
			'exclude_from_search'   => true,
			'publicly_queryable'    => true,
			'capability_type'       => 'page',
			'show_in_rest'          => true,
		);
		register_post_type( 'vicon', $args );

	}
	// add_action( 'init', 'custom_post_type', 0 );

}
