<?php

/**

 * @link       https://webflazz.com/
 * @since      1.0.0
 *
 * @package    Video_Conference
 */

if ( ! defined( 'WP_UNINSTALL_PLUGIN' ) ) {
	exit;
}
