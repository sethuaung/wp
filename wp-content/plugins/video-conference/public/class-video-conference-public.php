<?php

/**
 *
 * @package    Video_Conference
 * @subpackage Video_Conference/public
 * @author     Helmi <pm2monit@gmail.com>
 */
class Video_Conference_Public {

	private $plugin_name;
	private $version;

	public function __construct( $plugin_name, $version ) {
		$this->plugin_name = $plugin_name;
		$this->version = $version;
	}

	public function enqueue_styles() {
		wp_enqueue_style( $this->plugin_name, plugin_dir_url( __FILE__ ) . 'css/video-conference-public.css', array(), $this->version, 'all' );
	}

	public function enqueue_scripts() {
		wp_enqueue_script( $this->plugin_name, plugin_dir_url( __FILE__ ) . 'js/video-conference-public.js', array( 'jquery' ), $this->version, false );
		wp_enqueue_script( $this->plugin_name, plugin_dir_url( __FILE__ ) . 'js/external_api.js', array( 'jquery' ), $this->version, true );
	}

	public function vicon_join_public() {
		ob_start();
		include_once VICON_PATH . 'public/partials/video-conference-public-display.php';
		$template = ob_get_contents();
		ob_end_clean();
		echo $template;
	}

	public function jitsi_page() {
		global $post;
		$page_template = '';

		if($post->post_type === 'vicon') {
			$page_template = VICON_PATH . 'public/partials/single-vicon.php';	
		}

		return $page_template;
	}


	// public function pt_add_page_template_to_dropdown($templates) {
	// 	$templates[VICON_PATH . 'public/partials/page-jitsi.php'] = __('Jitsi Page', 'text-domain');
	// 	// $templates[plugin_dir_path(__FILE__) . 'templates/page-template.php'] = __('Page Template From Plugin', 'text-domain');
	// 	return $templates;
	// }

	// public function pt_change_page_template($template) {
	// 	global $post;
	// 	if (is_page()) {
	// 		$meta = get_post_meta(get_the_ID());

	// 		if (!empty($meta['_wp_page_template'][0]) && $meta['_wp_page_template'][0] != $template) {
	// 			$template = $meta['_wp_page_template'][0];
	// 		}
	// 	}

	// 	return $template;
	// }


	// public function pt_remove_style() {
	// 	if (is_page('my-page')) {
	// 		$theme = wp_get_theme();

	// 		$parent_style = $theme->stylesheet . '-style'; 

	// 		wp_dequeue_style($parent_style);
	// 		wp_deregister_style($parent_style);
	// 		wp_deregister_style($parent_style . '-css');
	// 	}
	// }

}
